<%--
  index.jsp
  Homepage del progetto
--%>

<%@ page import="classes.paziente" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="classes.visita" %>
<%
    String usertype="";
    Cookie[] cookies = request.getCookies();
    if(cookies != null)
    {
        for (int i = 0; i < cookies.length; i++)
        {
            if ((cookies[i].getName().equals("user_type")) && (cookies[i].getValue() != null))
            {
                usertype = cookies[i].getValue();
            }
        }
    }
%>

<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="it">
<head>
    <%@include file="assets/include/head.html" %>
    <title>Sanitaonline.it</title>
  </head>
  <body>
    <!-- Navbar  -->
    <nav class="navbar navbar-expand-lg navbar-light px-5">
      <%
        if(usertype.equals(""))
        {
      %>
      <a class="navbar-brand" href="/"><span class="brand-color"><span class="logo">Sanità</span>online</span></a>
      <%
        }else
          {
      %>
      <a class="navbar-brand" href="home"><span class="brand-color"><span class="logo">Sanità</span>online</span></a>
      <%
        }
      %>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav ml-auto">
          <%
            if(usertype.equals(""))
            {
          %>
            <a class="nav-item nav-link" href="login.jsp">
            Accedi
            <i class="las la-user-circle"></i>
            </a>
          <%
            }else
              {
          %>
            <li class="nav-item dropdown" href="#">
              <a class="nav-link dropdown-toggle" href="home" id="navbarDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Ciao
                <span class="font-weight-bold">
                  <%
                      if(usertype.equals("dottore"))
                      {
                  %>
                    Dr.
                  <%
                      }else if(usertype.equals("ssp"))
                      {
                  %>
                    SSP
                  <%
                      }
                  %>
                  ${cookie.nome.value}
                </span>
                <i class="las la-user-circle"></i>
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="home">Homepage</a>
                <a class="dropdown-item" href="/">ANSS</a>
                <a class="dropdown-item" href="logout">Logout</a>
              </div>
            </li>
          <%
              }
          %>
        </div>
      </div>
    </nav>

    <!-- main banner -->
    <div class="container-fluid main-landing-container pt-5">
      <div class="row">
        <div class="col-md">
          <div class="row h-100 justify-content-center align-items-center">
            <div class="col-10">
              <h1>Azienda Nazionale <br> per i Servizi Sanitari</h1>
            </div>
          </div>
        </div>
        <div class="col-md pr-5">
          <img src="assets/img/doctors.svg" class="float-left img-fluid">
        </div>
      </div>

      <!-- chi siamo -->
      <div class="row py-3 pl-3 pl-sm-5 chi-siamo">
        <div class="col-md-6 pr-5">
          <h2>Chi siamo</h2>
          <p>
            L’Azienda Nazionale per i Servizi Sanitari (ANSS) è l’ente strumentale
            della Nazione preposto alla gestione coordinata delle attività sanitarie
            e sociosanitarie per l’intero territorio italiano.
            <br>
            ANSS serve più di 60 milioni di abitanti sparsi nelle 20 regioni del paese.
            Può contare sulla collaborazione di circa 600.000 dipendenti,
            che operano in 518 ospedali pubblici e circa 4.000 ambulatori specialistici,
            distribuiti su tutto il territorio.
            <br>
            L’Azienda garantisce l’erogazione delle prestazioni anche grazie a
            rapporti contrattuali e convenzionali con alcune strutture ospedaliere
            e ambulatoriali private, con 482 ospedali e oltre 5.000 ambulatori specialistici.
          </p>
        </div>
      </div>

      <div class="row pl-3 pl-sm-5">
        <div class="col-md-6 pr-5">
          <h2>Cosa facciamo</h2>
          <p>
            Il portale <span class="brand-color"><span class="font-weight-bold">Sanità</span>online</span>
            è la piattaforma online dell'ANSS.
            <br>
            Permette la gestione coordinata dei servizi sociosanitari dedicati ai cittadini,
            ai dottori, e ai Sistemi Sanitari Provinciali (SSP) del territorio italiano.
          </p>
        </div>
      </div>

      <div class="row pl-3 pl-sm-5 pb-5">
        <div class="col-12 col-md-4 py-md-3">
          <h4 class="font-family-work-sans text-md-center pl-3 pl-md-0">Servizi per il cittadino</h4>
          <ul class="list-group list-group-flush">
            <li class="list-group-item">Visionare visite, ricette, esami, visite specialistiche e tickets</li>
            <li class="list-group-item">Prenotare una visita dal proprio dottore</li>
            <li class="list-group-item">Cambiare dottore</li>
            <li class="list-group-item">Pagare tickets</li>
          </ul>

        </div>
        <div class="col-12 col-md-4 py-md-3">
          <h4 class="font-family-work-sans text-md-center pl-3 pl-md-0">Servizi per il dottore</h4>
          <ul class="list-group list-group-flush">
            <li class="list-group-item">Visionare visite, ricette, esami e visite specialistiche dei propri pazienti</li>
            <li class="list-group-item">Prescrivere ricette, esami e visite specialistiche</li>
            <li class="list-group-item">Visionare il proprio parco pazienti</li>
            <li class="list-group-item">Effettuare visite specialistiche</li>
          </ul>
        </div>
        <div class="col-12 col-md-4 py-md-3">
          <h4 class="font-family-work-sans text-md-center pl-3 pl-md-0">Servizi per il SSR</h4>
          <ul class="list-group list-group-flush">
            <li class="list-group-item">Visionare le ricette prescritte nella provincia</li>
            <li class="list-group-item">Effettuare esami ai cittadini della provincia</li>
          </ul>
        </div>
      </div>
    </div>

    <%@include file="assets/include/javaScript.html" %>
 </body>
</html>
