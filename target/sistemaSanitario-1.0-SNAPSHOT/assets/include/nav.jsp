<%@ page contentType="text/html; charset=UTF-8" %>
<!-- Navbar  -->
<nav class="navbar navbar-expand-lg navbar-light px-5">
    <a class="navbar-brand" href="home"><span class="brand-color"><span class="logo">Sanità</span>online</span></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav ml-auto">
            <li class="nav-item dropdown" href="#">
                <a class="nav-link dropdown-toggle" href="home" id="navbarDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Ciao
                    <span class="font-weight-bold">
                  <%
                      if(usertype.equals("dottore"))
                      {
                  %>
                    Dr.
                  <%
                  }else if(usertype.equals("ssp"))
                  {
                  %>
                    SSP
                  <%
                      }
                  %>
                  ${cookie.nome.value}
                </span>
                    <i class="las la-user-circle"></i>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="home">Homepage</a>
                    <a class="dropdown-item" href="/">ANSS</a>
                    <a class="dropdown-item" href="logout">Logout</a>
                </div>
            </li>
        </div>
    </div>
</nav>

<div class="row m-0 p-0">
  <div class="col pl-5 pt-2">
    <button class="btn btn-outline-dark" id="indietro"> < </button>
  </div>
</div>
