<%!
    public String formatMonth(int month)
    {
        switch (month) {
        case 1:
            return "Gen";
        case 2:
            return "Feb";
        case 3:
            return "Mar";
        case 4:
            return "Apr";
        case 5:
            return "Mag";
        case 6:
            return "Giu";
        case 7:
            return "Lug";
        case 8:
            return "Aug";
        case 9:
            return "Set";
        case 10:
            return "Ott";
        case 11:
            return "Nov";
        case 12:
            return "Dic";
        default:
            return "n/a";
        }
    }
%>