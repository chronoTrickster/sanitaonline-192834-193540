<%--
  cambiaDottore.jsp
--%>

<%@page import="classes.dottore"%>

<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>

<%@include file="assets/include/loginCheck.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="it">
<head>
  <%@include file="assets/include/head.html" %>
  <%@include file="assets/include/javaScript.html" %>
  <!-- bootstrap-select -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>

  <title>Cambia dottore</title></head>
<body>
  <%@include file="assets/include/nav.jsp" %>

  <div class="container-fluid pt-5 px-3 px-sm-5">
    <div class="row align-items-center data-header pb-5">
      <div class="col-sm-6 offset-sm-3 wrap">
        ${dottore.getSesso() == ("M") ? "  Il tuo dottore è" : "La tua dottoressa è"}
        <br>
        <span class="data-header-nome">
          ${dottore.getNome()} ${dottore.getCognome()}
        </span>
      </div>
    </div>

    <div class="row align-items-center data-content pb-5">
      <div class="col-md-8 offset-md-2 col-lg-6 offset-lg-3">
        <%
          List<dottore> dottoriDisponibili = (ArrayList<dottore>)request.getAttribute("dottoriDisponibili");
          if(dottoriDisponibili.size() == 0){
        %>
         Non sono disponibili altri dottori nella sua provincia di residenza.
        <%
          }else{
        %>
          <h2>Cambia dottore</h2>
          Scegli un nuovo dottore dalla lista <br>
          <form method="post" action="cambiaDottore">
              <div class="form-group">
               <select class="form-control pt-3 pb-3 selectpicker show-tick" data-live-search="true" name='id' id="dottori">
                 <%
                     for(dottore dottore : dottoriDisponibili)
                     {
                           out.print("<option value=" + dottore.getId() + ">");
                           out.print(dottore.getSesso().equals("M") ? "Dott. " : "Dott.ssa ");
                           out.print(dottore.getNome() + " " + dottore.getCognome());
                           out.print("</option>");
                     }
                 %>
               </select>
              </div>
              <div class="row">
                <div class="col-auto pt-5 ml-auto">
                  <button class="btn btn-primary" type="submit">Scegli</button>
                </div>
              </div>
          </form>
        <%
          }
        %>
      </div>
    </div>
  </div>
</body>
</html>
