<!-- homePaziente.jsp -->

<%@ page import="classes.paziente" %>
<%@ page import="classes.visita" %>
<%@ page import="classes.ricetta" %>
<%@ page import="classes.esame" %>
<%@ page import="classes.specialistica" %>
<%@ page import="classes.ticketEsame" %>
<%@ page import="classes.ticketSpecialistica" %>

<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>

<%@include file="assets/include/loginCheck.jsp" %>
<%@include file="assets/include/monthFormatter.jsp" %>

<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="it">
<head>
    <%@include file="assets/include/head.html" %>
    <title>Homepage</title>
</head>
<body>
<%@include file="assets/include/nav.jsp" %>

<div class="container-fluid pt-5 px-3 px-sm-5">
  <div class="row wrap text-center text-lg-left pb-5">
    <div class="col-lg-4 pb-4 p-lg-5 user-nome">
      <img src="${paziente.getPropic()}"  class="img-fluid rounded-circle">
      <h1>${paziente.getNome()} ${paziente.getCognome()}</h1>
    </div>
    <div class="col-lg-8 pb-5 p-lg-5 lead text-left">
      <div class="row">
        <div class="col-12 col-sm-6 text-lg-right font-weight-normal">
          Luogo di nascita
        </div>
        <div class="col-12 col-sm-6">
          ${paziente.getLuogoNascita()}
        </div>
      </div>
      <div class="row">
        <div class="col-12 col-sm-6 text-lg-right font-weight-normal">
          Data di nascita
        </div>
        <div class="col-12 col-sm-6">
          ${paziente.getDataDiNascita().getDayOfMonth()}-${paziente.getDataDiNascita().getMonthValue()}-${paziente.getDataDiNascita().getYear()}
        </div>
      </div>
      <div class="row">
        <div class="col-12 col-sm-6 text-lg-right font-weight-normal">
          Codice dottore
        </div>
        <div class="col-12 col-sm-6">
          ${String.format("%05d", paziente.getIdDottore())}
        </div>
      </div>
      <div class="row">
        <div class="col-12 col-sm-6 text-lg-right font-weight-normal">
          Provincia
        </div>
        <div class="col-12 col-sm-6">
          ${paziente.getProvincia()}
        </div>
      </div>
      <div class="row">
        <div class="col-12 col-sm-6 text-lg-right font-weight-normal">
          Sesso
        </div>
        <div class="col-12 col-sm-6">
          ${paziente.getSesso() == 'M' ? 'Maschio' : 'Femmina'}
        </div>
      </div>
      <div class="row">
        <div class="col-12 col-sm-6 text-lg-right font-weight-normal">
          C.F.
        </div>
        <div class="col-12 col-sm-6">
         ${paziente.getCodiceFiscale()}
        </div>
      </div>

      <div class="row pr-lg-5 ">
        <div class="col-12 col-sm-6 col-lg-5 offset-lg-2 col-xl-4 offset-xl-4 pt-3">
          <a href="#" role="button" class="btn btn-outline-secondary btn-block" data-toggle="modal" data-target="#cambiaPropicModal">Cambia immagine</a>
        </div>
        <div class="col-12 col-sm-6 col-lg-5 col-xl-4 pt-3">
          <a href="cambiaDottore" role="button" class="btn btn-outline-secondary btn-block">Cambia dottore</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal -->
<div class="modal fade" id="cambiaPropicModal" tabindex="-1" role="dialog" aria-labelledby="cambiaPropic" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title font-family-work-sans" id="exampleModalLabel">Cambia immagine</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="cambiaPropic" enctype = "multipart/form-data">
        <div class="modal-body">
            <div class="form-group">
              <label for="uploadFile">Scegli una nuova immagine profilo</label>
              <input type="file" class="form-control-file" id="uploadFile" name="file" accept="image/jpeg">
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
          <button type="submit" class="btn btn-primary">Salva</button>
        </div>
      </form>
    </div>
  </div>
</div>

  <div class="row wrap text-center pb-5">
    <div class="col-12 col-md-6 col-lg-5 col-xl-4 mx-auto px-sm-3">
      <a href="/nuovaVisita" role="button" class="btn btn-block btn-lg btn-outline-primary">Prenota una visita</a>
    </div>
  </div>

  <!-- NAV PILL -->
  <ul class="nav nav-pills flex-column flex-sm-row" id="pazienteTab" role="tablist">
    <li class="nav-item flex-sm-fill text-sm-center">
      <a class="nav-link active" id="visite-tab" data-toggle="tab" href="#visite" role="tab" aria-controls="visite" aria-selected="true">Visite</a>
    </li>
    <li class="nav-item flex-sm-fill text-sm-center">
      <a class="nav-link" id="ricette-tab" data-toggle="tab" href="#ricette" role="tab" aria-controls="ricette" aria-selected="false">Ricette</a>
    </li>
    <li class="nav-item flex-sm-fill text-sm-center">
      <a class="nav-link" id="esami-tab" data-toggle="tab" href="#esami" role="tab" aria-controls="esami" aria-selected="false">Esami</a>
    </li>
    <li class="nav-item flex-sm-fill text-sm-center">
      <a class="nav-link" id="specialistiche-tab" data-toggle="tab" href="#specialistiche" role="tab" aria-controls="specialistiche" aria-selected="false">Specialistiche</a>
    </li>
    <li class="nav-item flex-sm-fill text-sm-center">
      <a class="nav-link" id="tickets-tab" data-toggle="tab" href="#tickets" role="tab" aria-controls="tickets" aria-selected="false">Tickets</a>
    </li>
  </ul>

  <!-- MAIN CONTENT -->
  <div class="user-data tab-content py-5" id="pazienteTabContent">
    <!-- VISITE -->
    <div class="tab-pane fade show active" id="visite" role="tabpanel" aria-labelledby="visite-tab">
      <div class="container-fluid p-0 m-0 px-md-5">
        <%
            List<visita> visite = (ArrayList<visita>)request.getAttribute("visite");
            if(visite.size() == 0){
              out.print("Nessuna visita presente.");
            }else{
        %>
        <div class="row px-2 py-2 px-md-5">
          <div class="col-6 col-md-4">
            <h5>Data</h5>
          </div>
          <div class="col-5 col-md-4">
            <h5>Ora</h5>
          </div>
        </div>

        <%
              for(visita visita : visite)
              {
        %>

          <div class="container-fluid m-0 p-0 list-links-container">
            <%
            out.print("<a href='visita?id=" + visita.getId() + "'>");
            %>
              <div class="row px-2 py-2 px-md-5 border-bottom align-items-center list-link">
                <div class="col-6 col-md-4">
                <%
                  out.print(visita.getTimestamp().getDayOfMonth() + " " + formatMonth(visita.getTimestamp().getMonthValue()) + " " + visita.getTimestamp().getYear());
                %>
                </div>
                <div class="col-4 col-md-4">
                  <%
                    out.print(visita.getTimestamp().getHour() + ":" + String.format("%02d", visita.getTimestamp().getMinute()));
                  %>
                </div>
                <div class="col-2 col-md-4 text-right">
                 <span>&nearr;</span>
                </div>
              </div>
            </a>
          </div>

        <%
              }
            }
        %>
      </div>
    </div>

    <!-- RICETTE -->
    <div class="tab-pane fade" id="ricette" role="tabpanel" aria-labelledby="ricette-tab">
      <div class="container-fluid p-0 m-0 px-md-5">
        <%
            List<ricetta> ricette = (ArrayList<ricetta>)request.getAttribute("ricette");
            if(ricette.size() == 0){
              out.print("Nessuna ricetta presente.");
            }else{
        %>
        <div class="row px-2 py-2 px-md-5">
          <div class="col-4 d-none d-sm-block">
            <h5>Data</h5>
          </div>
          <div class="col-7 d-none d-sm-block">
            <h5>Medicine</h5>
          </div>
        </div>

        <%
              for(ricetta ricetta : ricette)
              {
        %>

        <div class="container-fluid m-0 p-0 list-links-container">
          <%
          out.print("<a href='ricetta?id=" + ricetta.getId() + "'>");
          %>
            <div class="row px-2 py-2 px-md-5 border-bottom align-items-center list-link">
              <div class="col-12 col-sm-4">
                <%
                  out.print(ricetta.getTimestamp().getDayOfMonth() + " " + formatMonth(ricetta.getTimestamp().getMonthValue()) + " " + ricetta.getTimestamp().getYear());
                %>
              </div>
              <div class="col-12 col-sm-7">
                <span class="text-secondary d-block d-sm-none">Medicine: </span>
                <%
                  out.print(ricetta.getMedicinali());
                %>
              </div>
              <div class="col-12 col-sm-1 text-right">
               <span>&nearr;</span>
              </div>
            </div>
          </a>
        </div>
        <%
              }
            }
        %>
      </div>
    </div>

    <!-- ESAMI -->
    <div class="tab-pane fade" id="esami" role="tabpanel" aria-labelledby="esami-tab">
      <div class="container-fluid p-0 m-0 px-md-5">
        <%
            List<esame> esami = (ArrayList<esame>)request.getAttribute("esami");
            if(esami.size() == 0){
              out.print("Nessun esame presente.");
            }else{
        %>
        <div class="row px-2 py-2 px-md-5">
          <div class="col-7 d-none d-sm-block">
            <h5>Tipologia</h5>
          </div>
          <div class="col-5 d-none d-sm-block">
            <h5>Stato</h5>
          </div>
        </div>

        <%
              for(esame esame : esami)
              {
        %>

        <div class="container-fluid m-0 p-0 list-links-container">
          <%
          out.print("<a href='esame?id=" + esame.getId() + "'>");
          %>
            <div class="row px-2 py-2 px-md-5 border-bottom align-items-center list-link">
              <div class="col-12 col-sm-7">
                <span class="text-secondary d-block d-sm-none">Tipologia: </span>
                <%
                  out.print(esame.getTipoEsame());
                %>
              </div>
              <div class="col-12 col-sm-5">
               <span class="text-secondary d-block d-sm-none">Stato: </span>
               <%
               if(!esame.getPagato()){
                 out.print("da pagare");
               }else if(esame.getAttiva()){
                 out.print("pagato/da fare");
               }else{
                 out.print("Svolto il " + esame.getTimestamp().getDayOfMonth() + " " + formatMonth(esame.getTimestamp().getMonthValue()) + " " + esame.getTimestamp().getYear());
               }
               %>
              </div>
            </div>
          </a>
        </div>
        <%
              }
            }
        %>
      </div>
    </div>

    <!-- SPECIALISTICHE -->
    <div class="tab-pane fade" id="specialistiche" role="tabpanel" aria-labelledby="specialistiche-tab">
      <div class="container-fluid p-0 m-0 px-md-5">
        <%
            List<specialistica> specialistiche = (ArrayList<specialistica>)request.getAttribute("specialistiche");
            if(specialistiche.size() == 0){
              out.print("Nessuna visita specialistica presente.");
            }else{
        %>
        <div class="row px-2 py-2 px-md-5">
          <div class="col-7 d-none d-sm-block">
            <h5>Tipologia</h5>
          </div>
          <div class="col-5 d-none d-sm-block">
            <h5>Stato</h5>
          </div>
        </div>
        <%
              for(specialistica specialistica : specialistiche)
              {
        %>
        <div class="container-fluid m-0 p-0 list-links-container">
          <%
          out.print("<a href='specialistica?id=" + specialistica.getId() + "'>");
          %>
            <div class="row px-2 py-2 px-md-5 border-bottom align-items-center list-link">
              <div class="col-12 col-sm-7">
                <span class="text-secondary d-block d-sm-none">Tipologia: </span>
                <%
                  out.print(specialistica.getTipoSpecialistica());
                %>
              </div>
              <div class="col-12 col-sm-5">
               <span class="text-secondary d-block d-sm-none">Stato: </span>
               <%
               if(!specialistica.getPagato()){
                 out.print("da pagare");
               }else if(specialistica.getAttiva()){
                 out.print("pagato/da fare");
               }else{
                 out.print("svolta il " + specialistica.getTimestamp().getDayOfMonth() + " " + formatMonth(specialistica.getTimestamp().getMonthValue()) + " " + specialistica.getTimestamp().getYear());
               }
               %>
              </div>
            </div>
          </a>
        </div>
        <%
              }
            }
        %>
      </div>
    </div>

    <!-- TICKETS -->
    <div class="tab-pane fade" id="tickets" role="tabpanel" aria-labelledby="tickets-tab">
      <div class="container-fluid p-0 m-0 px-md-5">
        <div id="accordion">
          <div class="card">
            <div class="card-header" id="headingOne">
              <h5 class="mb-0">
                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                  Tickets Esami
                </button>
              </h5>
            </div>
            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
              <div class="card-body">
                <%
                  List<ticketEsame> ticketsEsami = (ArrayList<ticketEsame>)request.getAttribute("ticketEsami");
                  if(ticketsEsami.size() == 0){
                    out.print("Nessun ticket di esami presente.");
                  }else{
                %>
                <div class="row px-2 py-2 px-md-5">
                  <div class="col-7 d-none d-sm-block">
                    <h5>Tipologia</h5>
                  </div>
                  <div class="col-5 d-none d-sm-block">
                    <h5>Stato</h5>
                  </div>
                </div>
                <%
                  for(ticketEsame ticketEsami : ticketsEsami)
                  {
                %>
                <div class="container-fluid m-0 p-0 list-links-container">
                  <%
                    out.print("<a href='esame?id=" + ticketEsami.getEsameId() + "'>");
                  %>
                  <div class="row px-2 py-2 px-md-5 border-bottom align-items-center list-link">
                    <div class="col-12 col-sm-7">
                      <span class="text-secondary d-block d-sm-none">Tipologia: </span>
                      <%
                        out.print(ticketEsami.getNome());
                      %>
                    </div>
                    <div class="col-12 col-sm-5">
                      <span class="text-secondary d-block d-sm-none">Stato: </span>
                      <%
                        if(!ticketEsami.getPagato()){
                          out.print("da pagare");
                        }else{
                          out.print("pagato il " + ticketEsami.getDataPagamento().getDayOfMonth() + " " + formatMonth(ticketEsami.getDataPagamento().getMonthValue()) + " " + ticketEsami.getDataPagamento().getYear());
                        }
                      %>
                    </div>
                  </div>
                  </a>
                </div>
                <%
                    }
                  }
                %>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingTwo">
              <h5 class="mb-0">
                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                  Tickets Visite Specialistiche
                </button>
              </h5>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
              <div class="card-body">

                <%
                  List<ticketSpecialistica> ticketsSpecialistiche = (ArrayList<ticketSpecialistica>)request.getAttribute("ticketSpecialistiche");
                  if(ticketsSpecialistiche.size() == 0){
                    out.print("Nessun ticket di visite specialistiche presente.");
                  }else{
                %>
                <div class="row px-2 py-2 px-md-5">
                  <div class="col-7 d-none d-sm-block">
                    <h5>Tipologia</h5>
                  </div>
                  <div class="col-5 d-none d-sm-block">
                    <h5>Stato</h5>
                  </div>
                </div>
                <%
                  for(ticketSpecialistica ticketSpecialistiche : ticketsSpecialistiche)
                  {
                %>
                <div class="container-fluid m-0 p-0 list-links-container">
                  <%
                    out.print("<a href='specialistica?id=" + ticketSpecialistiche.getSpecialisticaId() + "'>");
                  %>
                  <div class="row px-2 py-2 px-md-5 border-bottom align-items-center list-link">
                    <div class="col-12 col-sm-7">
                      <span class="text-secondary d-block d-sm-none">Tipologia: </span>
                      <%
                        out.print(ticketSpecialistiche.getNome());
                      %>
                    </div>
                    <div class="col-12 col-sm-5">
                      <span class="text-secondary d-block d-sm-none">Stato: </span>
                      <%
                        if(!ticketSpecialistiche.getPagato()){
                          out.print("da pagare");
                        }else{
                          out.print("pagato il " + ticketSpecialistiche.getDataPagamento().getDayOfMonth() + " " + formatMonth(ticketSpecialistiche.getDataPagamento().getMonthValue()) + " " + ticketSpecialistiche.getDataPagamento().getYear());
                        }
                      %>
                    </div>
                  </div>
                  </a>
                </div>
                <%
                    }
                  }
                %>

              </div>
            </div>
          </div>
        </div>

        <div class="row pt-3">
          <div class="col-auto ml-auto">
           <a role="button" href="tickets" class="btn btn-outline-primary">Vedi report</a>
          </div>
        </div>

      </div>
    </div>


  </div>

</div>

<%@include file="assets/include/javaScript.html"%>
</body>
</html>
