<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="it">
<head>
    <%@include file="assets/include/head.html" %>
    <title>Cambia password</title>
</head>
<body>

<%
    // Controllo se l'utente ha già effettuato l'accesso
    // se l'ha già effettuato lo indirizzo alla sua home
    Cookie[] cookies = request.getCookies();
    if(cookies != null)
    {
        for (int i = 0; i < cookies.length; i++)
        {
            if ((cookies[i].getName().equals("user_type")) && (cookies[i].getValue() != null))
            {
                response.sendRedirect("/home");
            }
        }
    }

    // controllo se è già stato inviato un form con dati errati
    String err = (String)request.getAttribute("errore");
    String succ = (String)request.getAttribute("successo");
%>
<nav class="navbar navbar-expand-lg navbar-light px-5">
  <a class="navbar-brand" href="/"><span class="brand-color"><span class="logo">Sanità</span>online</span></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav ml-auto">
        <a class="nav-item nav-link" href="login.jsp">Accedi <i class="las la-user-circle"></i></a>
    </div>
  </div>
</nav>

<div class="container-fluid pt-2 px-2 px-sm-5">
    <div class="row">
        <div class="col-md">
            <div class="row h-100 justify-content-center align-items-center">
                <div class="col-9">
                    <%
                      if(succ != null){
                    %>
                    <div class="row pb-3 pb-sm-5">
                      <div class="col-auto">
                        <h3 class="text-success font-family-work-sans">Password cambiata con successo</h3>
                      </div>
                    </div>
                    <%
                      }
                    %>
                    <form class="form-signin form-signin-cambia-password" method="post" action="cambiaPassword">
                        <h3 class="mb-3">Inserisci i nuovi dati</h3>
                        <%
                           // se il form era invalid
                           if(err!=null){
                        %>
                        <label for="inputEmail" class="sr-only">Indirizzo email</label>
                        <input name="email" type="email" id="inputEmail" class="form-control is-invalid" placeholder="Indirizzo email" required autofocus>
                        <label for="vecchiaPassword" class="sr-only">Vecchia Password</label>
                        <input name="vecchiaPassword" type="password" id="vecchiaPassword" class="form-control is-invalid" placeholder="Inserisci vecchia password" required>
                        <label for="nuovaPassword" class="sr-only">Nuova Password</label>
                        <input name="nuovaPassword" type="password" id="nuovaPassword" class="form-control is-invalid" placeholder="Inserisci nuova password" required>
                        <div class="invalid-feedback mb-3">
                          email o password sbagliati
                        </div>
                        <%
                           }else{
                        %>
                        <label for="inputEmail" class="sr-only">Indirizzo email</label>
                        <input name="email" type="email" id="inputEmail" class="form-control" placeholder="Indirizzo email" required autofocus>
                        <label for="vecchiaPassword" class="sr-only">Vecchia Password</label>
                        <input name="vecchiaPassword" type="password" id="vecchiaPassword" class="form-control " placeholder="Inserisci vecchia password" required>
                        <label for="nuovaPassword" class="sr-only">Nuova Password</label>
                        <input name="nuovaPassword" type="password" id="nuovaPassword" class="form-control " placeholder="Inserisci nuova password" required>
                        <%
                           }
                        %>
                        <button class="btn btn-lg btn-primary btn-block mt-3" type="submit">Cambia Password</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md">
            <img src="assets/img/login.svg" class="float-left img-fluid p-5">
        </div>
    </div>
</div>

<%@include file="assets/include/javaScript.html" %>
</body>
</html>
