<!-- aggiungiEsame.jsp
Permette al dottore di creare un nuovo esame-->

<%@ page import="classes.tipologiaEsame" %>
<%@ page import="classes.visita" %>

<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>

<%@include file="assets/include/loginCheck.jsp" %>
<%@include file="assets/include/monthFormatter.jsp" %>

<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="it">
<head>
    <%@include file="assets/include/head.html" %>
    <%@include file="assets/include/javaScript.html" %>
    <!-- bootstrap-select -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>

    <title>Prescrivi Esame</title>
</head>
<body>
<%@include file="assets/include/nav.jsp" %>

<div class="container-fluid pt-5 px-3 px-sm-5">
  <div class="row align-items-center data-header pb-5">
    <div class="col-sm-6 offset-sm-3 wrap">
      Visita di <br>
      <span class="data-header-nome">
        ${visita.getNomePaziente()} ${visita.getCognomePaziente()}
      </span> <br>
      del
      <span class="data-header-data">
        ${visita.getTimestamp().getDayOfMonth()}-${visita.getTimestamp().getMonthValue()}-${visita.getTimestamp().getYear()}
      </span> <br>
      alle
      <span class="data-header-ora">
        ${visita.getTimestamp().getHour()}:${String.format("%02d", visita.getTimestamp().getMinute())} 
      </span>
    </div>
  </div>
  <div class="row align-items-center data-content pb-5">
    <div class="col-md-8 offset-md-2 col-lg-6 offset-lg-3">
      <h2>Prescrivi un esame</h2>
      <form method="post" action="aggiungiEsame">
          <div class="form-group">
           <label for="tipologia">Tipologia</label>
           <select class="form-control pb-3 selectpicker show-tick" data-live-search="true" name='e_id' id="tipologia">
             <%
                 List<tipologiaEsame> tipologieEsami = (ArrayList<tipologiaEsame>)request.getAttribute("tipologieEsami");
                 for(tipologiaEsame tipologiaEsame : tipologieEsami) {
                     out.print("<option value=" + tipologiaEsame.getId() + ">" + tipologiaEsame.getNome() + "</option>");
                 }
             %>
           </select>
          </div>
          <input type="hidden" value="${visita.getId()}" name="v_id">
          <div class="row">
            <div class="col-auto ml-auto">
              <button class="btn btn-primary" type="submit">Prescrivi</button>
            </div>
          </div>
      </form>
    </div>
  </div>

</div>


</body>
</html>
