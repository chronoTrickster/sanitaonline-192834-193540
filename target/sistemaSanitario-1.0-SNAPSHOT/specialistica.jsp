<!-- specialistica.jsp -->

<%@ page import="classes.specialistica" %>
<%@ page import="classes.paziente" %>
<%@ page import="classes.dottore" %>

<% specialistica specialistica = (specialistica)request.getAttribute("specialistica"); %>

<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>

<%@include file="assets/include/loginCheck.jsp" %>
<%@include file="assets/include/monthFormatter.jsp" %>

<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="it">
<head>
    <%@include file="assets/include/head.html" %>
    <title>Specialistica</title>
</head>
<body>
<%@include file="assets/include/nav.jsp" %>

<div class="container-fluid pt-5 px-3 px-sm-5">
  <div class="row align-items-center data-header pb-5">
    <div class="col-sm-6 offset-sm-3 wrap">
      Visita specialistica <br>
      <span class="data-header-nome">
        ${specialistica.getTipoSpecialistica()}
      </span> <br>
    </div>
  </div>

  <div class="row align-items-center data-content pb-5">
    <div class="col-md-9 col-lg-8 mx-auto lead wrap">
      <div class="row pb-2">
        <div class="col-12 col-sm-4 col-md-3 font-weight-normal">
          Paziente
        </div>
        <div class="col-12 col-sm-8 col-md-9">
          ${paziente.getNome()} ${paziente.getCognome()}
        </div>
        <div class="col-12 col-sm-4 col-md-3 font-weight-normal">
          Codice dottore
        </div>
        <div class="col-12 col-sm-8 col-md-9">
          ${String.format("%05d", specialistica.getDottoreId())}
        </div>
      </div>

      <%
        if(!specialistica.getPagato()){
          if(usertype.equals("paziente")){
      %>
      <div class="row pt-5 pr-5">
        <div class="col-12 col-md-auto ml-md-auto">
          <form class="form" action="pagaSpecialistica" method="post">
            <input type="hidden" name="id" value="${specialistica.getId()}">
            <button type="submit" class="btn btn-outline-primary btn-lg">Paga visita €50,00</button>
          </form>
        </div>
      </div>
      <%
        }else{
      %>
      <div class="row pt-5">
        <div class="col-12 font-weight-normal wrap">
          La visita specialistica deve essere pagata per essere svolta. <br>
          Il paziente deve accedere al portale per pagare la visita.
        </div>
      </div>
      <%
        }
      }else if(specialistica.getAttiva()){
        if(usertype.equals("dottore")){
      %>
      <div class="row align-items-center pt-5 pb-5">
        <div class="col-12">
          <h2>Anamnesi</h2>
          <form method="post" action="anamnesiSpecialistica">
              <div class="form-group">
                <label for="descrizione">Inserisci l'anamnesi o l'esito della visita</label>
                <textarea class="form-control" id="descrizione" name="anamnesi" rows="5"></textarea>
              </div>
              <input type="hidden" value="${specialistica.getId()}" name="id">
              <div class="row">
                <div class="col-auto ml-auto">
                  <button class="btn btn-primary" type="submit">Concludi</button>
                </div>
              </div>
          </form>
        </div>
      </div>
      <%
        }else{
      %>
      <div class="row pt-5">
        <div class="col-12 font-weight-normal wrap">
          La visita specialistica è stata pagata. <br>
          Il paziente si deve recare dal proprio dottore di base per svolgere la visita.
        </div>
      </div>
      <%
        }
      }else{
      %>
      <div class="row pt-5 pb-1">
        <h4>Risultati</h4>
      </div>
      <div class="row pb-2">
        <div class="col-12 col-sm-4 col-md-3 font-weight-normal">
          Data
        </div>
        <div class="col-12 col-sm-8 col-md-9">
          ${specialistica.getTimestamp().getDayOfMonth()}-${specialistica.getTimestamp().getMonthValue()}-${specialistica.getTimestamp().getYear()}
        </div>
      </div>
      <div class="row pb-2">
        <div class="col-12 col-sm-4 col-md-3 font-weight-normal">
          Ora
        </div>
        <div class="col-12 col-sm-8 col-md-9">
          ${specialistica.getTimestamp().getHour()}:${String.format("%02d", specialistica.getTimestamp().getMinute())}
        </div>
      </div>
      <div class="row pb-2">
        <div class="col-12 col-sm-4 col-md-3 font-weight-normal">
          Anamnesi
        </div>
        <div class="col-12 col-sm-8 col-md-9">
          ${specialistica.getAnamnesi()}
        </div>
      </div>
      <%
        }
      %>

    </div>
  </div>
</div>

<%@include file="assets/include/javaScript.html"%>
</body>
</html>
