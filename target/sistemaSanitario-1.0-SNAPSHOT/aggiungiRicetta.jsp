<!-- aggiungiRicetta.jsp
Permette al dottore di creare una nuova ricetta-->
<%@ page import="classes.ricetta"%>
<%@ page import="classes.visita" %>

<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>

<%@include file="assets/include/loginCheck.jsp" %>
<%@include file="assets/include/monthFormatter.jsp" %>

<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="it">
<head>
    <%@include file="assets/include/head.html" %>
    <title>Prescrivi Ricetta</title></head>
<body>
<%@include file="assets/include/nav.jsp"%>

<div class="container-fluid pt-5 px-3 px-sm-5">
  <div class="row align-items-center data-header pb-5">
    <div class="col-sm-6 offset-sm-3 wrap">
      Visita di <br>
      <span class="data-header-nome">
        ${visita.getNomePaziente()} ${visita.getCognomePaziente()}
      </span> <br>
      del
      <span class="data-header-data">
        ${visita.getTimestamp().getDayOfMonth()}-${visita.getTimestamp().getMonthValue()}-${visita.getTimestamp().getYear()}
      </span> <br>
      alle
      <span class="data-header-ora">
        ${visita.getTimestamp().getHour()}:${String.format("%02d", visita.getTimestamp().getMinute())}
      </span>
    </div>
  </div>
  <div class="row align-items-center data-content pb-5">
    <div class="col-md-6 offset-md-3">
      <h2>Prescrivi la ricetta</h2>
      <form method="post" action="aggiungiRicetta">
          <div class="form-group">
            <label for="descrizione">Medicinali</label>
            <textarea class="form-control" id="descrizione" name="medicinali" rows="5"></textarea>
          </div>
          <input type="hidden" value="${visita.getId()}" name="id">
          <div class="row">
            <div class="col-auto ml-auto">
              <button class="btn btn-primary" type="submit">Prescrivi</button>
            </div>
          </div>
      </form>
    </div>
  </div>
</div>

<%@include file="assets/include/javaScript.html"%>
</body>
</html>
