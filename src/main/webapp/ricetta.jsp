<!-- ricetta.jsp
Le informazioni riguardo una determinata ricetta-->

<%@ page import="classes.ricetta" %>
<%@ page import="classes.paziente" %>
<%@ page import="classes.dottore" %>

<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>

<%@include file="assets/include/loginCheck.jsp" %>
<%@include file="assets/include/monthFormatter.jsp" %>

<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="it">
<head>
    <%@include file="assets/include/head.html" %>
    <title>Ricetta</title>
</head>
<body>
<%@include file="assets/include/nav.jsp" %>

<div class="container-fluid pt-5 px-3 px-sm-5">
  <div class="row align-items-center data-header pb-5">
    <div class="col-sm-6 offset-sm-3 wrap">
      Ricetta Nr <br>
      <span class="data-header-numero monospace">
        ${String.format("%015d", ricetta.getId())}
      </span> <br>
      del
      <span class="data-header-data">
        ${ricetta.getTimestamp().getDayOfMonth()}-${ricetta.getTimestamp().getMonthValue()}-${ricetta.getTimestamp().getYear()}
      </span> <br>
      alle
      <span class="data-header-ora">
        ${ricetta.getTimestamp().getHour()}:${String.format("%02d", ricetta.getTimestamp().getMinute())}
      </span>
    </div>
  </div>

  <!-- fix data header class -->
  <div class="row align-items-center data-content pb-5">
    <div class="col-md-9 col-lg-8 mx-auto lead wrap">
      <div class="row pb-2">
        <div class="col-12 col-sm-4 col-md-3 font-weight-normal">
          Codice dottore
        </div>
        <div class="col-12 col-sm-8 col-md-9">
          ${String.format("%05d", ricetta.getDottoreId())}
        </div>
      </div>
      <div class="row pb-2">
        <div class="col-12 col-sm-4 col-md-3 font-weight-normal">
          Dottore
        </div>
        <div class="col-12 col-sm-8 col-md-9">
          ${dottore.getNome()} ${dottore.getCognome()}
        </div>
      </div>
      <div class="row pb-2">
        <div class="col-12 col-sm-4 col-md-3 font-weight-normal">
          Paziente
        </div>
        <div class="col-12 col-sm-8 col-md-9">
          ${paziente.getNome()} ${paziente.getCognome()}
        </div>
      </div>
      <div class="row pb-2">
        <div class="col-12 col-sm-4 col-md-3 font-weight-normal">
          Stato
        </div>
        <div class="col-12 col-sm-8 col-md-9">
          ${ricetta.getAttiva() == true ? 'Prescritta' : 'Erogata'}
        </div>
      </div>
      <div class="row pb-2">
        <div class="col-12 col-sm-4 col-md-3 font-weight-normal">
          C.F.
        </div>
        <div class="col-12 col-sm-8 col-md-9">
          ${paziente.getCodiceFiscale()}
        </div>
      </div>
      <div class="row pb-2">
        <div class="col-12 col-sm-4 col-md-3 font-weight-normal">
          Medicinali
        </div>
        <div class="col-12 col-sm-8 col-md-9">
          ${ricetta.getMedicinali()}
        </div>
      </div>

      <div class="row pt-5 pr-5">
        <div class="col-auto ml-auto">
          <a role="button" href="/stampaRicetta?id=${ricetta.getId()}" type="button" name="button" class="btn btn-outline-primary">Stampa ricetta</a>
        </div>
      </div>
    </div>
  </div>
</div>

<%@include file="assets/include/javaScript.html"%>
</body>
</html>
