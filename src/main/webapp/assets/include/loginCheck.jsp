
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>

<%
    // L'utente non può accedere a questa pagina senza prima aver loggato.
    // Controlliamo che sia loggato.
    String usertype="";
    String id_cookie="";
    Cookie[] cookies = request.getCookies();
    if(cookies != null)
    {
        boolean redirect = true;
        for (int i = 0; i < cookies.length; i++)
        {
            if ((cookies[i].getName().equals("user_type")) && (cookies[i].getValue() != null))
            {
                redirect = false;
                usertype = cookies[i].getValue();
            }
            if ((cookies[i].getName().equals("id")) && (cookies[i].getValue() != null))
            {
                id_cookie = cookies[i].getValue();
            }
        }
        if (redirect)
        {
            response.sendRedirect("login.jsp");
        }
    }
    else
    {
        response.sendRedirect("login.jsp");
    }
%>
