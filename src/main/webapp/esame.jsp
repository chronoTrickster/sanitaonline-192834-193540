<!-- esame.jsp
Le informazioni riguardo una determinato esame-->

<%@ page import="classes.esame" %>
<%@ page import="classes.paziente" %>
<%@ page import="classes.dottore" %>

<% esame esame = (esame)request.getAttribute("esame"); %>

<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>

<%@include file="assets/include/loginCheck.jsp" %>
<%@include file="assets/include/monthFormatter.jsp" %>

<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="it">
<head>
    <%@include file="assets/include/head.html" %>
    <title>Esame</title>
</head>
<body>
<%@include file="assets/include/nav.jsp" %>

<div class="container-fluid pt-5 px-3 px-sm-5">
  <div class="row align-items-center data-header pb-5">
    <div class="col-sm-6 offset-sm-3 wrap">
      Esame <br>
      <span class="data-header-nome">
        ${esame.getTipoEsame()}
      </span> <br>
    </div>
  </div>

  <div class="row align-items-center data-content pb-5">
    <div class="col-md-9 col-lg-8 mx-auto lead wrap">
      <div class="row pb-2">
        <div class="col-12 col-sm-4 col-md-3 font-weight-normal">
          Paziente
        </div>
        <div class="col-12 col-sm-8 col-md-9">
          ${paziente.getNome()} ${paziente.getCognome()}
        </div>
      </div>

      <%
        if(!esame.getPagato()){
          if(usertype.equals("paziente")){
      %>
      <div class="row pt-5 pr-5">
        <div class="col-12 col-md-auto ml-md-auto">
          <form class="form" action="pagaEsame" method="post">
            <input type="hidden" name="id" value="${esame.getId()}">
            <button type="submit" class="btn btn-outline-primary btn-lg">Paga esame €11,00</button>
          </form>
        </div>
      </div>
      <%
        }else{
      %>
      <div class="row pt-5">
        <div class="col-12 font-weight-normal wrap">
          L'esame deve essere pagato per essere svolto. <br>
          Il paziente deve accedere al portale per pagare l'esame.
        </div>
      </div>
      <%
        }
      }else if(esame.getAttiva()){
        if(usertype.equals("ssp")){
      %>
      <div class="row align-items-center pt-5 pb-5">
        <div class="col-12">
          <h2>Anamnesi</h2>
          <form method="post" action="anamnesiEsame">
              <div class="form-group">
                <label for="descrizione">Inserisci l'anamnesi o l'esito dell'esame</label>
                <textarea class="form-control" id="descrizione" name="anamnesi" rows="5"></textarea>
              </div>
              <input type="hidden" value="${esame.getId()}" name="id">
              <div class="row">
                <div class="col-auto ml-auto">
                  <button class="btn btn-primary" type="submit">Concludi</button>
                </div>
              </div>
          </form>
        </div>
      </div>
      <%
        }else{
      %>
      <div class="row pt-5">
        <div class="col-12 font-weight-normal wrap">
          L'esame è stato pagato. <br>
          Il paziente si deve recare al proprio Servizio Sanitario Provinciale per svolgere l'esame.
        </div>
      </div>
      <%
        }
      }else{
      %>
      <div class="row pt-5 pb-1">
        <h4>Risultati</h4>
      </div>
      <div class="row pb-2">
        <div class="col-12 col-sm-4 col-md-3 font-weight-normal">
          Data
        </div>
        <div class="col-12 col-sm-8 col-md-9">
          ${esame.getTimestamp().getDayOfMonth()}-${esame.getTimestamp().getMonthValue()}-${esame.getTimestamp().getYear()}
        </div>
      </div>
      <div class="row pb-2">
        <div class="col-12 col-sm-4 col-md-3 font-weight-normal">
          Ora
        </div>
        <div class="col-12 col-sm-8 col-md-9">
          ${esame.getTimestamp().getHour()}:${String.format("%02d", esame.getTimestamp().getMinute())}
        </div>
      </div>
      <div class="row pb-2">
        <div class="col-12 col-sm-4 col-md-3 font-weight-normal">
          Anamnesi
        </div>
        <div class="col-12 col-sm-8 col-md-9">
          ${esame.getAnamnesi()}
        </div>
      </div>
      <%
        }
      %>


    </div>
  </div>
</div>

<%@include file="assets/include/javaScript.html"%>
</body>
</html>
