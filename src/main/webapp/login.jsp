<%--
  login.jsp
--%>

<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="it">
<head>
    <%@include file="assets/include/head.html" %>
    <title>Login</title>
</head>
<body>

<%
    // Controllo se l'utente ha già effettuato l'accesso
    // se l'ha già effettuato lo indirizzo alla sua home
    Cookie[] cookies = request.getCookies();
    if(cookies != null)
    {
        for (int i = 0; i < cookies.length; i++)
        {
            if ((cookies[i].getName().equals("user_type")) && (cookies[i].getValue() != null))
            {
                response.sendRedirect("/home");
            }
        }
    }

    // controllo se è già stato inviato un form con dati errati
    String err = (String)request.getAttribute("errore");
%>

<!-- Navbar  -->
<nav class="navbar navbar-expand-lg navbar-light navbar-custom-background px-5">
  <a class="navbar-brand" href="/"><span class="brand-color"><span class="logo">Sanità</span>online</span></a>
</nav>

<!-- login -->

<div class="container-fluid pt-2 px-2 px-sm-5">
  <div class="row">
    <div class="col-md">
      <div class="row h-100 justify-content-center align-items-center">
        <div class="col-9">
          <% %>
          <form class="form-signin form-signin-login" method="post" action="login">
           <h3 class="mb-3">Inserisci i tuoi dati</h3>
           <%
              // se il form era invalid
              if(err!=null){
           %>
             <label for="inputEmail" class="sr-only">Indirizzo email</label>
             <input name="email" type="email" id="inputEmail" class="form-control is-invalid" placeholder="Indirizzo email" required autofocus>
             <label for="inputPassword" class="sr-only">Password</label>
             <input name="password" type="password" id="inputPassword" class="form-control is-invalid" placeholder="Password" required>
             <div class="invalid-feedback mb-3">
               email o password sbagliati
             </div>
           <%
              }else{
           %>
             <label for="inputEmail" class="sr-only">Indirizzo email</label>
             <input name="email" type="email" id="inputEmail" class="form-control" placeholder="Indirizzo email" required autofocus>
             <label for="inputPassword" class="sr-only">Password</label>
             <input name="password" type="password" id="inputPassword" class="form-control " placeholder="Password" required>
           <%
              }
           %>
           <div class="row mb-3 mt-1 px-3">
             <div class="col-12 col-sm-6 custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="ricordami" name="ricordami" value="ricordami">
              <label class="custom-control-label" for="ricordami">Ricordami</label>
            </div>
             <div class="col-12 col-sm-6 text-right">
               <a href="cambiaPassword">Cambia password</a>
             </div>
           </div>

           <button class="btn btn-lg btn-primary btn-block" type="submit">Accedi</button>
         </form>
        </div>
      </div>
    </div>
    <div class="col-md">
      <img src="assets/img/login.svg" class="float-left img-fluid p-5">
    </div>
  </div>
</div>

<%@include  file="assets/include/javaScript.html" %>
</body>
</html>
