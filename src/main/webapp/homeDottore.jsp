<%@ page import="classes.paziente" %>
<%@ page import="classes.visita" %>
<%@ page import="classes.dottore" %>

<%@ page import="java.time.LocalDateTime" %>
<%@ page import="java.time.ZoneOffset" %>

<%@include file="assets/include/loginCheck.jsp" %>
<%@include file="assets/include/monthFormatter.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="it">
<head>
    <%@include file="assets/include/head.html" %>
    <%@include file="assets/include/javaScript.html" %>
    <!-- bootstrap-select -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
    <title>Homepage</title>
</head>
<body>
    <%@include file="assets/include/nav.jsp" %>

    <div class="container-fluid pt-5 px-3 px-sm-5">
      <!-- header row: nome e informazioni -->
      <div class="row wrap text-center text-lg-left pb-5">
        <div class="col-lg-4 pb-4 p-lg-5 user-nome">
          <h5>${dottore.getSesso() == 'M' ? 'Dottore' : 'Dottoressa'}</h5>
          <h1>${dottore.getNome()} ${dottore.getCognome()}</h1>
        </div>
        <div class="col-lg-8 pb-5 p-lg-5 lead text-left">
          <div class="row">
            <div class="col-12 col-sm-6 text-lg-right font-weight-normal">
              Codice dottore
            </div>
            <div class="col-12 col-sm-6">
              ${String.format("%05d", dottore.getId())}
            </div>
          </div>
          <div class="row">
            <div class="col-12 col-sm-6 text-lg-right font-weight-normal">
              Provincia
            </div>
            <div class="col-12 col-sm-6">
              ${dottore.getProvincia()}
            </div>
          </div>
          <div class="row">
            <div class="col-12 col-sm-6 text-lg-right font-weight-normal">
              Sesso
            </div>
            <div class="col-12 col-sm-6">
              ${dottore.getSesso() == 'M' ? 'Maschio' : 'Femmina'}
            </div>
          </div>
        </div>
      </div>

      <!-- content row: agenda e parco pazienti -->
      <div class="row">
        <!-- AGENDA -->
        <div class="agenda col-12 col-lg-4 pb-3 px-lg-5 order-lg-2">

          <h1 class="text-lg-center pb-3">Agenda</h1>

          <div class="container p-0 m-0">
            <%
               List<visita> visite = (ArrayList<visita>)request.getAttribute("visite");
               int contVisite = 0;
               if(visite.size() == 0){
                 out.print("Nessuna visita in programma.");
               }else{
                 for(visita visita : visite)
                 {
                   if(visita.getTimestamp().compareTo(LocalDateTime.now()) >= 0){
                     contVisite++;
            %>
            <div class="container-fluid m-0 p-0 list-links-container">
              <%
              out.print("<a href='visita?id=" + visita.getId() + "'>");
              %>
                <div class="row align-items-center pt-3 pb-2 border-bottom list-link">
                  <div class="date col-2 col-md-1 col-lg-3 text-lg-right">
                    <p class="month text-secondary">
                      <%
                        out.print(formatMonth(visita.getTimestamp().getMonthValue()));
                      %>
                    </p>
                    <p class="day font-weight-bold">
                      <%
                        out.print(visita.getTimestamp().getDayOfMonth());
                      %>
                    </p>
                    <p class="year text-secondary">
                      <%
                        out.print(visita.getTimestamp().getYear());
                      %>
                    </p>
                  </div>
                  <div class="time col-2 col-md-1 col-lg-2">
                    <%
                      out.print(visita.getTimestamp().getHour() + ":" + String.format("%02d", visita.getTimestamp().getMinute()));
                    %>
                  </div>
                  <div class="agenda-patient col-8 col-md-10 col-lg-7 text-lg-left">
                    <%
                      out.print(visita.getNomePaziente() + " " + visita.getCognomePaziente());
                    %>
                  </div>
                </div>
              <%
              out.print("</a>");
              %>
            </div>
            <%
                  }
                }
                if(contVisite == 0) {
                  out.print("Nessuna visita in programma.");
                }
              }
            %>
          </div>
        </div>
        <!-- LISTA PAZIENTI -->
        <div class="col-12 col-lg-8 order-lg-1">
          <!-- h1 -->
          <h1 class="pb-5">Pazienti</h1>

          <%
            List<paziente> pazienti = (ArrayList<paziente>)request.getAttribute("pazienti");
            if(pazienti.size() > 0){
          %>
          <div class="row px-md-5">
            <h5>Cerca pazienti</h5>
          </div>
          <form method="post" action="cercaPaziente" >
            <div class="row px-md-5 pb-5">
              <div class="form-group col-12 col-md-8 col-xl-6">
                <select class="form-control selectpicker show-tick" data-live-search="true" name='id' id="pazienti">
                   <%
                       for(paziente paziente : pazienti)
                       {
                             out.print("<option value=" + paziente.getId() + ">");
                             out.print(paziente.getNome() + " " + paziente.getCognome());
                             out.print("</option>");
                       }
                   %>
                </select>
              </div>
              <div class="col-12 col-md-auto">
                <button class="btn btn-outline-primary btn-block" type="submit">Vedi paziente</button>
              </div>
            </div>
          </form>
          <%
            }
          %>

          <!-- ultimavista/ricetta -->
          <div class="row p-md-3 text-center">
            <div class="col-md-2 offset-md-8 d-none d-md-block d-lg-block d-xl-block">
              Ultima visita
            </div>
            <div class="col-md-2 d-none d-md-block d-lg-block d-xl-block">
              Ultima ricetta
            </div>
          </div>

          <!-- patient list -->
          <%
              if(pazienti.size() == 0){
                out.print("Nessun paziente trovato.");
              }else{
                for(paziente paziente : pazienti)
                {
          %>

          <div class="container-fluid m-0 p-0 patients list-links-container">
            <%
                out.print("<a href='paziente?id=" + paziente.getId() + "'>");
            %>
              <div class="row shadow-sm p-3 mb-3 bg-white rounded align-items-center text-center list-link">
                <div class="col-12 pb-2 col-md-2 pb-md-0">
                  <%
                    out.print("<img src='" + paziente.getPropic() + "' class='img-fluid rounded-circle'>");
                  %>
                </div>
                <div class="col-12 pb-2 col-md-6 pb-md-0 text-md-left font-weight-bold">
                  <%
                      out.print(paziente.getNome() + " " + paziente.getCognome());
                  %>
                </div>
                <div class="col-6 col-md-2 text-secondary">
                  <%
                    if(paziente.getUltimaVisita() != null )
                    {
                      out.print(paziente.getUltimaVisita().getDayOfMonth() + " " + formatMonth(paziente.getUltimaVisita().getMonthValue()) + " " + paziente.getUltimaVisita().getYear() + "<br>");
                      out.print(paziente.getUltimaVisita().getHour() + ":" + String.format("%02d", paziente.getUltimaVisita().getMinute()));
                    }
                    else
                    {
                      out.print("-");
                    }
                  %>
                  <span class="font-weight-light d-md-none"><br>ultima visita<span>
                </div>
                <div class="col-6 col-md-2 text-secondary">
                  <%
                    if(paziente.getUltimaRicetta() != null )
                    {
                      out.print(paziente.getUltimaRicetta().getDayOfMonth() + " " + formatMonth(paziente.getUltimaRicetta().getMonthValue()) + " " + paziente.getUltimaRicetta().getYear() + "<br>");
                      out.print(paziente.getUltimaRicetta().getHour() + ":" + String.format("%02d", paziente.getUltimaRicetta().getMinute()));
                    }
                    else
                    {
                      out.print("-");
                    }
                  %>
                  <span class="font-weight-light d-md-none"><br>ultima ricetta<span>
                </div>
              </div>
            </a>
          </div>

          <%
              }
            }
          %>

      </div>
    </div>

</body>
</html>
