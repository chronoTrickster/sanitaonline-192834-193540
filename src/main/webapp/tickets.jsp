<%@ page import="classes.paziente" %>
<%@ page import="classes.ticketEsame" %>
<%@ page import="classes.ticketSpecialistica" %>

<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>

<%@include file="assets/include/loginCheck.jsp" %>
<%@include file="assets/include/monthFormatter.jsp" %>

<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="it">
<head>
    <title>Tickets</title>

    <%@include file="assets/include/head.html" %>
    <%@include file="assets/include/javaScript.html"%>
    <%-- for datatables --%>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <%-- for pdf print --%>
    <script src="assets/printjs/print.min.js"></script>

</head>
<body>
  <nav class="navbar navbar-expand-lg navbar-light px-5">
      <a class="navbar-brand" href="home"><span class="brand-color"><span class="logo">Sanità</span>online</span></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
          <div class="navbar-nav ml-auto">
              <li class="nav-item dropdown" href="#">
                  <a class="nav-link dropdown-toggle" href="home" id="navbarDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Ciao
                      <span class="font-weight-bold">
                      ${cookie.nome.value}
                  </span>
                      <i class="las la-user-circle"></i>
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item" href="home">Homepage</a>
                      <a class="dropdown-item" href="/">ANSS</a>
                      <a class="dropdown-item" href="logout">Logout</a>
                  </div>
              </li>
          </div>
      </div>
  </nav>

  <div class="row m-0 p-0">
    <div class="col pl-5 pt-2">
      <button class="btn btn-outline-dark" id="indietro"> < </button>
    </div>
  </div>

<div class="container-fluid pt-2 px-sm-5">
  <div class="row align-items-center data-header pb-5">
    <div class="col-sm-6 offset-sm-3 wrap">
      <span class="data-header-nome">
        Report Tickets
      </span> <br>
      di ${paziente.getNome()} ${paziente.getCognome()}
    </div>
  </div>

  <div class="row align-items-center pb-5">
    <div class="col-12 col-md-10 offset-md-1">
      <h2 class="font-family-work-sans">Tickets di esami</h2>
      <%
        List<ticketEsame> ticketsEsami = (ArrayList<ticketEsame>)request.getAttribute("ticketEsami");
        if(ticketsEsami.size() == 0){
          out.print("Nessun ticket di esami presente.");
        }else{
      %>
        <table id="ticketsEsami" class="table table-hover table-striped">
          <thead class="thead-dark">
              <tr>
                  <th>Tipologia</th>
                  <th>Stato</th>
              </tr>
          </thead>
          <tbody>
            <%
              for(ticketEsame ticketEsami : ticketsEsami)
              {
            %>
              <tr>
                  <td>
                  <%
                    out.print(ticketEsami.getNome());
                  %>
                  </td>
                  <td>
                  <%
                    if(!ticketEsami.getPagato()){
                      out.print("da pagare");
                    }else{
                      out.print("pagato il " + ticketEsami.getDataPagamento().getDayOfMonth() + " " + formatMonth(ticketEsami.getDataPagamento().getMonthValue()) + " " + ticketEsami.getDataPagamento().getYear());
                    }
                  %>
                  </td>
              </tr>
            <%
                }
            %>
          </tbody>
        </table>

        <div class="row pb-3">
          <div class="col-12 col-md-auto ml-md-auto">
            <button type="button" class="btn btn-primary" onclick="printJS({
              printable: 'ticketsEsami',
              type: 'html',
              documentTitle: 'ticketsEsami${paziente.getNome()}${paziente.getCognome()}',
              targetStyles: ['*']
            })">
              Scarica PDF
            </button>
          </div>
        </div>
      <%
        }
      %>
    </div>

    <div class="col-12 col-md-10 offset-md-1">
      <h2 class="font-family-work-sans">Tickets di visite specialistiche</h2>
      <%
        List<ticketSpecialistica> ticketsSpecialistiche = (ArrayList<ticketSpecialistica>)request.getAttribute("ticketSpecialistiche");
        if(ticketsSpecialistiche.size() == 0){
          out.print("Nessun ticket di visite specialistiche presente.");
        }else{
      %>
        <table id="ticketsSpecialistica" class="table table-hover table-striped">
          <thead class="thead-dark">
              <tr>
                  <th>Tipologia</th>
                  <th>Stato</th>
              </tr>
          </thead>
          <tbody>
          <%
            for(ticketSpecialistica ticketSpecialistiche : ticketsSpecialistiche)
            {
          %>
              <tr>
                  <td>
                  <%
                    out.print(ticketSpecialistiche.getNome());
                  %>
                  </td>
                  <td>
                  <%
                    if(!ticketSpecialistiche.getPagato()){
                      out.print("da pagare");
                    }else{
                      out.print("pagato il " + ticketSpecialistiche.getDataPagamento().getDayOfMonth() + " " + formatMonth(ticketSpecialistiche.getDataPagamento().getMonthValue()) + " " + ticketSpecialistiche.getDataPagamento().getYear());
                    }
                  %>
                  </td>
              </tr>
            <%
                }
            %>
          </tbody>
        </table>

        <div class="row pb-3">
          <div class="col-12 col-md-auto ml-md-auto">
            <button type="button" class="btn btn-primary" onclick="printJS({
              printable: 'ticketsSpecialistica',
              type: 'html',
              documentTitle: 'ticketsSpecialistiche${paziente.getNome()}${paziente.getCognome()}',
              targetStyles: ['*']
            })">
              Scarica PDF
            </button>
          </div>
        </div>
      <%
        }
      %>
    </div>

  </div>
</div>

  <script>
    $(document).ready( function () {
      $('#ticketsEsami').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Italian.json"
        }
      });
      $('#ticketsSpecialistica').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Italian.json"
        }
      });
    } );
  </script>
</body>
</html>
