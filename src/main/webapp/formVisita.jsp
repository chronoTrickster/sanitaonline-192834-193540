<%--
form per prenotare una visita
--%>
<%@ page import="classes.dottore" %>

<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.time.LocalDateTime" %>
<%@ page import="java.time.ZoneOffset" %>

<%@include file="assets/include/loginCheck.jsp" %>
<%@include file="assets/include/monthFormatter.jsp" %>

<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="it">
<head>
    <%@include file="assets/include/head.html" %>
    <title>Nuova Visita</title>
</head>
<body>
<%@include file="assets/include/nav.jsp" %>

<div class="container-fluid pt-5 px-3 px-sm-5">
  <div class="row align-items-center data-header pb-5">
    <div class="col-sm-6 offset-sm-3 wrap">
      Prenota una visita dal <br>
      <span class="data-header-nome">
        Dr. ${dottore.getNome()} ${dottore.getCognome()}
      </span>
    </div>
  </div>

  <div class="row align-items-center data-content pb-5">
    <div class="col-md-8 offset-md-2 col-lg-6 offset-lg-3">
      <h2>Scegli la data</h2>
      <form method="post" action="nuovaVisita">
          <div class="form-group">
             <%
             String data = LocalDateTime.now().toString();
             data = data.substring(0,16);
             out.print("<input type='datetime-local' name='data' id='data' class='form-control' required value='" + data + "' min='" + data + "'></input>");
             %>
          </div>
          <div class="row">
            <div class="col-auto ml-auto">
              <button class="btn btn-primary" type="submit">Prenota</button>
            </div>
          </div>
      </form>
    </div>
  </div>
  <%-- <form method="post" action="nuovaVisita">
      PLACEHOLDER INUTILE:<input type="text" name="email" /><br/>
      COME SOPRA:<input type="text" name="password" /><br/>
      <input type="submit" value="nuovaVisita" />
  </form> --%>

  </div>

<%@include file="assets/include/javaScript.html" %>
</body>
</html>
