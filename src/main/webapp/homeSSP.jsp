<%@ page import="classes.esame" %>
<%@ page import="classes.ssp" %>

<%@ page import="java.time.LocalDateTime" %>
<%@ page import="java.time.ZoneOffset" %>

<%@include file="assets/include/loginCheck.jsp" %>
<%@include file="assets/include/monthFormatter.jsp" %>
<%@include file="assets/include/provinciaFormatter.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="it">
<head>
    <%@include file="assets/include/head.html" %>
    <title>Homepage</title>

    <%@include file="assets/include/javaScript.html"%>
    <%-- for datatables --%>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>

</head>
<body>
  <%@include file="assets/include/nav.jsp" %>

  <div class="container-fluid pt-5 px-sm-5">
    <div class="row wrap text-center text-lg-left pb-5">
      <div class="col-lg-4 pb-4 p-lg-5 user-nome">
        <h1>S.S.P.
         <%
         ssp ssp = (ssp)request.getAttribute("ssp");
         out.print(formatProvincia(ssp.getProvincia()));
         %>
       </h1>
      </div>
    </div>

    <div class="row align-items-center pb-5">
      <div class="col-12 col-md-10 offset-md-1">
        <h2 class="font-family-work-sans">Ricette</h2>
        <div class="row d-flex align-items-end pb-5">
          <div class="col-12 col-sm-6 pb-5 pb-sm-0 px-md-3 px-lg-4 px-xl-5">
            <form method="get" action="downloadExcel">
              <div class="form-group">
                <label for="dataRicetta">Scegli il giorno</label>
                <input type="date" name="data" class="form-control" id="dataRicetta">
              </div>
              <div>
                <button class="btn btn-block btn-lg btn-outline-primary" type="submit" id="reportDay">Report giornaliero</button>
                <small id="reportDay" class="form-text text-muted">Scarica il report delle ricette prescritte nel giorno desiderato.</small>
              </div>
            </form>
          </div>
          <div class="col-12 col-sm-6 px-md-3 px-lg-4 px-xl-5">
              <a href= "downloadExcel" role="button" class="btn btn-block btn-lg btn-primary" id="reportAll">Report completo</a>
              <small id="reportAll" class="form-text text-muted">Scarica il report di tutte le ricette prescritte presenti nel database.</small>
          </div>
        </div>
      </div>
    </div>

    <div class="row align-items-center pb-5">
      <div class="col-12 col-md-10 offset-md-1">
        <h2 class="font-family-work-sans">Esami</h2>
        <%
          List<esame> esami = (ArrayList<esame>)request.getAttribute("esami");
          if(esami.size() == 0){
            out.print("Nessun esame presente.");
          }else{
        %>
          <table id="esamiSSP" class="table table-hover table-striped">
            <thead class="thead-dark">
                <tr>
                    <th>Tipologia</th>
                    <th>Stato</th>
                </tr>
            </thead>
            <tbody>
              <%
                    for(esame esame : esami)
                    {
              %>
                <tr>
                    <td>
                      <%
                        out.print(esame.getTipoEsame());
                      %>
                    </td>
                    <td>
                      <%
                      out.print("<a href='esame?id=" + esame.getId() + "'>");

                        if(!esame.getPagato()){
                          out.print("da pagare");
                        }else if(esame.getAttiva()){
                          out.print("pagato/da fare");
                        }else{
                          out.print("Svolto il " + esame.getTimestamp().getDayOfMonth() + " " + formatMonth(esame.getTimestamp().getMonthValue()) + " " + esame.getTimestamp().getYear());
                        }

                      out.print("</a>");
                      %>
                    </td>
                </tr>
              <%
                  }
              %>
            </tbody>
          </table>
        <%
          }
        %>
      </div>
    </div>

  <script>
    document.getElementById("dataRicetta").valueAsDate = new Date();

    $(document).ready( function () {
      $('#esamiSSP').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Italian.json"
        }
      });
    } );
  </script>
  </body>
  </html>
