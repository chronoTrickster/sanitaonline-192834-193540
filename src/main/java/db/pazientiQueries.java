package db;

import classes.paziente;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nolbertj
 */

public class pazientiQueries
{
    protected static final String TABLE_NAME = "pazienti";

    public static paziente getPazienteById(int id)
    {
        paziente response_emp = new paziente();

        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement("SELECT * FROM " + TABLE_NAME  + " WHERE id=?");
            ps.setInt(1,id);

            ResultSet rs = ps.executeQuery();

            if(rs.next())
            {
                response_emp.setId(rs.getInt(1));
                response_emp.setNome(rs.getString(2));
                response_emp.setCognome(rs.getString( 3));
                response_emp.setLuogoNascita(rs.getString(4));
                response_emp.setSesso(rs.getString(5));
                response_emp.setIdDottore(rs.getInt(6));
                response_emp.setProvincia(rs.getString(7));
                // LA 8 E' LOGIN INFO
                response_emp.setCodiceFiscale(rs.getString(9));
                response_emp.setDataDiNascita(rs.getTimestamp(10).toLocalDateTime());
                response_emp.setPropic(rs.getString(11));
            }

            ps.close();
            con.close();
        }
        catch(SQLException ex){
            System.out.println("pazientiQueries.getPazienteById() error: PreparedStatement.");
            ex.printStackTrace(System.err);
            System.exit(1);
        }

        return response_emp;
    }

    public static int getIdPazienteByLoginId(int login_id)
    {
        int id = 0;

        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement("SELECT * FROM " + TABLE_NAME  + " WHERE login_id=?");
            ps.setInt(1, login_id);

            ResultSet rs = ps.executeQuery();

            if(rs.next())
            {
                id = rs.getInt(1);
            }

            ps.close();
            con.close();
        }
        catch(SQLException ex){
            System.out.println("pazientiQueries.getIdPazienteByLoginId() error: PreparedStatement.");
            ex.printStackTrace(System.err);
            System.exit(1);
        }

        return id;
    }

    public static List<paziente> getAllPazienti()
    {
        List<paziente> list = new ArrayList<>();

        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement(
                    "SELECT * FROM " + TABLE_NAME
                            + " ORDER BY id"
            );

            ResultSet rs=ps.executeQuery();

            while(rs.next())
            {
                paziente e = new paziente();

                e.setId(rs.getInt(1));
                e.setNome(rs.getString(2));
                e.setCognome(rs.getString( 3));
                e.setLuogoNascita(rs.getString(4));
                e.setSesso(rs.getString(5));
                e.setIdDottore(rs.getInt(6));
                e.setProvincia(rs.getString(7));
                // LA 8 E' LOGIN INFO
                e.setCodiceFiscale(rs.getString(9));
                e.setDataDiNascita(rs.getTimestamp(10).toLocalDateTime());
                e.setPropic(rs.getString(11));
                list.add(e);
            }

            ps.close();
            con.close();
        }
        catch(SQLException e){
            System.out.println("pazientiQueries.getAllPazienti() error: PreparedStatement.");
            e.printStackTrace(System.err);
            System.exit(1);
        }

        return list;
    }



    public static List<paziente> getPazientiByDottoreId(int id)
    {
        List<paziente> list = new ArrayList<>();

        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement(
                    "SELECT * FROM pazienti\n" +
                            "\tLEFT JOIN (\n" +
                            "\t\t\t\tSELECT paziente_id, MAX(timestamp) FROM visite\n" +
                            "\t\t\t\tWHERE timestamp < CURRENT_TIMESTAMP \n" +
                            "\t\t\t\tGROUP BY paziente_id\n" +
                            "\t\t\t  ) groupvisite\n" +
                            "\tON pazienti.id = groupvisite.paziente_id\n" +
                            "\tLEFT JOIN \n" +
                            "\t(\n" +
                            "\t\tSELECT paziente_id, MAX(ricette.timestamp) FROM ricette\n" +
                            "\t\tJOIN visite ON visite.id = ricette.visita_id\n" +
                            "\t\tGROUP BY paziente_id\n" +
                            "\t) groupricette\n" +
                            "\tON pazienti.id = groupricette.paziente_id\n" +
                            "WHERE pazienti.dottore_id = ?" +
                            "ORDER BY pazienti.id"
            );
            ps.setInt(1, id);
            ResultSet rs=ps.executeQuery();

            while(rs.next())
            {
                paziente e = new paziente();

                e.setId(rs.getInt(1));
                e.setNome(rs.getString(2));
                e.setCognome(rs.getString( 3));
                e.setLuogoNascita(rs.getString(4));
                e.setSesso(rs.getString(5));
                e.setIdDottore(rs.getInt(6));
                e.setProvincia(rs.getString(7));
                // LA 8 E' LOGIN INFO
                e.setCodiceFiscale(rs.getString(9));
                e.setDataDiNascita(rs.getTimestamp(10).toLocalDateTime());
                e.setPropic(rs.getString(11));

                if (rs.getObject(13) != null)
                {
                    e.setUltimaVisita(rs.getTimestamp(13).toLocalDateTime());
                }
                if (rs.getObject(15) != null)
                {
                    e.setUltimaRicetta(rs.getTimestamp(15).toLocalDateTime());
                }
                list.add(e);
            }

            ps.close();
            con.close();
        }
        catch(SQLException e){
            System.out.println("pazientiQueries.getPazientiByDottoreId() error: PreparedStatement.");
            e.printStackTrace(System.err);
            System.exit(1);
        }

        return list;
    }

    public static void updatePazienteDottoreById(int p_id, int d_id) {
        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement("UPDATE " + TABLE_NAME  + " SET dottore_id=? " + " WHERE id=?");
            ps.setInt(1, d_id);
            ps.setInt(2, p_id);

            ps.executeUpdate();

            ps.close();
            con.close();
        }
        catch(SQLException ex){
            System.out.println("pazientiQueries.updatePazienteDottoreById() error: PreparedStatement.");
            ex.printStackTrace(System.err);
            System.exit(1);
        }
    }
}
