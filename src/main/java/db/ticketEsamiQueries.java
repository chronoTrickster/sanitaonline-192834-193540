package db;

import classes.ticketEsame;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

public class ticketEsamiQueries {
    protected static final String TABLE_NAME = "ticket_esami";

    public static ticketEsame getTicketById(int id) {

        ticketEsame response_emp = new ticketEsame();
        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement(
                    "SELECT * FROM " + TABLE_NAME
                        + " JOIN esami ON ticket_esami.esame_id = esami.id"
                        + " JOIN tipologia_esami ON esami.tipo_esame_id = tipologia_esami.id"
                        + " WHERE " + TABLE_NAME + ".id=?"
                            + " ORDER BY " +TABLE_NAME  +".id");
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();

            if(rs.next())
            {
                response_emp.setId(rs.getInt(1));
                response_emp.setEsameId(rs.getInt(2));
                response_emp.setPazienteId(rs.getInt(3));
                response_emp.setPagato(rs.getBoolean(4));
                response_emp.setNome(rs.getString(12));

                if(response_emp.getPagato())
                {
                    response_emp.setDataPagamento(rs.getTimestamp(5).toLocalDateTime());
                }

            }

            ps.close();
            con.close();
        }
        catch(SQLException ex){
            System.out.println("ticketEsamiQueries.getTicketById() error: PreparedStatement.");
            ex.printStackTrace(System.err);
            System.exit(1);
        }

        return response_emp;
    }

    public static ticketEsame getTicketByEsameId(int e_id) {

        ticketEsame response_emp = new ticketEsame();
        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement(
                    "SELECT * FROM " + TABLE_NAME
                        + " JOIN esami ON ticket_esami.esame_id = esami.id"
                        + " JOIN tipologia_esami ON esami.tipo_esame_id = tipologia_esami.id"
                        + " WHERE " + TABLE_NAME + ".esame_id=?"
                            + " ORDER BY " +TABLE_NAME  +".id");
            ps.setInt(1, e_id);

            ResultSet rs = ps.executeQuery();

            if(rs.next())
            {
                response_emp.setId(rs.getInt(1));
                response_emp.setEsameId(rs.getInt(2));
                response_emp.setPazienteId(rs.getInt(3));
                response_emp.setPagato(rs.getBoolean(4));
                response_emp.setNome(rs.getString(12));

                if(response_emp.getPagato())
                {
                    response_emp.setDataPagamento(rs.getTimestamp(5).toLocalDateTime());
                }

            }

            ps.close();
            con.close();
        }
        catch(SQLException ex){
            System.out.println("ticketEsamiQueries.getTicketByEsameId() error: PreparedStatement.");
            ex.printStackTrace(System.err);
            System.exit(1);
        }

        return response_emp;
    }

    public static List<ticketEsame> getTicketEsamiByPazienteId(int p_id)
    {
        List<ticketEsame> list = new ArrayList<>();

        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement(
                    "SELECT * FROM " + TABLE_NAME
                            + " JOIN esami ON ticket_esami.esame_id = esami.id"
                            + " JOIN tipologia_esami ON esami.tipo_esame_id = tipologia_esami.id"
                            + " WHERE paziente_id=?"
                            + " ORDER BY " +TABLE_NAME  +".id"
            );
            ps.setInt(1, p_id);
            ResultSet rs=ps.executeQuery();

            while(rs.next())
            {
                ticketEsame e = new ticketEsame();

                e.setId(rs.getInt(1));
                e.setEsameId(rs.getInt(2));
                e.setPazienteId(rs.getInt(3));
                e.setPagato(rs.getBoolean(4));
                e.setNome(rs.getString(12));

                if(e.getPagato())
                {
                    e.setDataPagamento(rs.getTimestamp(5).toLocalDateTime());
                }

                list.add(e);
            }

            ps.close();
            con.close();
        }
        catch(SQLException e){
            System.out.println("ticketEsamiQueries.getTicketsEsameByPazienteId() error: PreparedStatement.");
            e.printStackTrace(System.err);
            System.exit(1);
        }

        return list;
    }

    public static ticketEsame newTicketEsame(int e_id, int p_id, Boolean pagato)
    {
        ticketEsame newEsame = new ticketEsame();
        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement("INSERT INTO  " + TABLE_NAME  + " (esame_id, paziente_id, pagato) VALUES  (?, ?, ?) RETURNING id");
            ps.setInt(1, e_id);
            ps.setInt(2, p_id);
            ps.setBoolean(3, pagato);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                int key = rs.getInt(1);
                newEsame = getTicketById(key);
            }
            ps.close();
            con.close();
        }
        catch(SQLException ex){
            System.out.println("esamiQueries.newTicketEsame() error: PreparedStatement.");
            ex.printStackTrace(System.err);
            System.exit(1);
        }

        return newEsame;
    }

    public static void pagaTicketById(int id) {

        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement("UPDATE " + TABLE_NAME  + " SET pagato = true, data_pagamento = ? WHERE id=?");
            Timestamp ts = new Timestamp(LocalDateTime.now(ZoneOffset.UTC).toInstant(ZoneOffset.UTC).toEpochMilli());
            ps.setTimestamp(1, ts);
            ps.setInt(2, id);

            ps.executeUpdate();

            ps.close();
            con.close();
        }
        catch(SQLException ex){
            System.out.println("ticketEsamiQueries.pagaTicketById() error: PreparedStatement.");
            ex.printStackTrace(System.err);
            System.exit(1);
        }

    }
}
