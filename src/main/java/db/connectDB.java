package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class connectDB {

    //-------------DATABASE CONFIGURATION-------------------------
    //----- Puoi impostare il duo database semplicemente da queste 4 stringhe.
    public static final String JDBC_DRIVER = "org.postgresql.Driver";
    private static final String URL = "jdbc:postgresql://localhost:5432/servizioSanitario";
    private static final String USER = "postgres";
    private static final String PASS = "admin";
    //------------------------------------------------------------

    static Connection con = null;

    public static Connection getConnection()
    {
        try{
            Class.forName(JDBC_DRIVER);
            con = DriverManager.getConnection(URL,USER,PASS);

        }
        catch(ClassNotFoundException e){
            System.out.println("JavaDB " + JDBC_DRIVER + " non trovato! ");
            System.exit(1);
        }
        catch(SQLException e){
            System.out.println("Impossibile connettersi al database!");
            System.exit(1);
        }

        return con;
    }

}
