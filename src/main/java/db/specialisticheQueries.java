package db;

import classes.risultatoSpecialistica;
import classes.specialistica;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class specialisticheQueries {
    protected static final String TABLE_NAME = "specialistiche";

    public static specialistica getSpecialisticaById(int id)
    {
        specialistica response_emp = new specialistica();

        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement(
                    "SELECT * FROM " + TABLE_NAME
                        + " JOIN visite ON visita_id = visite.id"
                        + " JOIN tipologia_specialistiche ON tipo_specialistica_id = tipologia_specialistiche.id"
                        + " JOIN ticket_specialistiche ON specialistiche.id = ticket_specialistiche.specialistica_id"
                        + " LEFT JOIN risultati_specialistiche ON specialistiche.id = risultati_specialistiche.specialistica_id "
                        + " WHERE " + TABLE_NAME +".id=?"
                            + " ORDER BY " +TABLE_NAME  +".id");
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();

            if(rs.next())
            {
                response_emp.setId(rs.getInt(1));
                response_emp.setVisitaId(rs.getInt(2));
                response_emp.setTipoSpecialisticaId(rs.getInt(3));
                response_emp.setDottoreId(rs.getInt(4));
                response_emp.setAttiva(rs.getBoolean(5));
                response_emp.setProvincia(rs.getString(6));
                response_emp.setPazienteId(rs.getInt(8));

                response_emp.setTipoSpecialistica(rs.getString(13));
                response_emp.setPagato(rs.getBoolean(17));

                if (!(response_emp.getAttiva()))
                {
                    response_emp.setAnamnesi(rs.getString(21));
                    response_emp.setTimestamp(rs.getTimestamp(22).toLocalDateTime());
                }

            }

            ps.close();
            con.close();
        }
        catch(SQLException ex){
            System.out.println("specialisticheQueries.getSpecialisticaById() error: PreparedStatement.");
            ex.printStackTrace(System.err);
            System.exit(1);
        }

        return response_emp;
    }

    public static List<specialistica> getSpecialisticheByIdPaziente(int p_id)
    {
        List<specialistica> list = new ArrayList<>();

        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement(
                    "SELECT * FROM " + TABLE_NAME
                            + " JOIN visite ON visita_id = visite.id"
                            + " JOIN tipologia_specialistiche ON tipo_specialistica_id = tipologia_specialistiche.id"
                            + " JOIN ticket_specialistiche ON specialistiche.id = ticket_specialistiche.specialistica_id"
                            + " LEFT JOIN risultati_specialistiche ON specialistiche.id = risultati_specialistiche.specialistica_id "
                            + " WHERE visite.paziente_id=?"
                            + " ORDER BY " +TABLE_NAME  +".id"
            );
            ps.setInt(1, p_id);
            ResultSet rs=ps.executeQuery();

            while(rs.next())
            {
                specialistica e = new specialistica();

                e.setId(rs.getInt(1));
                e.setVisitaId(rs.getInt(2));
                e.setTipoSpecialisticaId(rs.getInt(3));
                e.setDottoreId(rs.getInt(4));
                e.setAttiva(rs.getBoolean(5));
                e.setProvincia(rs.getString(6));
                e.setPazienteId(rs.getInt(8));

                e.setTipoSpecialistica(rs.getString(13));
                e.setPagato(rs.getBoolean(17));

                if (!(e.getAttiva()))
                {
                    e.setAnamnesi(rs.getString(21));
                    e.setTimestamp(rs.getTimestamp(22).toLocalDateTime());
                }

                list.add(e);
            }

            ps.close();
            con.close();
        }
        catch(SQLException e){
            System.out.println("specialisticheQueries.getSpecialisticheByIdPaziente() error: PreparedStatement.");
            e.printStackTrace(System.err);
            System.exit(1);
        }

        return list;
    }

    public static List<specialistica> getSpecialisticheByIdVisita(int v_id)
    {
        List<specialistica> list = new ArrayList<>();

        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement(
                    "SELECT * FROM " + TABLE_NAME
                            + " JOIN visite ON visita_id = visite.id"
                            + " JOIN tipologia_specialistiche ON tipo_specialistica_id = tipologia_specialistiche.id"
                            + " JOIN ticket_specialistiche ON specialistiche.id = ticket_specialistiche.specialistica_id"
                            + " LEFT JOIN risultati_specialistiche ON specialistiche.id = risultati_specialistiche.specialistica_id "
                            + " WHERE visita_id=?"
                            + " ORDER BY " +TABLE_NAME  +".id"
            );
            ps.setInt(1, v_id);
            ResultSet rs=ps.executeQuery();

            while(rs.next())
            {
                specialistica e = new specialistica();

                e.setId(rs.getInt(1));
                e.setVisitaId(rs.getInt(2));
                e.setTipoSpecialisticaId(rs.getInt(3));
                e.setDottoreId(rs.getInt(4));
                e.setAttiva(rs.getBoolean(5));
                e.setProvincia(rs.getString(6));
                e.setPazienteId(rs.getInt(8));

                e.setTipoSpecialistica(rs.getString(13));
                e.setPagato(rs.getBoolean(17));

                if (!(e.getAttiva()))
                {
                    e.setAnamnesi(rs.getString(21));
                    e.setTimestamp(rs.getTimestamp(22).toLocalDateTime());
                }

                list.add(e);
            }

            ps.close();
            con.close();
        }
        catch(SQLException e){
            System.out.println("specialisticheQueries.getSpecialisticheByIdVisita() error: PreparedStatement.");
            e.printStackTrace(System.err);
            System.exit(1);
        }

        return list;
    }

    public static int newSpecialistica(int v_id, int s_id, int d_id, Boolean attiva, String provincia)
    {
        int key = 0;
        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement("INSERT INTO  " + TABLE_NAME  + " (visita_id, tipo_specialistica_id, dottore_id, attiva, provincia) VALUES  (?, ?, ?, ?, ?) RETURNING id");
            ps.setInt(1, v_id);
            ps.setInt(2, s_id);
            ps.setInt(3, d_id);
            ps.setBoolean(4, attiva);
            ps.setString(5, provincia);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                key = rs.getInt(1);
            }
            ps.close();
            con.close();
        }
        catch(SQLException ex){
            System.out.println("specialisticheQueries.newSpecialistica() error: PreparedStatement.");
            ex.printStackTrace(System.err);
            System.exit(1);
        }

        return key;
    }

    public static void disattivaSpecialisticaById(int id) {
        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement("UPDATE " + TABLE_NAME  + " SET attiva = false WHERE id=?");
            ps.setInt(1, id);

            ps.executeUpdate();

            ps.close();
            con.close();
        }
        catch(SQLException ex){
            System.out.println("specialisticheQueries.disattivaSpecialisticaById() error: PreparedStatement.");
            ex.printStackTrace(System.err);
            System.exit(1);
        }
    }
}
