package db;

import classes.esame;
import classes.risultatoEsame;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class esamiQueries {
    protected static final String TABLE_NAME = "esami";

    public static esame getEsameById(int id)
    {
        esame response_emp = new esame();

        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement(
                    "SELECT * FROM " + TABLE_NAME
                        + " JOIN visite ON visita_id = visite.id"
                        + " JOIN tipologia_esami ON tipo_esame_id = tipologia_esami.id"
                        + " JOIN ticket_esami ON esami.id = ticket_esami.esame_id"
                        + " LEFT JOIN risultati_esami ON esami.id = risultati_esami.esame_id "
                        + " WHERE " + TABLE_NAME +".id=?"
                            + " ORDER BY " +TABLE_NAME  +".id");
            
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();

            if(rs.next())
            {
                response_emp.setId(rs.getInt(1));
                response_emp.setVisitaId(rs.getInt(2));
                response_emp.setTipoEsameId(rs.getInt(3));
                response_emp.setAttiva(rs.getBoolean(4));
                response_emp.setProvincia(rs.getString(5));
                response_emp.setPazienteId(rs.getInt(7));

                response_emp.setTipoEsame(rs.getString(12));
                response_emp.setPagato(rs.getBoolean(16));

                if (!(response_emp.getAttiva()))
                {
                    response_emp.setAnamnesi(rs.getString(20));
                    response_emp.setTimestamp(rs.getTimestamp(21).toLocalDateTime());
                }

            }

            ps.close();
            con.close();
        }
        catch(SQLException ex){
            System.out.println("esamiQueries.getEsamiById() error: PreparedStatement.");
            ex.printStackTrace(System.err);
            System.exit(1);
        }

        return response_emp;
    }

    public static List<esame> getEsamiByIdPaziente(int p_id)
    {
        List<esame> list = new ArrayList<>();

        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement(
                    "SELECT * FROM " + TABLE_NAME
                            + " JOIN visite ON visita_id = visite.id"
                            + " JOIN tipologia_esami ON tipo_esame_id = tipologia_esami.id"
                            + " JOIN ticket_esami ON esami.id = ticket_esami.esame_id"
                            + " LEFT JOIN risultati_esami ON esami.id = risultati_esami.esame_id "
                            + " WHERE visite.paziente_id=?"
                            + " ORDER BY " +TABLE_NAME  +".id"
            );
            ps.setInt(1, p_id);
            ResultSet rs=ps.executeQuery();

            while(rs.next())
            {
                esame e = new esame();

                e.setId(rs.getInt(1));
                e.setVisitaId(rs.getInt(2));
                e.setTipoEsameId(rs.getInt( 3));
                e.setAttiva(rs.getBoolean(4));
                e.setProvincia(rs.getString(5));
                e.setPazienteId(rs.getInt(7));

                e.setTipoEsame(rs.getString(12));
                e.setPagato(rs.getBoolean(16));

                if (!(e.getAttiva()))
                {
                    e.setAnamnesi(rs.getString(20));
                    e.setTimestamp(rs.getTimestamp(21).toLocalDateTime());
                }

                list.add(e);
            }

            ps.close();
            con.close();
        }
        catch(SQLException e){
            System.out.println("esamiQueries.getEsamiByIdPaziente() error: PreparedStatement.");
            e.printStackTrace(System.err);
            System.exit(1);
        }

        return list;
    }

    public static List<esame> getEsamiByIdVisita(int v_id)
    {
        List<esame> list = new ArrayList<>();

        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement(
                    "SELECT * FROM " + TABLE_NAME
                            + " JOIN visite ON visita_id = visite.id"
                            + " JOIN tipologia_esami ON tipo_esame_id = tipologia_esami.id"
                            + " JOIN ticket_esami ON esami.id = ticket_esami.esame_id"
                            + " LEFT JOIN risultati_esami ON esami.id = risultati_esami.esame_id "
                            + " WHERE visita_id=?"
                            + " ORDER BY " +TABLE_NAME  +".id"
            );
            ps.setInt(1, v_id);
            ResultSet rs=ps.executeQuery();

            while(rs.next())
            {
                esame e = new esame();

                e.setId(rs.getInt(1));
                e.setVisitaId(rs.getInt(2));
                e.setTipoEsameId(rs.getInt( 3));
                e.setAttiva(rs.getBoolean(4));
                e.setProvincia(rs.getString(5));

                e.setPazienteId(rs.getInt(7));
                e.setTipoEsame(rs.getString(12));
                e.setPagato(rs.getBoolean(16));

                if (!(e.getAttiva()))
                {
                    e.setAnamnesi(rs.getString(20));
                    e.setTimestamp(rs.getTimestamp(21).toLocalDateTime());
                }

                list.add(e);
            }

            ps.close();
            con.close();
        }
        catch(SQLException e){
            System.out.println("esamiQueries.getEsamiByIdVisita() error: PreparedStatement.");
            e.printStackTrace(System.err);
            System.exit(1);
        }

        return list;
    }


    public static List<esame> getEsamiByProvincia(String provincia) {
        List<esame> list = new ArrayList<>();

        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement(
                    "SELECT * FROM " + TABLE_NAME
                            + " JOIN visite ON visita_id = visite.id"
                            + " JOIN tipologia_esami ON tipo_esame_id = tipologia_esami.id"
                            + " JOIN ticket_esami ON esami.id = ticket_esami.esame_id"
                            + " LEFT JOIN risultati_esami ON esami.id = risultati_esami.esame_id "
                            + " WHERE esami.provincia=?"
                            + " ORDER BY " +TABLE_NAME  +".id"
            );
            ps.setString(1, provincia);
            ResultSet rs=ps.executeQuery();

            while(rs.next())
            {
                esame e = new esame();

                e.setId(rs.getInt(1));
                e.setVisitaId(rs.getInt(2));
                e.setTipoEsameId(rs.getInt( 3));
                e.setAttiva(rs.getBoolean(4));
                e.setProvincia(rs.getString(5));

                e.setPazienteId(rs.getInt(7));
                e.setTipoEsame(rs.getString(12));
                e.setPagato(rs.getBoolean(16));

                if (!(e.getAttiva()))
                {
                    e.setAnamnesi(rs.getString(20));
                    e.setTimestamp(rs.getTimestamp(21).toLocalDateTime());
                }

                list.add(e);
            }

            ps.close();
            con.close();
        }
        catch(SQLException e){
            System.out.println("esamiQueries.getEsamiByProvincia() error: PreparedStatement.");
            e.printStackTrace(System.err);
            System.exit(1);
        }

        return list;
    }

    public static int newEsame(int v_id, int e_id, Boolean attiva, String provincia)
    {
        int key = 0;
        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement("INSERT INTO  " + TABLE_NAME  + " (visita_id, tipo_esame_id, attiva, provincia) VALUES  (?, ?, ?, ?) RETURNING id");
            ps.setInt(1, v_id);
            ps.setInt(2, e_id);
            ps.setBoolean(3, attiva);
            ps.setString(4, provincia);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                key = rs.getInt(1);
            }
            ps.close();
            con.close();
        }
        catch(SQLException ex){
            System.out.println("esamiQueries.newEsame() error: PreparedStatement.");
            ex.printStackTrace(System.err);
            System.exit(1);
        }

        return key;
    }


    public static void disattivaEsameById(int id) {
        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement("UPDATE " + TABLE_NAME  + " SET attiva = false WHERE id=?");
            ps.setInt(1, id);

            ps.executeUpdate();

            ps.close();
            con.close();
        }
        catch(SQLException ex){
            System.out.println("esamiQueries.disattivaEsameById() error: PreparedStatement.");
            ex.printStackTrace(System.err);
            System.exit(1);
        }
    }

}
