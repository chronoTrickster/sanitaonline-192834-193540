package db;

import classes.visita;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

public class visiteQueries {
    protected static final String TABLE_NAME = "visite";

    public static visita getVisitaById(int id)
    {
        visita response_emp = new visita();

        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement("SELECT * FROM visite " +
                    " JOIN pazienti ON visite.paziente_id = pazienti.id " +
                    " JOIN dottori ON visite.dottore_id = dottori.id" +
                    " WHERE visite.id = ?" +
                    " ORDER BY visite.id");
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();

            if(rs.next())
            {
                response_emp.setId(rs.getInt(1));
                response_emp.setPazienteId(rs.getInt(2));
                response_emp.setDottoreId(rs.getInt(3));
                response_emp.setAttiva(rs.getBoolean(4));
                response_emp.setTimestamp(rs.getTimestamp(5).toLocalDateTime());

                response_emp.setNomePaziente(rs.getString(7));
                response_emp.setCognomePaziente(rs.getString(8));
                response_emp.setNomeDottore(rs.getString(18));
                response_emp.setCognomeDottore(rs.getString(19));
            }

            ps.close();
            con.close();
        }
        catch(SQLException ex){
            System.out.println("visiteQueries.getVisitaById() error: PreparedStatement.");
            ex.printStackTrace(System.err);
            System.exit(1);
        }

        return response_emp;
    }

    public static List<visita> getVisiteByIdPaziente(int p_id)
    {
        List<visita> list = new ArrayList<>();

        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement(
                    "SELECT * FROM visite " +
                            " JOIN pazienti ON visite.paziente_id = pazienti.id " +
                            " JOIN dottori ON visite.dottore_id = dottori.id" +
                            " WHERE visite.paziente_id = ?" +
                            " ORDER BY visite.id"
            );
            ps.setInt(1, p_id);
            ResultSet rs=ps.executeQuery();

            while(rs.next())
            {
                visita e = new visita();

                e.setId(rs.getInt(1));
                e.setPazienteId(rs.getInt(2));
                e.setDottoreId(rs.getInt( 3));
                e.setAttiva(rs.getBoolean(4));
                e.setTimestamp(rs.getTimestamp(5).toLocalDateTime());

                e.setNomePaziente(rs.getString(7));
                e.setCognomePaziente(rs.getString(8));
                e.setNomeDottore(rs.getString(18));
                e.setCognomeDottore(rs.getString(19));

                list.add(e);
            }

            ps.close();
            con.close();
        }
        catch(SQLException e){
            System.out.println("visiteQueries.getVisiteByIdPaziente() error: PreparedStatement.");
            e.printStackTrace(System.err);
            System.exit(1);
        }

        return list;
    }

    public static List<visita> getVisiteByIdDottore(int d_id)
    {
        List<visita> list = new ArrayList<>();

        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement(
                    "SELECT * FROM visite JOIN pazienti ON pazienti.dottore_id = visite.dottore_id" +
                            " AND pazienti.id = visite.paziente_id" +
                            " JOIN dottori ON dottori.id = visite.dottore_id" +
                            " WHERE visite.dottore_id= ?" +
                            " ORDER BY visite.id"
            );
            ps.setInt(1, d_id);
            ResultSet rs=ps.executeQuery();

            while(rs.next())
            {
                visita e = new visita();

                e.setId(rs.getInt(1));
                e.setPazienteId(rs.getInt(2));
                e.setDottoreId(rs.getInt( 3));
                e.setAttiva(rs.getBoolean(4));
                e.setTimestamp(rs.getTimestamp(5).toLocalDateTime());

                e.setNomePaziente(rs.getString(7));
                e.setCognomePaziente(rs.getString(8));
                e.setNomeDottore(rs.getString(18));
                e.setCognomeDottore(rs.getString(19));
                list.add(e);
            }

            ps.close();
            con.close();
        }
        catch(SQLException e){
            System.out.println("visiteQueries.getVisiteByIdDottore() error: PreparedStatement.");
            e.printStackTrace(System.err);
            System.exit(1);
        }

        return list;
    }

    public static visita newVisita(int p_id, int d_id, Boolean attiva, LocalDateTime timestamp)
    {
        visita newVisita = new visita();
        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement("INSERT INTO  " + TABLE_NAME  + " (paziente_id, dottore_id, attiva, timestamp) VALUES  (?, ?, ?, ?) RETURNING id");
            ps.setInt(1, p_id);
            ps.setInt(2, d_id);
            ps.setBoolean(3, attiva);
            Timestamp ts = new Timestamp(timestamp.toInstant(ZoneOffset.UTC).toEpochMilli());
            ps.setTimestamp(4, ts);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                int key = rs.getInt(1);
                newVisita = getVisitaById(key);
            }
            ps.close();
            con.close();
        }
        catch(SQLException ex){
            System.out.println("visiteQueries.newVisita() error: PreparedStatement.");
            ex.printStackTrace(System.err);
            System.exit(1);
        }

        return newVisita;
    }

}
