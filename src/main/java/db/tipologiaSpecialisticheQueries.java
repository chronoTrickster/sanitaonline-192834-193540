package db;

import classes.tipologiaSpecialistica;
import db.connectDB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class tipologiaSpecialisticheQueries {
    protected static final String TABLE_NAME = "tipologia_specialistiche";

    public static tipologiaSpecialistica getTipologiaSpecialisticaById(int id) {

        tipologiaSpecialistica response_emp = new tipologiaSpecialistica();
        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement("SELECT * FROM " + TABLE_NAME  + " WHERE id=?");
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();

            if(rs.next())
            {
                response_emp.setId(rs.getInt(1));
                response_emp.setNome(rs.getString(2));
            }

            ps.close();
            con.close();
        }
        catch(SQLException ex){
            System.out.println("tipologiaSpecialisticheQueries.getTipologiaSpecialisticaById() error: PreparedStatement.");
            ex.printStackTrace(System.err);
            System.exit(1);
        }

        return response_emp;
    }

    public static List<tipologiaSpecialistica> getTipologieSpecialistiche() {
        List<tipologiaSpecialistica> list = new ArrayList<>();

        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement(
                    "SELECT * FROM " + TABLE_NAME
            );
            ResultSet rs=ps.executeQuery();

            while(rs.next())
            {
                tipologiaSpecialistica e = new tipologiaSpecialistica();

                e.setId(rs.getInt(1));
                e.setNome(rs.getString(2));

                list.add(e);
            }

            ps.close();
            con.close();
        }
        catch(SQLException e){
            System.out.println("tipologiaSpecialisticaQueries.getTipologieSpecialistiche() error: PreparedStatement.");
            e.printStackTrace(System.err);
            System.exit(1);
        }

        return list;
    }
}