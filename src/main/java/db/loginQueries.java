package db;

import classes.login;
import db.pazientiQueries;


import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class loginQueries {

    protected static final String TABLE_NAME = "login";

    public static login getLoginByEmail(String email)
    {
        login response_emp = new login();

        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement("SELECT * FROM " + TABLE_NAME  + " WHERE email=?");
            ps.setString(1,email);

            ResultSet rs = ps.executeQuery();

            if(rs.next())
            {
                response_emp.setId(rs.getInt(1));
                response_emp.setEmail(rs.getString(2));
                response_emp.setPassword(rs.getString( 3));
                response_emp.setUserType(rs.getInt(4));
            }

            ps.close();
            con.close();
        }
        catch(SQLException ex){
            System.out.println("pazientiQueries.getPazienteById() error: PreparedStatement.");
            ex.printStackTrace(System.err);
            System.exit(1);
        }

        return response_emp;
    }

    public static int getExIdByLogin(int id)
    {
        int exId = 0;

        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement("SELECT * FROM " + TABLE_NAME  + " WHERE id=?");
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();

            if(rs.next())
            {
                int usertype = rs.getInt (4);
                if (usertype == 0)
                {
                    exId = pazientiQueries.getIdPazienteByLoginId(id);
                }
                else if (usertype == 1)
                {
                    exId = dottoriQueries.GetIdDottoreByLoginId(id);
                }
                else if (usertype == 2)
                {
                    exId = sspQueries.getIdSSPByLoginId(id);
                }
            }

            ps.close();
            con.close();
        }
        catch(SQLException ex){
            System.out.println("pazientiQueries.getExIdByLogin() error: PreparedStatement.");
            ex.printStackTrace(System.err);
            System.exit(1);
        }

        return exId;
    }

    public static void changePassword(int id, String nuovaPassword) {
        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement("UPDATE " + TABLE_NAME  + " SET password = ? WHERE id = ?");
            ps.setString(1, nuovaPassword);
            ps.setInt(2, id);

            ps.executeUpdate();

            ps.close();
            con.close();
        }
        catch(SQLException ex){
            System.out.println("pazientiQueries.getExIdByLogin() error: PreparedStatement.");
            ex.printStackTrace(System.err);
            System.exit(1);
        }
    }
}
