package db;

import classes.ticketSpecialistica;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

public class ticketSpecialisticheQueries {
    protected static final String TABLE_NAME = "ticket_specialistiche ";

    public static ticketSpecialistica getTicketById(int id) {

        ticketSpecialistica response_emp = new ticketSpecialistica();
        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement(
                    "SELECT * FROM " + TABLE_NAME
                        + " JOIN specialistiche ON ticket_specialistiche.specialistica_id = specialistiche.id"
                        + " JOIN tipologia_specialistiche ON specialistiche.tipo_specialistica_id = tipologia_specialistiche.id"
                        + " WHERE "+ TABLE_NAME + ".id=?");
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();

            if(rs.next())
            {
                response_emp.setId(rs.getInt(1));
                response_emp.setSpecialisticaId(rs.getInt(2));
                response_emp.setPazienteId(rs.getInt(3));
                response_emp.setPagato(rs.getBoolean(4));
                response_emp.setNome(rs.getString(13));

                if(response_emp.getPagato())
                {
                    response_emp.setDataPagamento(rs.getTimestamp(5).toLocalDateTime());
                }
            }

            ps.close();
            con.close();
        }
        catch(SQLException ex){
            System.out.println("ticketSpecialisticheQueries.getTicketById() error: PreparedStatement.");
            ex.printStackTrace(System.err);
            System.exit(1);
        }

        return response_emp;
    }

    public static ticketSpecialistica getTicketBySpecialisticaId(int s_id) {

        ticketSpecialistica response_emp = new ticketSpecialistica();
        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement(
                    "SELECT * FROM " + TABLE_NAME
                        + " JOIN specialistiche ON ticket_specialistiche.specialistica_id = specialistiche.id"
                        + " JOIN tipologia_specialistiche ON specialistiche.tipo_specialistica_id = tipologia_specialistiche.id"
                        + " WHERE specialistica_id=?");
            ps.setInt(1, s_id);

            ResultSet rs = ps.executeQuery();

            if(rs.next())
            {
                response_emp.setId(rs.getInt(1));
                response_emp.setSpecialisticaId(rs.getInt(2));
                response_emp.setPazienteId(rs.getInt(3));
                response_emp.setPagato(rs.getBoolean(4));
                response_emp.setNome(rs.getString(13));

                if(response_emp.getPagato())
                {
                    response_emp.setDataPagamento(rs.getTimestamp(5).toLocalDateTime());
                }
            }

            ps.close();
            con.close();
        }
        catch(SQLException ex){
            System.out.println("ticketEsamiQueries.getTicketByEsameId() error: PreparedStatement.");
            ex.printStackTrace(System.err);
            System.exit(1);
        }

        return response_emp;
    }

    public static List<ticketSpecialistica> getTicketSpecialisticheByPazienteId(int p_id)
    {
        List<ticketSpecialistica> list = new ArrayList<>();

        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement(
                    "SELECT * FROM " + TABLE_NAME
                            + " JOIN specialistiche ON ticket_specialistiche.specialistica_id = specialistiche.id"
                            + " JOIN tipologia_specialistiche ON specialistiche.tipo_specialistica_id = tipologia_specialistiche.id"
                            + " WHERE paziente_id=?"
                            + " ORDER BY " + TABLE_NAME + ".id"
            );
            ps.setInt(1, p_id);
            ResultSet rs=ps.executeQuery();

            while(rs.next())
            {
                ticketSpecialistica e = new ticketSpecialistica();

                e.setId(rs.getInt(1));
                e.setSpecialisticaId(rs.getInt(2));
                e.setPazienteId(rs.getInt(3));
                e.setPagato(rs.getBoolean(4));
                e.setNome(rs.getString(13));

                if(e.getPagato())
                {
                    e.setDataPagamento(rs.getTimestamp(5).toLocalDateTime());
                }

                list.add(e);
            }

            ps.close();
            con.close();
        }
        catch(SQLException e){
            System.out.println("ticketSpecialisticheQueries.getTicketSpecialisticaByPazienteId() error: PreparedStatement.");
            e.printStackTrace(System.err);
            System.exit(1);
        }

        return list;
    }

    public static ticketSpecialistica newTicketSpecialistica(int s_id, int p_id, Boolean pagato)
    {
        ticketSpecialistica newTicket = new ticketSpecialistica();
        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement("INSERT INTO  " + TABLE_NAME  + " (specialistica_id, paziente_id, pagato) VALUES  (?, ?, ?) RETURNING id");
            ps.setInt(1, s_id);
            ps.setInt(2, p_id);
            ps.setBoolean(3, pagato);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                int key = rs.getInt(1);
                newTicket = getTicketById(key);
            }
            ps.close();
            con.close();
        }
        catch(SQLException ex){
            System.out.println("ticketSpecialisticheQueries.newTicketSpecialistica() error: PreparedStatement.");
            ex.printStackTrace(System.err);
            System.exit(1);
        }

        return newTicket;
    }

    public static void pagaTicketById(int id) {

        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement("UPDATE " + TABLE_NAME  + " SET pagato = true, data_pagamento = ? WHERE id=?");
            Timestamp ts = new Timestamp(LocalDateTime.now(ZoneOffset.UTC).toInstant(ZoneOffset.UTC).toEpochMilli());
            ps.setTimestamp(1, ts);
            ps.setInt(2, id);

            ps.executeUpdate();

            ps.close();
            con.close();
        }
        catch(SQLException ex){
            System.out.println("ticketSpecialisticheQueries.pagaTicketById() error: PreparedStatement.");
            ex.printStackTrace(System.err);
            System.exit(1);
        }

    }
}
