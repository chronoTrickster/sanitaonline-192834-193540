package db;

import classes.risultatoSpecialistica;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class risultatiSpecialisticheQueries {
    protected static final String TABLE_NAME = "risultati_specialistiche";

    public static risultatoSpecialistica getRisultatoSpecialisticaBySpecialisticaId(int s_id)
    {
        risultatoSpecialistica response_emp = new risultatoSpecialistica();
        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement("SELECT * FROM " + TABLE_NAME  + " WHERE specialistica_id=?");
            ps.setInt(1, s_id);

            ResultSet rs = ps.executeQuery();

            if(rs.next())
            {
                response_emp.setId(rs.getInt(1));
                response_emp.setSpecialisticaId(rs.getInt(2));
                response_emp.setAnamnesi(rs.getString(3));
                response_emp.setTimestamp(rs.getTimestamp(4).toLocalDateTime());
            }

            ps.close();
            con.close();
        }
        catch(SQLException ex){
            System.out.println("risultatiSpecialisticheQueries.getRisultatoSpecialisticaBySpecialisticaId() error: PreparedStatement.");
            ex.printStackTrace(System.err);
            System.exit(1);
        }

        return response_emp;
    }

    public static String getAnamnesiSpecialisticaBySpecialisticaId(int s_id) {

        String anamnesi = "";
        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement("SELECT anamnesi FROM " + TABLE_NAME  + " WHERE specialistica_id=?");
            ps.setInt(1, s_id);

            ResultSet rs = ps.executeQuery();

            if(rs.next())
            {
                anamnesi = rs.getString(1);
            }

            ps.close();
            con.close();
        }
        catch(SQLException ex){
            System.out.println("risultatiSpecialisticheQueries.getAnamnesiSpecialisticaBySpecialisticaId() error: PreparedStatement.");
            ex.printStackTrace(System.err);
            System.exit(1);
        }

        return anamnesi;
    }

    public static LocalDateTime getTimeStampBySpecialisticaId(int s_id) {

        LocalDateTime timestamp = LocalDateTime.now();
        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement("SELECT timestamp FROM " + TABLE_NAME  + " WHERE specialistica_id=?");
            ps.setInt(1, s_id);

            ResultSet rs = ps.executeQuery();

            if(rs.next())
            {
                timestamp = rs.getTimestamp(1).toLocalDateTime();
            }

            ps.close();
            con.close();
        }
        catch(SQLException ex){
            System.out.println("risultatiSpecialisticheQueries.getTimeStampBySpecialisticaId() error: PreparedStatement.");
            ex.printStackTrace(System.err);
            System.exit(1);
        }

        return timestamp;
    }

    public static int newRisultatoSpecialistica(int s_id, String anamnesi, LocalDateTime timestamp)
    {
        int newId = 0;
        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement("INSERT INTO  " + TABLE_NAME  + " (specialistica_id, anamnesi, timestamp) VALUES  (?, ?, ?) RETURNING id");
            ps.setInt(1, s_id);
            ps.setString(2, anamnesi);
            Timestamp ts = new Timestamp(timestamp.toInstant(ZoneOffset.UTC).toEpochMilli());
            ps.setTimestamp(3, ts);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                int key = rs.getInt(1);
            }
            ps.close();
            con.close();
        }
        catch(SQLException ex){
            System.out.println("risultatiSpecialisticheQueries.newRisultatoSpecialistica() error: PreparedStatement.");
            ex.printStackTrace(System.err);
            System.exit(1);
        }

        return newId;
    }
}
