package db;

import classes.dottore;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class dottoriQueries {
    protected static final String TABLE_NAME = "dottori";

    public static dottore getDottoreById(int id)
    {
        dottore response_emp = new dottore();

        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement("SELECT * FROM " + TABLE_NAME  + " WHERE id=?");
            ps.setInt(1,id);

            ResultSet rs = ps.executeQuery();

            if(rs.next())
            {
                response_emp.setId(rs.getInt(1));
                response_emp.setNome(rs.getString(2));
                response_emp.setCognome(rs.getString( 3));
                response_emp.setSesso(rs.getString(4));
                response_emp.setProvincia(rs.getString(5));
            }

            ps.close();
            con.close();
        }
        catch(SQLException ex){
            System.out.println("dottoriQueries.getDottoreById() error: PreparedStatement.");
            ex.printStackTrace(System.err);
            System.exit(1);
        }

        return response_emp;
    }

    public static int GetIdDottoreByLoginId(int login_id) {
        int id = 0;

        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement("SELECT * FROM " + TABLE_NAME  + " WHERE login_id=?");
            ps.setInt(1, login_id);

            ResultSet rs = ps.executeQuery();

            if(rs.next())
            {
                id = rs.getInt(1);
            }

            ps.close();
            con.close();
        }
        catch(SQLException ex){
            System.out.println("dottoriQueries.getIdDottoreByLoginId() error: PreparedStatement.");
            ex.printStackTrace(System.err);
            System.exit(1);
        }

        return id;
    }

    public static List<dottore> getDottoriByProvincia(String provincia)
    {
        List<dottore> list = new ArrayList<>();

        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement(
                    "SELECT * FROM " + TABLE_NAME + " WHERE provincia=?"
                            + " ORDER BY id"
            );
            ps.setString(1, provincia);
            ResultSet rs=ps.executeQuery();

            while(rs.next())
            {
                dottore e = new dottore();

                e.setId(rs.getInt(1));
                e.setNome(rs.getString(2));
                e.setCognome(rs.getString( 3));
                e.setSesso(rs.getString(4));
                e.setProvincia(rs.getString(5));

                list.add(e);
            }

            ps.close();
            con.close();
        }
        catch(SQLException e){
            System.out.println("dottoriQueries.getDottoriByProvincia() error: PreparedStatement.");
            e.printStackTrace(System.err);
            System.exit(1);
        }

        return list;
    }
}
