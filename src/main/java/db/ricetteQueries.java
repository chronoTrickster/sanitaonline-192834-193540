package db;

import classes.esame;
import classes.ricetta;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

public class ricetteQueries {
    protected static final String TABLE_NAME = "ricette";

    public static ricetta getRicettaById(int id)
    {
        ricetta response_emp = new ricetta();

        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement("SELECT * FROM " + TABLE_NAME  +
                    " JOIN visite ON ricette.visita_id = visite.id" +
                    " WHERE ricette.id=?");
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();

            if(rs.next())
            {
                response_emp.setId(rs.getInt(1));
                response_emp.setVisitaId(rs.getInt(2));
                response_emp.setAttiva(rs.getBoolean(3));
                response_emp.setMedicinali(rs.getString(4));
                response_emp.setTimestamp(rs.getTimestamp(5).toLocalDateTime());

                response_emp.setPazienteId(rs.getInt(7));
                response_emp.setDottoreId(rs.getInt(8));
            }

            ps.close();
            con.close();
        }
        catch(SQLException ex){
            System.out.println("ricetteQueries.getRicettaById() error: PreparedStatement.");
            ex.printStackTrace(System.err);
            System.exit(1);
        }

        return response_emp;
    }

    public static List<ricetta> getRicetteByIdVisita(int v_id)
    {
        List<ricetta> list = new ArrayList<>();

        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement(
                    "SELECT * FROM " + TABLE_NAME +
                            " JOIN visite ON ricette.visita_id = visite.id" +
                            " WHERE visita_id=?" +
                            " ORDER BY ricette.id"
            );
            ps.setInt(1, v_id);
            ResultSet rs=ps.executeQuery();

            while(rs.next())
            {
                ricetta e = new ricetta();

                e.setId(rs.getInt(1));
                e.setVisitaId(rs.getInt(2));
                e.setAttiva(rs.getBoolean(3));
                e.setMedicinali(rs.getString(4));
                e.setTimestamp(rs.getTimestamp(5).toLocalDateTime());

                e.setPazienteId(rs.getInt(7));
                e.setDottoreId(rs.getInt(8));
                list.add(e);
            }

            ps.close();
            con.close();
        }
        catch(SQLException e){
            System.out.println("ricetteQueries.getRicetteById() error: PreparedStatement.");
            e.printStackTrace(System.err);
            System.exit(1);
        }

        return list;
    }


    public static List<ricetta> getRicetteByIdPaziente(int p_id) {
        List<ricetta> list = new ArrayList<>();

        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement(
                    "SELECT * FROM ricette" +
                            " JOIN visite ON visite.id = ricette.visita_id" +
                            " WHERE visite.paziente_id = ?"
            );
            ps.setInt(1, p_id);
            ResultSet rs=ps.executeQuery();

            while(rs.next())
            {
                ricetta e = new ricetta();

                e.setId(rs.getInt(1));
                e.setVisitaId(rs.getInt(2));
                e.setAttiva(rs.getBoolean(3));
                e.setMedicinali(rs.getString(4));
                e.setTimestamp(rs.getTimestamp(5).toLocalDateTime());

                e.setPazienteId(p_id);
                e.setDottoreId(rs.getInt(8));
                list.add(e);
            }

            ps.close();
            con.close();
        }
        catch(SQLException e){
            System.out.println("ricetteQueries.getRicetteByIdPaziente() error: PreparedStatement.");
            e.printStackTrace(System.err);
            System.exit(1);
        }

        return list;
    }

    public static List<ricetta> getRicetteByProvincia(String provincia)
    {
        List<ricetta> list = new ArrayList<>();

        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement(
                    "SELECT * FROM ricette" +
                            " JOIN visite ON ricette.visita_id = visite.id" +
                            " JOIN pazienti ON visite.paziente_id = pazienti.id" +
                            " JOIN dottori ON visite.dottore_id = dottori.id" +
                            " WHERE pazienti.provincia = ?"
            );
            ps.setString(1, provincia);
            ResultSet rs=ps.executeQuery();

            while(rs.next())
            {
                ricetta e = new ricetta();

                e.setId(rs.getInt(1));
                e.setVisitaId(rs.getInt(2));
                e.setAttiva(rs.getBoolean(3));
                e.setMedicinali(rs.getString(4));
                e.setTimestamp(rs.getTimestamp(5).toLocalDateTime());

                e.setPazienteId(rs.getInt(7));
                e.setDottoreId(rs.getInt(8));

                e.setNomePaziente(rs.getString(12));
                e.setCognomePaziente(rs.getString(13));
                e.setCodiceFiscalePaziente(rs.getString(19));

                e.setNomeDottore(rs.getString(23));
                e.setCognomeDottore(rs.getString(24));

                list.add(e);
            }

            ps.close();
            con.close();
        }
        catch(SQLException e){
            System.out.println("ricetteQueries.getRicetteByProvincia() error: PreparedStatement.");
            e.printStackTrace(System.err);
            System.exit(1);
        }

        return list;
    }


    public static List<ricetta> getRicetteByProvinciaInGiornata(String provincia, LocalDateTime data) {
        List<ricetta> list = new ArrayList<>();

        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement(
                    "SELECT * FROM ricette" +
                            " JOIN visite ON ricette.visita_id = visite.id" +
                            " JOIN pazienti ON visite.paziente_id = pazienti.id" +
                            " JOIN dottori ON visite.dottore_id = dottori.id" +
                            " WHERE pazienti.provincia = ? AND ricette.timestamp >= ? AND ricette.timestamp < ?"
            );

            Timestamp ts = new Timestamp(data.toInstant(ZoneOffset.UTC).toEpochMilli());
            Timestamp ts2 = new Timestamp(data.plusDays(1).toInstant(ZoneOffset.UTC).toEpochMilli());

            ps.setString(1, provincia);
            ps.setTimestamp(2, ts);
            ps.setTimestamp(3, ts2);
            ResultSet rs=ps.executeQuery();

            while(rs.next())
            {
                ricetta e = new ricetta();

                e.setId(rs.getInt(1));
                e.setVisitaId(rs.getInt(2));
                e.setAttiva(rs.getBoolean(3));
                e.setMedicinali(rs.getString(4));
                e.setTimestamp(rs.getTimestamp(5).toLocalDateTime());

                e.setPazienteId(rs.getInt(7));
                e.setDottoreId(rs.getInt(8));

                e.setNomePaziente(rs.getString(12));
                e.setCognomePaziente(rs.getString(13));
                e.setCodiceFiscalePaziente(rs.getString(19));

                e.setNomeDottore(rs.getString(23));
                e.setCognomeDottore(rs.getString(24));
                list.add(e);
            }

            ps.close();
            con.close();
        }
        catch(SQLException e){
            System.out.println("ricetteQueries.getRicetteByProvinciaInGiornata() error: PreparedStatement.");
            e.printStackTrace(System.err);
            System.exit(1);
        }

        return list;
    }

    public static ricetta newRicetta(int v_id, Boolean attiva, String medicinali, LocalDateTime timestamp)
    {
        ricetta newRicetta = new ricetta();
        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement("INSERT INTO  " + TABLE_NAME  + " (visita_id, attiva, medicinali, timestamp) VALUES  (?, ?, ?, ?) RETURNING id");
            ps.setInt(1, v_id);
            ps.setBoolean(2, attiva);
            ps.setString(3, medicinali);
            Timestamp ts = new Timestamp(timestamp.toInstant(ZoneOffset.UTC).toEpochMilli());
            ps.setTimestamp(4, ts);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                int key = rs.getInt(1);
                newRicetta = getRicettaById(key);
            }
            ps.close();
            con.close();
        }
        catch(SQLException ex){
            System.out.println("esamiQueries.newRicetta() error: PreparedStatement.");
            ex.printStackTrace(System.err);
            System.exit(1);
        }

        return newRicetta;
    }

}
