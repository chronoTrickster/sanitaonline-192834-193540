package db;

import classes.paziente;
import classes.ssp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class sspQueries {
    protected static final String TABLE_NAME = "ssp";

    public static ssp getSSPById(int id)
    {
        ssp response_emp = new ssp();

        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement("SELECT * FROM " + TABLE_NAME  + " WHERE id=?");
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();

            if(rs.next())
            {
                response_emp.setId(rs.getInt(1));
                response_emp.setProvincia(rs.getString(2));
            }

            ps.close();
            con.close();
        }
        catch(SQLException ex){
            System.out.println("sspQueries.getSSPById() error: PreparedStatement.");
            ex.printStackTrace(System.err);
            System.exit(1);
        }

        return response_emp;
    }

    public static int getIdSSPByLoginId(int login_id)
    {
        int id = 0;

        try(Connection con = connectDB.getConnection())
        {
            PreparedStatement ps = con.prepareStatement("SELECT * FROM " + TABLE_NAME  + " WHERE login_id=?");
            ps.setInt(1, login_id);

            ResultSet rs = ps.executeQuery();

            if(rs.next())
            {
                id = rs.getInt(1);
            }

            ps.close();
            con.close();
        }
        catch(SQLException ex){
            System.out.println("sspQueries.getIdSSPByLoginId() error: PreparedStatement.");
            ex.printStackTrace(System.err);
            System.exit(1);
        }

        return id;
    }
}
