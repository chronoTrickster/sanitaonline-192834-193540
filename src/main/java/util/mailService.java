package util;

import java.util.Date;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
public class mailService {
    public static void sendMailRicetta(int id_ricetta) {
        final String host = "smtp.gmail.com";
        final String port = "465";
        final String username = "matetovantas@gmail.com";
        final String password = "znsekijhyzhhmpet";
        Properties props = System.getProperties();
        props.setProperty("mail.smtp.host", host);
        props.setProperty("mail.smtp.port", port);
        props.setProperty("mail.smtp.socketFactory.port", port);
        props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.setProperty("mail.smtp.auth", "true");
        props.setProperty("mail.smtp.starttls.enable", "true");
        props.setProperty("mail.debug", "true");
        Session session = Session.getInstance(props, new Authenticator() {

            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });
        Message msg = new MimeMessage(session);
        try {
            msg.setFrom(new InternetAddress(username));
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse("michelangelo.kanaan@studenti.unitn.it", false));
            msg.setSubject("Nuova ricetta.");
            msg.setText("È disponibile una nuova ricetta: http://localhost:8080/ricetta?id=" + id_ricetta);
            msg.setSentDate(new Date());
            Transport.send(msg);
        } catch (MessagingException me) {
            me.printStackTrace(System.err);
        }
    }

    public static void sendMailSpecialistica(int id_specialistica) {
        final String host = "smtp.gmail.com";
        final String port = "465";
        final String username = "matetovantas@gmail.com";
        final String password = "znsekijhyzhhmpet";
        Properties props = System.getProperties();
        props.setProperty("mail.smtp.host", host);
        props.setProperty("mail.smtp.port", port);
        props.setProperty("mail.smtp.socketFactory.port", port);
        props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.setProperty("mail.smtp.auth", "true");
        props.setProperty("mail.smtp.starttls.enable", "true");
        props.setProperty("mail.debug", "true");
        Session session = Session.getInstance(props, new Authenticator() {

            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });
        Message msg = new MimeMessage(session);
        try {
            msg.setFrom(new InternetAddress(username));
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse("michelangelo.kanaan@studenti.unitn.it", false));
            msg.setSubject("Nuovo referto: Visita Specialistica.");
            msg.setText("È disponibile una nuov referto relativo ad una visita specialistica: http://localhost:8080/specialistica?id=" + id_specialistica);
            msg.setSentDate(new Date());
            Transport.send(msg);
        } catch (MessagingException me) {
            me.printStackTrace(System.err);
        }
    }

    public static void sendMailEsame(int id_esame) {
        final String host = "smtp.gmail.com";
        final String port = "465";
        final String username = "matetovantas@gmail.com";
        final String password = "znsekijhyzhhmpet";
        Properties props = System.getProperties();
        props.setProperty("mail.smtp.host", host);
        props.setProperty("mail.smtp.port", port);
        props.setProperty("mail.smtp.socketFactory.port", port);
        props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.setProperty("mail.smtp.auth", "true");
        props.setProperty("mail.smtp.starttls.enable", "true");
        props.setProperty("mail.debug", "true");
        Session session = Session.getInstance(props, new Authenticator() {

            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });
        Message msg = new MimeMessage(session);
        try {
            msg.setFrom(new InternetAddress(username));
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse("michelangelo.kanaan@studenti.unitn.it", false));
            msg.setSubject("Nuovo referto: Esame.");
            msg.setText("È disponibile una nuov referto relativo ad un esame: http://localhost:8080/esame?id=" + id_esame);
            msg.setSentDate(new Date());
            Transport.send(msg);
        } catch (MessagingException me) {
            me.printStackTrace(System.err);
        }
    }
}