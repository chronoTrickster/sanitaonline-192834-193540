package classes;

import java.time.LocalDateTime;

public class specialistica {
    //ATTRIBUTI
    private int id;
    private int visita_id;
    private int tipo_specialistica_id;
    private int dottore_id;
    private Boolean attiva;
    private String provincia;
    private String tipo_specialistica;
    private LocalDateTime timestamp; // Solo se attiva = 0
    private String anamnesi; // Solo se attiva = 0

    private int paziente_id;
    private Boolean pagato;

    //METODI
    public int getId() { return id; }

    public int getVisitaId() { return visita_id; }

    public int getTipoSpecialisticaId() { return tipo_specialistica_id; }

    public int getDottoreId() { return dottore_id; }

    public Boolean getAttiva() { return attiva; }

    public String getProvincia() { return provincia; }

    public String getTipoSpecialistica() { return tipo_specialistica; }

    public LocalDateTime getTimestamp() { return timestamp; }

    public String getAnamnesi() { return anamnesi; }

    public Boolean getPagato() { return pagato; }

    public int getPazienteId() { return paziente_id; }


    public void setId(int id) {  this.id = id; }

    public void setVisitaId(int v_id) {  this.visita_id = v_id; }

    public void setTipoSpecialisticaId(int tipo_specialistica_id) { this.tipo_specialistica_id = tipo_specialistica_id; }

    public void setDottoreId(int d_id) { this.dottore_id = d_id; }

    public void setAttiva(Boolean att) { this.attiva = att; }

    public void setProvincia(String provincia) { this.provincia = provincia; }

    public void setTipoSpecialistica(String tipo_specialistica) { this.tipo_specialistica = tipo_specialistica; }

    public void setTimestamp(LocalDateTime tsp) { this.timestamp = tsp; }

    public void setAnamnesi(String anamnesi) { this.anamnesi = anamnesi; }

    public void setPagato(Boolean pagato) { this.pagato = pagato; }

    public void setPazienteId(int id) { this.paziente_id = id; }
}
