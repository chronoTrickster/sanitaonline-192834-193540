package classes;

import java.time.LocalDateTime;

public class ricetta {
    //ATTRIBUTI
    private int id;
    private int visita_id;
    private Boolean attiva;
    private String medicinali;
    private LocalDateTime timestamp;

    private int paziente_id;
    private int dottore_id;

    private String nomePaziente;
    private String cognomePaziente;
    public String codice_fiscale;
    private String nomeDottore;
    private String cognomeDottore;


    //METODI
    public int getId() { return id; }

    public int getVisitaId() {
        return visita_id;
    }

    public Boolean getAttiva() { return attiva; }

    public LocalDateTime getTimestamp() { return timestamp; }

    public String getMedicinali() { return medicinali; }

    public int getPazienteId() { return paziente_id; }

    public int getDottoreId() { return dottore_id; }

    public String getNomePaziente() { return nomePaziente; }

    public String getCognomePaziente() { return cognomePaziente; }

    public String getNomeDottore() { return nomeDottore; }

    public String getCognomeDottore() { return cognomeDottore; }

    public String getCodiceFiscalePaziente() { return codice_fiscale; }


    public void setId(int id) {
        this.id = id;
    }

    public void setVisitaId(int v_id) {
        this.visita_id = v_id;
    }

    public void setAttiva(Boolean att) { this.attiva = att; }

    public void setMedicinali(String medicinali) { this.medicinali = medicinali; }

    public void setTimestamp(LocalDateTime tsp) { this.timestamp = tsp; }

    public void setPazienteId(int p_id) { this.paziente_id = p_id; }

    public void setDottoreId(int d_id) { this.dottore_id = d_id; }

    public void setNomePaziente(String nome) { this.nomePaziente = nome; }

    public void setCognomePaziente(String cognome) { this.cognomePaziente = cognome; }

    public void setNomeDottore(String nome) { this.nomeDottore = nome; }

    public void setCognomeDottore(String cognome) { this.cognomeDottore = cognome; }

    public void setCodiceFiscalePaziente(String codice_fiscale) { this.codice_fiscale = codice_fiscale; }
}
