package classes;

import java.time.LocalDateTime;

public class visita {
    //ATTRIBUTI
    private int id;
    private int paziente_id;
    private int dottore_id;
    private Boolean attiva;
    private LocalDateTime timestamp;

    // Astrazioni
    private String nomePaziente;
    private String cognomePaziente;
    private String nomeDottore;
    private String cognomeDottore;



    //METODI
    public int getId() { return id; }

    public int getPazienteId() { return paziente_id; }

    public int getDottoreId() { return dottore_id; }

    public Boolean getAttiva() { return attiva; }

    public LocalDateTime getTimestamp() { return timestamp; }

    public String getNomePaziente() { return nomePaziente; }

    public String getCognomePaziente() { return cognomePaziente; }

    public String getNomeDottore() { return nomeDottore; }

    public String getCognomeDottore() { return cognomeDottore; }

    public void setId(int id) { this.id = id; }

    public void setPazienteId(int p_id) { this.paziente_id = p_id; }

    public void setDottoreId(int d_id) { this.dottore_id = d_id; }

    public void setAttiva(Boolean att) { this.attiva = att; }

    public void setTimestamp(LocalDateTime tsp) { this.timestamp = tsp; }

    public void setNomePaziente(String nome) { this.nomePaziente = nome; }

    public void setCognomePaziente(String cognome) { this.cognomePaziente = cognome; }

    public void setNomeDottore(String nome) { this.nomeDottore = nome; }

    public void setCognomeDottore(String cognome) { this.cognomeDottore = cognome; }
}
