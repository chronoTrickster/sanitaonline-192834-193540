package classes;

import java.time.LocalDateTime;

public class ticketSpecialistica {
    // ATTRIBUTI
    private int id;
    private int specialistica_id;
    private int paziente_id;
    private Boolean pagato;
    private LocalDateTime data_pagamento;

    private String nome;

    // METODI

    public int getId() { return id; }

    public int getSpecialisticaId() { return specialistica_id; }

    public int getPazienteId() { return paziente_id; }

    public Boolean getPagato() { return pagato; }

    public LocalDateTime getDataPagamento() { return data_pagamento; }

    public String getNome() { return nome; }

    public void setId(int id) { this.id = id; }

    public void setSpecialisticaId(int s_id) { this.specialistica_id = s_id; }

    public void setPazienteId(int p_id) { this.paziente_id = p_id; }

    public void setPagato(Boolean pagato) { this.pagato = pagato; }

    public void setDataPagamento(LocalDateTime data_pagamento) { this.data_pagamento = data_pagamento; }

    public void setNome(String nome) { this.nome = nome; }

}
