package classes;

public class ssp {
    // ATTRIBUTI
    private int id;
    private String provincia;

    // METODI
    public int getId() { return id; }

    public String getProvincia() { return provincia; }

    public void setId(int id) { this.id = id; }

    public void setProvincia(String provincia) { this.provincia = provincia; }
}
