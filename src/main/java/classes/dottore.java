package classes;

public class dottore {
    //ATTRIBUTI
    private int id;
    private String nome;
    private String cognome;
    private String sesso;
    private String provincia;

    //METODI
    public int getId() { return id; }

    public String getNome() {
        return nome;
    }

    public String getCognome() { return cognome; }

    public String getSesso() { return sesso; }

    public String getProvincia() { return provincia; }

    public void setId(int id) {
        this.id = id;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setCognome(String cognome) { this.cognome = cognome; }

    public void setSesso(String sesso) { this.sesso = sesso; }

    public void setProvincia(String provincia) { this.provincia = provincia; }

}
