package classes;

import java.time.LocalDateTime;

public class paziente {
    //ATTRIBUTI
    private int id;
    private String nome;
    private String cognome;
    private String luogo_nascita;
    private String sesso;
    private int dottore_id;
    private String provincia;
    private String codice_fiscale;
    private LocalDateTime data_di_nascita;
    private String propic;

    private LocalDateTime ultima_visita;
    private LocalDateTime ultima_ricetta;

    //METODI
    public int getId() { return id; }

    public String getNome() {
        return nome;
    }

    public String getCognome() { return cognome; }

    public String getLuogoNascita() { return luogo_nascita; }

    public String getSesso() { return sesso; }

    public int getIdDottore() { return dottore_id; }

    public String getProvincia() { return provincia; }

    public String getCodiceFiscale() { return codice_fiscale; }

    public LocalDateTime getDataDiNascita() { return data_di_nascita; }

    public String getPropic() { return propic; }

    public LocalDateTime getUltimaVisita() { return ultima_visita; }

    public LocalDateTime getUltimaRicetta() { return ultima_ricetta; }



    public void setId(int id) {
        this.id = id;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setCognome(String cognome) { this.cognome = cognome; }

    public void setLuogoNascita(String luogo_nascita) { this.luogo_nascita = luogo_nascita; }

    public void setSesso(String sesso) { this.sesso = sesso; }

    public void setIdDottore(int dottore_id) { this.dottore_id = dottore_id; }

    public void setProvincia(String provincia) { this.provincia = provincia; }

    public void setCodiceFiscale(String cf) { this.codice_fiscale = cf; }

    public void setDataDiNascita(LocalDateTime ddn) { this.data_di_nascita = ddn; }

    public void setPropic(String pp) { this.propic = pp; }

    public void setUltimaVisita(LocalDateTime duv) { this.ultima_visita = duv; }

    public void setUltimaRicetta(LocalDateTime dur) { this.ultima_ricetta = dur; }
}
