package classes;

import java.time.LocalDateTime;

public class esame {
    //ATTRIBUTI
    private int id;
    private int visita_id;
    private int tipo_esame_id;
    private Boolean attiva;
    private String provincia;
    private String tipo_esame;
    private LocalDateTime timestamp; // Solo se attiva = 0
    private String anamnesi; // Solo se attiva = 0

    private int paziente_id;
    private Boolean pagato;

    //METODI
    public int getId() { return id; }

    public int getVisitaId() { return visita_id; }

    public int getTipoEsameId() { return tipo_esame_id; }

    public Boolean getAttiva() { return attiva; }

    public String getProvincia() { return provincia; }

    public String getTipoEsame() { return tipo_esame; }

    public LocalDateTime getTimestamp() { return timestamp; }

    public String getAnamnesi() { return anamnesi; }

    public Boolean getPagato() { return pagato; }

    public int getPazienteId() { return paziente_id; }


    public void setId(int id) {  this.id = id; }

    public void setVisitaId(int v_id) {  this.visita_id = v_id; }

    public void setTipoEsameId(int t_esame_id) { this.tipo_esame_id = t_esame_id; }

    public void setAttiva(Boolean att) { this.attiva = att; }

    public void setProvincia(String provincia) { this.provincia = provincia; }

    public void setTipoEsame(String tipo_esame) { this.tipo_esame = tipo_esame; }

    public void setTimestamp(LocalDateTime tsp) { this.timestamp = tsp; }

    public void setAnamnesi(String anamnesi) { this.anamnesi = anamnesi; }

    public void setPagato(Boolean pagato) { this.pagato = pagato; }

    public void setPazienteId(int id) { this.paziente_id = id; }
}
