package classes;

import java.time.LocalDateTime;

public class risultatoEsame {
    //ATTRIBUTI
    private int id;
    private int esame_id;
    private String anamnesi;
    private LocalDateTime timestamp;

    //METODI

    public int getId() { return id; }

    public int getEsameId() { return esame_id; }

    public String getAnamnesi() { return anamnesi; }

    public LocalDateTime getTimestamp() { return timestamp; }


    public void setId(int id) { this.id = id; }

    public void setEsameId(int id) { this.esame_id = id; }

    public void setAnamnesi(String anamnesi) { this.anamnesi = anamnesi; }

    public void setTimestamp(LocalDateTime tsp) { this.timestamp = tsp; }
}
