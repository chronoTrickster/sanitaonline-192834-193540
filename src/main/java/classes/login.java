package classes;

public class login {

    //ATTRIBUTI
    private int id;
    private String email;
    private String password;
    private int user_type;

    //METODI
    public int getId() { return id; }

    public String getEmail() {
        return email;
    }

    public String getPassword() { return password; }

    public int getUserType() { return user_type; }

    public void setId(int id) { this.id = id; }

    public void setEmail(String email) { this.email = email; }

    public void setPassword(String password) { this.password = password; }

    public void setUserType(int user_type) { this.user_type = user_type; }
}
