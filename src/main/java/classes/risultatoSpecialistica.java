package classes;

import java.time.LocalDateTime;

public class risultatoSpecialistica {
    //ATTRIBUTI
    private int id;
    private int specialistica_id;
    private String anamnesi;
    private LocalDateTime timestamp;

    //METODI

    public int getId() { return id; }

    public int getSpecialisticaId() { return specialistica_id; }

    public String getAnamnesi() { return anamnesi; }

    public LocalDateTime getTimestamp() { return timestamp; }


    public void setId(int id) { this.id = id; }

    public void setSpecialisticaId(int id) { this.specialistica_id = id; }

    public void setAnamnesi(String anamnesi) { this.anamnesi = anamnesi; }

    public void setTimestamp(LocalDateTime tsp) { this.timestamp = tsp; }
}
