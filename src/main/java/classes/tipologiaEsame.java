package classes;

public class tipologiaEsame {
    // ATTRIBUTI
    private int id;
    private String nome;

    // METODI
    public int getId() { return id; }

    public String getNome() { return nome; }

    public void setId(int id) { this.id = id; }

    public void setNome(String nome) { this.nome = nome; }
}
