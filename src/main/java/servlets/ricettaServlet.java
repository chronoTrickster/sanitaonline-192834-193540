package servlets;

import classes.dottore;
import classes.paziente;
import classes.ricetta;
import classes.visita;
import db.dottoriQueries;
import db.pazientiQueries;
import db.ricetteQueries;
import db.visiteQueries;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ricettaServlet extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        String r_id = request.getParameter("id");
        ricetta r = null;
        String usertype = "";
        String id = "";

        Cookie[] cookies = request.getCookies();

        for(int i = 0; i < cookies.length; i++)
        {
            if ((cookies[i].getName().equals("user_type")) && (cookies[i].getValue() != null))
            {
                usertype = cookies[i].getValue();
            }

            if ((cookies[i].getName().equals("id")) && (cookies[i].getValue() != null))
            {
                id = cookies[i].getValue();
            }
        }
        if (r_id != null && !r_id.trim().isEmpty())
        {
            r = ricetteQueries.getRicettaById(Integer.parseInt(r_id));
            if (r != null)
            {
                if ((usertype.equals("dottore")) || (usertype.equals("paziente") && Integer.parseInt(id) == r.getPazienteId()) || (usertype.equals("SSP")))
                {
                    // L'utente loggato è il dottore o il paziente della ricetta o il SSP .
                    paziente p = pazientiQueries.getPazienteById(r.getPazienteId());

                    dottore d = dottoriQueries.getDottoreById(r.getDottoreId());


                    request.setAttribute("paziente", p);
                    request.setAttribute("dottore", d);
                    request.setAttribute("ricetta", r);
                    request.getRequestDispatcher("ricetta.jsp").include(request,response);
                }
                else
                {
                    // L'utente loggato non è autorizzato ad accedere a queste informazioni.
                    response.sendRedirect("/home");
                    request.getRequestDispatcher("/home").include(request,response);
                }
            }
            else
            {
                response.sendRedirect("/home");
                request.getRequestDispatcher("/home").include(request,response);
            }
        }



    }


}
