package servlets;

import classes.esame;
import classes.ricetta;
import classes.specialistica;
import classes.visita;
import db.*;
import util.mailService;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class anamnesiEsameServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        String e_id = request.getParameter("id");
        esame e = null;
        String usertype = "";
        String id = "";

        Cookie[] cookies = request.getCookies();

        for(int i = 0; i < cookies.length; i++)
        {
            if ((cookies[i].getName().equals("user_type")) && (cookies[i].getValue() != null))
            {
                usertype = cookies[i].getValue();
            }

            if ((cookies[i].getName().equals("id")) && (cookies[i].getValue() != null))
            {
                id = cookies[i].getValue();
            }

        }

        if (!usertype.equals("ssp"))
        {
            // Accesso negato.
            response.sendRedirect("/home");
            return;
        }

        if (e_id != null && !e_id.trim().isEmpty())
        {
            e = esamiQueries.getEsameById(Integer.parseInt(e_id));
            if (e != null)
            {
                // L'utente loggato è SSP
                if(e.getAttiva())
                {
                    // L'esame è ancora attivo, quindi SSP può scrivere la sua anamnesi
                    request.setAttribute("esame", e);
                    request.getRequestDispatcher("anamnesiEsame.jsp").include(request,response);
                }
                else
                {
                    // L'esame non è attiva, ma lo visualizziamo anyway.
                    response.sendRedirect("esame?id=" + e.getId());
                }

            }
            else
            {
                response.sendRedirect("/home");
                request.getRequestDispatcher("/home").include(request,response);
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();

        String e_id = request.getParameter("id");
        String anamnesi = request.getParameter("anamnesi");


        esame e = null;
        String usertype = "";
        String id = "";

        Cookie[] cookies = request.getCookies();


        for(int i = 0; i < cookies.length; i++)
        {
            if ((cookies[i].getName().equals("user_type")) && (cookies[i].getValue() != null))
            {
                usertype = cookies[i].getValue();
            }

            if ((cookies[i].getName().equals("id")) && (cookies[i].getValue() != null))
            {
                id = cookies[i].getValue();
            }

        }

        if (!usertype.equals("ssp"))
        {
            // Accesso negato.
            response.sendRedirect("/home");
            return;
        }



        if (e_id != null && !e_id.trim().isEmpty())
        {
            e = esamiQueries.getEsameById(Integer.parseInt(e_id));
            if (e != null)
            {
                // L'utente loggato è il dottore associato alla specialistica.
                if(e.getAttiva())
                {
                    // L'esame è ancora attiva, quindi SSP può scrivere la sua anamnesi
                    int i = risultatiEsamiQueries.newRisultatoEsame(e.getId(), anamnesi, LocalDateTime.now(ZoneOffset.UTC));
                    esamiQueries.disattivaEsameById(e.getId());
                    mailService.sendMailEsame(e.getId());
                    response.sendRedirect("esame?id=" + e.getId());
                }
                else
                {
                    response.sendRedirect("esame?id=" + e.getId());
                }
            }
            else
            {
                response.sendRedirect("/home");
                request.getRequestDispatcher("/home").include(request,response);
            }
        }

    }
}
