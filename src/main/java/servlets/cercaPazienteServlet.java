package servlets;

import classes.*;
import db.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class cercaPazienteServlet extends HttpServlet {


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        String p_id = request.getParameter("id");
        paziente p = null;
        String usertype = "";
        String id = "";

        Cookie[] cookies = request.getCookies();



        for(int i = 0; i < cookies.length; i++)
        {
            if ((cookies[i].getName().equals("user_type")) && (cookies[i].getValue() != null))
            {
                usertype = cookies[i].getValue();
            }

            if ((cookies[i].getName().equals("id")) && (cookies[i].getValue() != null))
            {
                id = cookies[i].getValue();
            }

        }

        if (!usertype.equals("dottore"))
        {
            response.sendRedirect("/home");
            return;
        }

        if (p_id != null && !p_id.trim().isEmpty())
        {
            p = pazientiQueries.getPazienteById(Integer.parseInt(p_id));
            if (p != null)
            {
                if(Integer.parseInt(id) == p.getIdDottore())
                {
                    // L'utente loggato è il dottore del paziente.
                    List<visita> v = visiteQueries.getVisiteByIdPaziente(p.getId());

                    List<ricetta> r = ricetteQueries.getRicetteByIdPaziente(p.getId());
                    List<esame> e = esamiQueries.getEsamiByIdPaziente(p.getId());
                    List<specialistica> s = specialisticheQueries.getSpecialisticheByIdPaziente(p.getId());

                    request.setAttribute("paziente", p);
                    request.setAttribute("visite", v);
                    request.setAttribute("ricette", r);
                    request.setAttribute("esami", e);
                    request.setAttribute("specialistiche", s);
                    request.getRequestDispatcher("paziente.jsp").include(request,response);
                }
                else
                {
                    // L'utente loggato non è il dottore del paziente.
                    response.sendRedirect("/home");
                    request.getRequestDispatcher("/home").include(request,response);
                }
            }
            else
            {
                response.sendRedirect("/home");
                request.getRequestDispatcher("/home").include(request,response);
            }
        }

    }


}
