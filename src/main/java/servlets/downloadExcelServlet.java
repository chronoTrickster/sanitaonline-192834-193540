package servlets;

import classes.ricetta;
import classes.ssp;
import db.ricetteQueries;
import db.sspQueries;
import org.apache.poi.hssf.usermodel.*;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


// Extend HttpServlet class
public class downloadExcelServlet extends HttpServlet {
    private static final long serialVersionUID = 2067115822080269398L;

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            String data = request.getParameter("data");
            response.setContentType("application/vnd.ms-excel");
            String usertype = "";
            String id = "";
            String nome = data;


            Cookie[] cookies = request.getCookies();
            for(int i = 0; i < cookies.length; i++)
            {
                if ((cookies[i].getName().equals("user_type")) && (cookies[i].getValue() != null))
                {
                    usertype = cookies[i].getValue();
                }

                if ((cookies[i].getName().equals("id")) && (cookies[i].getValue() != null))
                {
                    id = cookies[i].getValue();
                }

            }

            if(!usertype.equals("ssp"))
            {
                // Accesso negato.
                response.sendRedirect("/home");
                return;
            }
            ssp s = sspQueries.getSSPById(Integer.parseInt(id));


            if(data != null && !data.trim().isEmpty())
            {
                data = data + "T00:00:00.000000000";
                nome = "Report del " + nome;
                response.setHeader("Content-Disposition", "attachment; filename=" + nome + ".xls");
                LocalDateTime dateTime =LocalDateTime.parse(data);
                dateTime = dateTime.minusHours(1);
                List<ricetta> ricette = ricetteQueries.getRicetteByProvinciaInGiornata(s.getProvincia(), dateTime);
                HSSFWorkbook workbook = createExcel(s, ricette);
                workbook.write(response.getOutputStream());
            }
            else
            {
                nome = "Report completo";
                response.setHeader("Content-Disposition", "attachment; filename=" + nome + ".xls");
                List<ricetta> ricette = ricetteQueries.getRicetteByProvincia(s.getProvincia());
                HSSFWorkbook workbook = createExcel(s, ricette);
                workbook.write(response.getOutputStream());
            }

        } catch (Exception e) {
            throw new ServletException("Exception in DownLoad Excel Servlet", e);
        }
    }

    private HSSFWorkbook createExcel(ssp s, List<ricetta> ricette) {
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet worksheet = workbook.createSheet("Lista Ricette");

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        HSSFRow row1 = worksheet.createRow(0);

        HSSFCell cellA1 = row1.createCell(0);
        cellA1.setCellValue("Data");
        HSSFCellStyle cellStyle = workbook.createCellStyle();
        cellA1.setCellStyle(cellStyle);

        cellA1 = row1.createCell(1);
        cellA1.setCellValue("Farmaci");
         cellStyle = workbook.createCellStyle();
        cellA1.setCellStyle(cellStyle);

        cellA1 = row1.createCell(2);
        cellA1.setCellValue("Id Medico");
         cellStyle = workbook.createCellStyle();
        cellA1.setCellStyle(cellStyle);

        cellA1 = row1.createCell(3);
        cellA1.setCellValue("Nome Medico: ");
        cellStyle = workbook.createCellStyle();
        cellA1.setCellStyle(cellStyle);

        cellA1 = row1.createCell(4);
        cellA1.setCellValue("Cognome Medico: ");
        cellStyle = workbook.createCellStyle();
        cellA1.setCellStyle(cellStyle);

        cellA1 = row1.createCell(5);
        cellA1.setCellValue("Nome Paziente: ");
        cellStyle = workbook.createCellStyle();
        cellA1.setCellStyle(cellStyle);

        cellA1 = row1.createCell(6);
        cellA1.setCellValue("Cognome Paziente: ");
        cellStyle = workbook.createCellStyle();
        cellA1.setCellStyle(cellStyle);

        cellA1 = row1.createCell(7);
        cellA1.setCellValue("Codice Fiscale Paziente: ");
        cellStyle = workbook.createCellStyle();
        cellA1.setCellStyle(cellStyle);
        int i = 1;
        for (ricetta ricetta : ricette)
        {
            /* (dataora, farmaco, farmacia, medico di base,
            paziente, tiket, solo per la provincia di appartenenza)*/
            row1 = worksheet.createRow(i);

            cellA1 = row1.createCell(0);
            cellA1.setCellValue(ricetta.getTimestamp().format(formatter));
            cellStyle = workbook.createCellStyle();
            cellA1.setCellStyle(cellStyle);

            cellA1 = row1.createCell(1);
            cellA1.setCellValue(ricetta.getMedicinali());
            cellStyle = workbook.createCellStyle();
            cellA1.setCellStyle(cellStyle);

            cellA1 = row1.createCell(2);
            cellA1.setCellValue(ricetta.getDottoreId());
            cellStyle = workbook.createCellStyle();
            cellA1.setCellStyle(cellStyle);

            cellA1 = row1.createCell(3);
            cellA1.setCellValue(ricetta.getNomeDottore());
            cellStyle = workbook.createCellStyle();
            cellA1.setCellStyle(cellStyle);

            cellA1 = row1.createCell(4);
            cellA1.setCellValue(ricetta.getCognomeDottore());
            cellStyle = workbook.createCellStyle();
            cellA1.setCellStyle(cellStyle);

            cellA1 = row1.createCell(5);
            cellA1.setCellValue(ricetta.getNomePaziente());
            cellStyle = workbook.createCellStyle();
            cellA1.setCellStyle(cellStyle);

            cellA1 = row1.createCell(6);
            cellA1.setCellValue(ricetta.getCognomePaziente());
            cellStyle = workbook.createCellStyle();
            cellA1.setCellStyle(cellStyle);

            cellA1 = row1.createCell(7);
            cellA1.setCellValue(ricetta.getCodiceFiscalePaziente());
            cellStyle = workbook.createCellStyle();
            cellA1.setCellStyle(cellStyle);

            i++;
        }
        return workbook;
    }

    public void destroy() {
        // do nothing.
    }
}