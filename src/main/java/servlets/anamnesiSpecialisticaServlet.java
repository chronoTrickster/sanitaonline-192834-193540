package servlets;

import classes.ricetta;
import classes.specialistica;
import classes.visita;
import db.ricetteQueries;
import db.risultatiSpecialisticheQueries;
import db.specialisticheQueries;
import db.visiteQueries;
import util.mailService;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class anamnesiSpecialisticaServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        String s_id = request.getParameter("id");
        specialistica s = null;
        String usertype = "";
        String id = "";

        Cookie[] cookies = request.getCookies();

        for(int i = 0; i < cookies.length; i++)
        {
            if ((cookies[i].getName().equals("user_type")) && (cookies[i].getValue() != null))
            {
                usertype = cookies[i].getValue();
            }

            if ((cookies[i].getName().equals("id")) && (cookies[i].getValue() != null))
            {
                id = cookies[i].getValue();
            }

        }

        if (!usertype.equals("dottore"))
        {
            response.sendRedirect("/home");
            return;
        }

        if (s_id != null && !s_id.trim().isEmpty())
        {
            s = specialisticheQueries.getSpecialisticaById(Integer.parseInt(s_id));
            if (s != null)
            {
                if(Integer.parseInt(id) == s.getDottoreId())
                {
                    // L'utente loggato è il dottore associato alla specialistica.
                    if(s.getAttiva())
                    {
                        // La specialistica è ancora attiva, quindi il dottore può scrivere la sua anamnesi
                        request.setAttribute("specialistica", s);
                        request.getRequestDispatcher("anamnesiSpecialistica.jsp").include(request,response);
                    }
                    else
                    {
                        // La specialistica non è attiva, ma la visualizziamo anyway.
                        response.sendRedirect("specialistica?id=" + s.getId());
                    }
                }
                else
                {
                    // L'utente loggato non è il dottore della specialistica.
                    response.sendRedirect("/home");
                    request.getRequestDispatcher("/home").include(request,response);
                }
            }
            else
            {
                response.sendRedirect("/home");
                request.getRequestDispatcher("/home").include(request,response);
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();

        String s_id = request.getParameter("id");
        String anamnesi = request.getParameter("anamnesi");

        specialistica s = null;
        String usertype = "";
        String id = "";

        Cookie[] cookies = request.getCookies();


        for(int i = 0; i < cookies.length; i++)
        {
            if ((cookies[i].getName().equals("user_type")) && (cookies[i].getValue() != null))
            {
                usertype = cookies[i].getValue();
            }

            if ((cookies[i].getName().equals("id")) && (cookies[i].getValue() != null))
            {
                id = cookies[i].getValue();
            }

        }

        if (!usertype.equals("dottore"))
        {
            response.sendRedirect("/home");
            return;
        }



        if (s_id != null && !s_id.trim().isEmpty())
        {
            s = specialisticheQueries.getSpecialisticaById(Integer.parseInt(s_id));
            if (s != null)
            {
                if(Integer.parseInt(id) == s.getDottoreId())
                {
                    if(s.getAttiva())
                    {
                        // La specialistica è ancora attiva, quindi il dottore può scrivere la sua anamnesi
                        int i = risultatiSpecialisticheQueries.newRisultatoSpecialistica(s.getId(), anamnesi, LocalDateTime.now(ZoneOffset.UTC));
                        specialisticheQueries.disattivaSpecialisticaById(s.getId());
                        mailService.sendMailSpecialistica(s.getId());
                        response.sendRedirect("specialistica?id=" + s.getId());
                    }
                    else
                    {
                        // La specialistica non è attiva, ma la visualizziamo anyway.
                        response.sendRedirect("specialistica?id=" + s.getId());
                    }
                }
                else
                {
                    // L'utente loggato non è il dottore della specialistica.
                    response.sendRedirect("/home");
                    request.getRequestDispatcher("/home").include(request,response);
                }
            }
            else
            {
                response.sendRedirect("/home");
                request.getRequestDispatcher("/home").include(request,response);
            }
        }

    }
}
