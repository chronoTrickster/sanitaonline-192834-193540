package servlets;

import classes.paziente;
import classes.ricetta;
import db.pazientiQueries;
import db.ricetteQueries;
import net.glxn.qrgen.QRCode;
import net.glxn.qrgen.image.ImageType;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.format.DateTimeFormatter;

public class stampaRicettaServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=RICETTA.jpg");

        String r_id = request.getParameter("id");

        ricetta r = ricetteQueries.getRicettaById(Integer.parseInt(r_id));
        paziente p = pazientiQueries.getPazienteById(r.getPazienteId());

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        /* con Codice medico, codicefiscale
        paziente, timestamp prescrizione, codice univo prescrizione, descrizione farmaco) */

        QRCode.from("Codice Medico: " + r.getDottoreId() + "\n" +
                    "Codice Fiscale: " + p.getCodiceFiscale() + "\n" +
                    "Data prescrizione: " + r.getTimestamp().format(formatter) + "\n" +
                    "Codice Ricetta: " + r.getId() + "\n" +
                    "Farmaci: " + r.getMedicinali()
                    ) // settiamo il testo da codificare
                .to(ImageType.JPG) // settiamo il tipo di immagine da generare
                .withSize(250, 250) // settiamo la dimensione dell'immagine
                .writeTo(response.getOutputStream()); // scriviamo l'immagine su un file
    }
}
