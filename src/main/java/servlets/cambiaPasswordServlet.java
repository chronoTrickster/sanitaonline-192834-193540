package servlets;

import db.dottoriQueries;
import db.loginQueries;
import classes.login;
import db.pazientiQueries;
import db.sspQueries;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class cambiaPasswordServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        response.sendRedirect("/cambiaPassword.jsp");
        request.getRequestDispatcher("/cambiaPassword.jsp").include(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();

        int id = 0;

        String email = request.getParameter("email");
        String password = request.getParameter("vecchiaPassword");
        String nuovaPassword = request.getParameter("nuovaPassword");

        login login_info = loginQueries.getLoginByEmail(email);

        if((login_info.getEmail() != null) && (password.equals(login_info.getPassword())))
        {
            loginQueries.changePassword(login_info.getId(), nuovaPassword);
            request.setAttribute("successo", "Password cambiata con successo.");
            request.getRequestDispatcher("cambiaPassword.jsp").include(request, response);
        }
        else
        {
            request.setAttribute("errore", "Username o password errati.");
            request.getRequestDispatcher("cambiaPassword.jsp").include(request, response);
        }

        out.close();
    }

}