package servlets;

import classes.paziente;
import classes.visita;
import db.pazientiQueries;
import db.visiteQueries;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.List;

public class cambiaPropicServlet extends HttpServlet {

    private boolean isMultipart;
    private String filePath;
    private int maxFileSize = 52428800;
    private int maxMemSize = 52428800;
    private File file ;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String usertype = "";
        String id = "";
        Cookie[] cookies = request.getCookies();


        for(int i = 0; i < cookies.length; i++)
        {
            if ((cookies[i].getName().equals("user_type")) && (cookies[i].getValue() != null))
            {
                usertype = cookies[i].getValue();
            }

            if ((cookies[i].getName().equals("id")) && (cookies[i].getValue() != null))
            {
                id = cookies[i].getValue();
            }

        }

        if (!usertype.equals("paziente"))
        {
            // Accesso negato.
            response.sendRedirect("/home");
            return;
        }



        isMultipart = ServletFileUpload.isMultipartContent(request);
        response.setContentType("text/html");
        java.io.PrintWriter out = response.getWriter( );


        if( !isMultipart ) {
            response.sendRedirect("/home");
            return;
        }

        DiskFileItemFactory factory = new DiskFileItemFactory();

        factory.setSizeThreshold(maxMemSize);

        factory.setRepository(new File("c:\\temp"));

        ServletFileUpload upload = new ServletFileUpload(factory);

        upload.setSizeMax( maxFileSize );

        try {
            List fileItems = upload.parseRequest(request);

            Iterator i = fileItems.iterator();

            while ( i.hasNext () ) {
                FileItem fi = (FileItem)i.next();
                if ( !fi.isFormField () ) {

                    String fieldName = fi.getFieldName();
                    String fileName = id + ".jpg";
                    String contentType = fi.getContentType();
                    boolean isInMemory = fi.isInMemory();
                    long sizeInBytes = fi.getSize();

                    if( fileName.lastIndexOf("\\") >= 0 ) {
                        file = new File( getServletContext().getRealPath("/") + "assets/propic/" + fileName.substring( fileName.lastIndexOf("\\"))) ;
                    } else {
                        file = new File( getServletContext().getRealPath("/") + "assets/propic/" + fileName.substring(fileName.lastIndexOf("\\")+1)) ;
                    }
                    if (file.exists())
                    {
                        // Rimpiazziamo la foto precedente
                        file.delete();
                    }
                    fi.write( file ) ;
                    response.sendRedirect("/home");
                }
            }
        } catch(Exception ex) {
            System.out.println(ex);
        }
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, java.io.IOException {

        throw new ServletException("GET method used with " +
                getClass().getName() + ": POST method required.");
    }
}