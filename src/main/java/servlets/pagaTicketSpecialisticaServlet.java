package servlets;

import classes.ticketSpecialistica;
import db.*;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class pagaTicketSpecialisticaServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();

        String t_id = request.getParameter("id");

        ticketSpecialistica t = null;
        String usertype = "";
        String id = "";

        Cookie[] cookies = request.getCookies();


        for(int i = 0; i < cookies.length; i++)
        {
            if ((cookies[i].getName().equals("user_type")) && (cookies[i].getValue() != null))
            {
                usertype = cookies[i].getValue();
            }

            if ((cookies[i].getName().equals("id")) && (cookies[i].getValue() != null))
            {
                id = cookies[i].getValue();
            }

        }

        if (!usertype.equals("paziente"))
        {
            // Accesso negato.
            response.sendRedirect("/home");
            return;
        }



        if (t_id != null && !t_id.trim().isEmpty())
        {
            t = ticketSpecialisticheQueries.getTicketById(Integer.parseInt(t_id));
            if (t != null)
            {
                if(Integer.parseInt(id) == t.getPazienteId())
                {
                    // L'utente loggato è il paziente associato al ticket.
                    ticketSpecialisticheQueries.pagaTicketById(t.getId());
                    response.sendRedirect("/profiloPersonale");
                }
                else
                {
                    // L'utente loggato non è il dottore del paziente.
                    response.sendRedirect("/home");
                    request.getRequestDispatcher("/home").include(request,response);
                }
            }
            else
            {
                response.sendRedirect("/home");
                request.getRequestDispatcher("/home").include(request,response);
            }
        }

    }
}
