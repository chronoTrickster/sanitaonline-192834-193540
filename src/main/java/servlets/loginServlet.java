package servlets;

import db.dottoriQueries;
import db.loginQueries;
import classes.login;
import db.pazientiQueries;
import db.sspQueries;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class loginServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();

        int id = 0;

        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String ricordami = request.getParameter("ricordami");

        login login_info = loginQueries.getLoginByEmail(email);

        if((login_info.getEmail() != null) && (password.equals(login_info.getPassword()))){

            id = loginQueries.getExIdByLogin(login_info.getId());

            String nome = "";
            String usertype = "";
            if(login_info.getUserType() == 0)
            {
                usertype = "paziente";
                nome = pazientiQueries.getPazienteById(id).getNome();
            }
            else if (login_info.getUserType() == 1)
            {
                usertype = "dottore";
                nome = dottoriQueries.getDottoreById(id).getNome();
            }
            else if (login_info.getUserType() == 2)
            {
                usertype = "ssp";
                nome = sspQueries.getSSPById(id).getProvincia();
            }
            Cookie ck_user_type = new Cookie("user_type", usertype);
            Cookie ck_id = new Cookie("id", Integer.toString(id));
            Cookie ck_nome = new Cookie("nome", nome);

            if(ricordami != null)
            {
                ck_user_type.setMaxAge(60 * 24 * 3600);
                ck_user_type.setPath("/");
                ck_id.setMaxAge(60 * 24 * 3600);
                ck_id.setPath("/");
                ck_nome.setMaxAge(60 * 24 * 3600);
                ck_nome.setPath("/");
            }

            response.addCookie(ck_nome);
            response.addCookie(ck_user_type);
            response.addCookie(ck_id);

            // request.setAttribute("test", "test");
            response.sendRedirect("/home");
        }
        else{
            request.setAttribute("errore", "Username o password errati.");
            request.getRequestDispatcher("login.jsp").include(request,response);
        }

        out.close();
    }

}
