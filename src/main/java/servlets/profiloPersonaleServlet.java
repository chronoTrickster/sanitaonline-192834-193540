package servlets;

import classes.paziente;
import classes.ticketEsame;
import classes.ticketSpecialistica;
import classes.visita;
import db.pazientiQueries;
import db.ticketEsamiQueries;
import db.ticketSpecialisticheQueries;
import db.visiteQueries;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class profiloPersonaleServlet extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        Cookie[] cookies = request.getCookies();

        if (cookies == null)
        {
            response.sendRedirect("login.jsp");
        }

        String usertype = "";
        String id = "";
        for(int i = 0; i < cookies.length; i++)
        {
            if ((cookies[i].getName().equals("user_type")) && (cookies[i].getValue() != null))
            {
                usertype = cookies[i].getValue();
            }

            if ((cookies[i].getName().equals("id")) && (cookies[i].getValue() != null))
            {
                id = cookies[i].getValue();
            }
        }

        if(usertype.equals("paziente"))
        {
            // Link al profilo paziente
            paziente p = pazientiQueries.getPazienteById(Integer.parseInt(id));
            request.setAttribute("paziente", p);

            List<visita> v = visiteQueries.getVisiteByIdPaziente(Integer.parseInt(id));
            request.setAttribute("visite", v);

            List<ticketEsame> t_e = ticketEsamiQueries.getTicketEsamiByPazienteId(Integer.parseInt(id));
            request.setAttribute("ticket_esami", t_e);

            List<ticketSpecialistica> t_s = ticketSpecialisticheQueries.getTicketSpecialisticheByPazienteId(Integer.parseInt(id));
            request.setAttribute("ticket_specialistiche", t_s);

            request.getRequestDispatcher("profiloPaziente.jsp").include(request,response);
        }
        else if(usertype.equals("dottore"))
        {
            // etc etc getDottoreById

        }
        else
        {
            request.getRequestDispatcher("login.jsp").include(request, response);
        }

    }



}