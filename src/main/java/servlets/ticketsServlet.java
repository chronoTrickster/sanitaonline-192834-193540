package servlets;

import classes.paziente;
import classes.ticketEsame;
import classes.ticketSpecialistica;
import db.pazientiQueries;
import db.ticketEsamiQueries;
import db.ticketSpecialisticheQueries;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class ticketsServlet extends HttpServlet  {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        Cookie[] cookies = request.getCookies();

        String usertype = "";
        String id = "";
        for(int i = 0; i < cookies.length; i++)
        {
            if ((cookies[i].getName().equals("user_type")) && (cookies[i].getValue() != null))
            {
                usertype = cookies[i].getValue();
            }

            if ((cookies[i].getName().equals("id")) && (cookies[i].getValue() != null))
            {
                id = cookies[i].getValue();
                System.out.println("Trovato cookie id: "+ id);
            }
        }

        if(usertype.equals("paziente"))
        {

            List<ticketEsame> t_e = ticketEsamiQueries.getTicketEsamiByPazienteId(Integer.parseInt(id));
            List<ticketSpecialistica> t_s = ticketSpecialisticheQueries.getTicketSpecialisticheByPazienteId(Integer.parseInt(id));
            paziente p = pazientiQueries.getPazienteById(Integer.parseInt(id));

            request.setAttribute("paziente", p);
            request.setAttribute("ticketEsami", t_e);
            request.setAttribute("ticketSpecialistiche", t_s);
            request.getRequestDispatcher("tickets.jsp").include(request, response);
        }
        else
        {
            request.getRequestDispatcher("/home").include(request, response);
        }

    }

}
