package servlets;

import classes.dottore;
import classes.paziente;
import db.dottoriQueries;
import db.loginQueries;
import classes.login;
import db.pazientiQueries;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class cambiaDottoreServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        String usertype = "";
        String id = "";
        Cookie[] cookies = request.getCookies();


        for(int i = 0; i < cookies.length; i++)
        {
            if ((cookies[i].getName().equals("user_type")) && (cookies[i].getValue() != null))
            {
                usertype = cookies[i].getValue();
            }

            if ((cookies[i].getName().equals("id")) && (cookies[i].getValue() != null))
            {
                id = cookies[i].getValue();
            }

        }

        if (!usertype.equals("paziente"))
        {
            response.sendRedirect("/home");
            return;
        }

        paziente p = pazientiQueries.getPazienteById(Integer.parseInt(id));

        List<dottore> dottoriDisponibili = dottoriQueries.getDottoriByProvincia(p.getProvincia());

        dottore d = new dottore();
        for (int i = 0; i < dottoriDisponibili.size(); i++) {
            // Togliamo come disponibile il dottore che ha già.
            if(p.getIdDottore() == dottoriDisponibili.get(i).getId())
            {
                d = dottoriDisponibili.get(i);
                dottoriDisponibili.remove(i);
            }
        }
        request.setAttribute("dottore", d);
        request.setAttribute("dottoriDisponibili", dottoriDisponibili);
        request.getRequestDispatcher("cambiaDottore.jsp").include(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        String d_id = request.getParameter("id");

        paziente p = null;
        dottore d = null;
        String usertype = "";
        String id = "";

        Cookie[] cookies = request.getCookies();

        for(int i = 0; i < cookies.length; i++)
        {
            if ((cookies[i].getName().equals("user_type")) && (cookies[i].getValue() != null))
            {
                usertype = cookies[i].getValue();
            }

            if ((cookies[i].getName().equals("id")) && (cookies[i].getValue() != null))
            {
                id = cookies[i].getValue();
            }

        }

        if (!usertype.equals("paziente"))
        {
            response.sendRedirect("/home");
            return;
        }

        if (d_id != null && !d_id.trim().isEmpty())
        {
            d = dottoriQueries.getDottoreById(Integer.parseInt(d_id));

            if (d != null)
            {
                p = pazientiQueries.getPazienteById(Integer.parseInt(id));
                if(p.getProvincia().equals(d.getProvincia()))
                {
                    pazientiQueries.updatePazienteDottoreById(p.getId(), d.getId());
                    response.sendRedirect("/cambiaDottore");
                }
                else
                {
                    response.sendRedirect("/cambiaDottore");
                    request.getRequestDispatcher("/cambiaDottore").include(request,response);
                }
            }
            else
            {
                response.sendRedirect("/cambiaDottore");
                request.getRequestDispatcher("/cambiaDottore").include(request,response);
            }
        }


    }

}