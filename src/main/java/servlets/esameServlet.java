package servlets;

import classes.dottore;
import classes.esame;
import classes.paziente;
import classes.ricetta;
import db.dottoriQueries;
import db.esamiQueries;
import db.pazientiQueries;
import db.ricetteQueries;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class esameServlet extends HttpServlet  {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        String e_id = request.getParameter("id");
        esame e = null;
        String usertype = "";
        String id = "";

        Cookie[] cookies = request.getCookies();

        for(int i = 0; i < cookies.length; i++)
        {
            if ((cookies[i].getName().equals("user_type")) && (cookies[i].getValue() != null))
            {
                usertype = cookies[i].getValue();
            }

            if ((cookies[i].getName().equals("id")) && (cookies[i].getValue() != null))
            {
                id = cookies[i].getValue();
            }
        }
        if (e_id != null && !e_id.trim().isEmpty())
        {
            e = esamiQueries.getEsameById(Integer.parseInt(e_id));
            if (e != null)
            {
                if (((usertype.equals("dottore") && Integer.parseInt(id) == (pazientiQueries.getPazienteById(e.getPazienteId()).getIdDottore()))|| (usertype.equals("paziente") && Integer.parseInt(id) == e.getPazienteId()) || (usertype.equals("ssp"))))
                {
                    // L'utente loggato è il dottore o il paziente dell'esame o il SSP .
                    paziente p = pazientiQueries.getPazienteById(e.getPazienteId());


                    request.setAttribute("paziente", p);
                    request.setAttribute("esame", e);
                    request.getRequestDispatcher("esame.jsp").include(request,response);
                }
                else
                {
                    // L'utente loggato non è autorizzato ad accedere a queste informazioni.
                    response.sendRedirect("/home");
                    request.getRequestDispatcher("/home").include(request,response);
                }
            }
            else
            {
                response.sendRedirect("/home");
                request.getRequestDispatcher("/home").include(request,response);
            }
        }


    }
}
