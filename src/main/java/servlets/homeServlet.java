package servlets;

import classes.*;
import db.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class homeServlet extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        Cookie[] cookies = request.getCookies();

        String usertype = "";
        String id = "";
        for(int i = 0; i < cookies.length; i++)
        {
            if ((cookies[i].getName().equals("user_type")) && (cookies[i].getValue() != null))
            {
                usertype = cookies[i].getValue();
            }

            if ((cookies[i].getName().equals("id")) && (cookies[i].getValue() != null))
            {
                id = cookies[i].getValue();
            }
        }

        if(usertype.equals("dottore"))
        {
            dottore d = dottoriQueries.getDottoreById(Integer.parseInt(id));
            List<paziente> pazienti = pazientiQueries.getPazientiByDottoreId(Integer.parseInt(id));
            List<visita> visite = visiteQueries.getVisiteByIdDottore(Integer.parseInt(id));

            request.setAttribute("dottore", d);
            request.setAttribute("pazienti", pazienti);
            request.setAttribute("visite", visite);
            request.getRequestDispatcher("homeDottore.jsp").include(request, response);
        }
        else if(usertype.equals("paziente"))
        {
            paziente p = pazientiQueries.getPazienteById(Integer.parseInt(id));

            List<visita> v = visiteQueries.getVisiteByIdPaziente(p.getId());

            List<ricetta> r = ricetteQueries.getRicetteByIdPaziente(p.getId());
            List<esame> e = esamiQueries.getEsamiByIdPaziente(p.getId());
            List<specialistica> s = specialisticheQueries.getSpecialisticheByIdPaziente(p.getId());

            List<ticketEsame> t_e = ticketEsamiQueries.getTicketEsamiByPazienteId(Integer.parseInt(id));
            List<ticketSpecialistica> t_s = ticketSpecialisticheQueries.getTicketSpecialisticheByPazienteId(Integer.parseInt(id));

            request.setAttribute("ticketEsami", t_e);
            request.setAttribute("ticketSpecialistiche", t_s);
            request.setAttribute("paziente", p);
            request.setAttribute("visite", v);
            request.setAttribute("ricette", r);
            request.setAttribute("esami", e);
            request.setAttribute("specialistiche", s);
            request.getRequestDispatcher("homePaziente.jsp").include(request, response);
        }
        else if(usertype.equals("ssp"))
        {
            ssp s = sspQueries.getSSPById(Integer.parseInt(id));
            List<esame> e = esamiQueries.getEsamiByProvincia(s.getProvincia());

            request.setAttribute("ssp", s);
            request.setAttribute("esami", e);
            request.getRequestDispatcher("homeSSP.jsp").include(request, response);
        }
        else
        {
            request.getRequestDispatcher("login.jsp").include(request, response);
        }

    }


}