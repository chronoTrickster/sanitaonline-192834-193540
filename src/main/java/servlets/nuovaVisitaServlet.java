package servlets;

import classes.dottore;
import classes.paziente;
import classes.visita;
import db.pazientiQueries;
import db.visiteQueries;
import db.dottoriQueries;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

public class nuovaVisitaServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();



        Cookie[] cookies = request.getCookies();

        if (cookies == null)
        {
            response.sendRedirect("login.jsp");
        }

        String usertype = "";
        String id = "";
        for(int i = 0; i < cookies.length; i++)
        {
            if ((cookies[i].getName().equals("user_type")) && (cookies[i].getValue() != null))
            {
                usertype = cookies[i].getValue();
            }

            if ((cookies[i].getName().equals("id")) && (cookies[i].getValue() != null))
            {
                id = cookies[i].getValue();
            }
        }

        if(usertype.equals("paziente"))
        {
            paziente p = pazientiQueries.getPazienteById(Integer.parseInt(id));
            dottore d = dottoriQueries.getDottoreById(p.getIdDottore());
            request.setAttribute("dottore", d);
            request.getRequestDispatcher("formVisita.jsp").include(request,response);
        }
        else
        {
            response.sendRedirect("/home");
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();



        Cookie[] cookies = request.getCookies();

        if (cookies == null)
        {
            response.sendRedirect("login.jsp");
        }

        String data = request.getParameter("data");

        String usertype = "";
        String id = "";
        for(int i = 0; i < cookies.length; i++)
        {
            if ((cookies[i].getName().equals("user_type")) && (cookies[i].getValue() != null))
            {
                usertype = cookies[i].getValue();
            }

            if ((cookies[i].getName().equals("id")) && (cookies[i].getValue() != null))
            {
                id = cookies[i].getValue();
            }
        }

        if(usertype.equals("paziente"))
        {
            // Link al profilo paziente
            paziente p = pazientiQueries.getPazienteById(Integer.parseInt(id));
            LocalDateTime dateTime = LocalDateTime.parse(data);
            dateTime = dateTime.minusHours(1);
            visita v = visiteQueries.newVisita(p.getId(), p.getIdDottore(), true, dateTime);
            //request.setAttribute("visita", v);
            response.sendRedirect("visita?id=" + v.getId());
            // request.getRequestDispatcher("visita.jsp").include(request,response);
        }
        else if(usertype.equals("dottore"))
        {
            // etc etc getDottoreById

        }
        else
        {
            request.getRequestDispatcher("login.jsp").include(request, response);
        }

    }
}

