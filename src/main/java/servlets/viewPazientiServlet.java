package servlets;

import classes.paziente;
import db.pazientiQueries;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class viewPazientiServlet extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        try (PrintWriter out = response.getWriter())
        {
            StringBuilder html = new StringBuilder();

            html.append("<h1>Lista pazienti.</h1>");

            List<paziente> pazienti = pazientiQueries.getAllPazienti();
            html.append("<head>");
            html.append("<link rel=\"stylesheet\" href=\"//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css\">");
            html.append("</head>");
            html.append("<table border='5' width='100%'");
            html.append("<tr><th>Id</th><th>Nome</th><th>Cognome</th><th>Sesso</th><th>Luogo di Nascita</th><th>Provincia</th></tr>");

            for(paziente e: pazienti)
            {
                String id = Integer.toString(e.getId());

                html.append("<tr><td>"
                        + id +"</td><td>"
                        + e.getNome() +"</td><td>"
                        + e.getCognome() +"</td><td>"
                        + e.getSesso() +"</td><td>"
                        + e.getLuogoNascita() +"</td><td>"
                        + e.getProvincia() +"</td></tr>"
                );
            }
            html.append("</table>");

            out.print(html);
            out.close();
        }
    }


}
