package servlets;

import classes.*;
import db.*;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class aggiungiEsameServlet extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        String v_id = request.getParameter("id");
        visita v = null;
        String usertype = "";
        String id = "";

        Cookie[] cookies = request.getCookies();



        for(int i = 0; i < cookies.length; i++)
        {
            if ((cookies[i].getName().equals("user_type")) && (cookies[i].getValue() != null))
            {
                usertype = cookies[i].getValue();
            }

            if ((cookies[i].getName().equals("id")) && (cookies[i].getValue() != null))
            {
                id = cookies[i].getValue();
            }

        }

        if (!usertype.equals("dottore"))
        {
            // Accesso negato.
            response.sendRedirect("/home");
            return;
        }

        if (v_id != null && !v_id.trim().isEmpty())
        {
            v = visiteQueries.getVisitaById(Integer.parseInt(v_id));
            if (v != null)
            {
                if(Integer.parseInt(id) == v.getDottoreId())
                {
                    // L'utente loggato è il dottore associato alla visita.
                    request.setAttribute("visita", v);
                    List<tipologiaEsame> esami_disponibili = tipologiaEsamiQueries.getTipologieEsami();
                    request.setAttribute("tipologieEsami", esami_disponibili);
                    request.getRequestDispatcher("aggiungiEsame.jsp").include(request,response);
                }
                else
                {
                    // L'utente loggato non è il dottore del paziente.
                    response.sendRedirect("/home");
                    request.getRequestDispatcher("/home").include(request,response);
                }
            }
            else
            {
                response.sendRedirect("/home");
                request.getRequestDispatcher("/home").include(request,response);
            }
        }
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {


        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        String v_id = request.getParameter("v_id");

        String e_id = request.getParameter("e_id");

        visita v = null;
        String usertype = "";
        String id = "";

        Cookie[] cookies = request.getCookies();


        for(int i = 0; i < cookies.length; i++)
        {
            if ((cookies[i].getName().equals("user_type")) && (cookies[i].getValue() != null))
            {
                usertype = cookies[i].getValue();
            }

            if ((cookies[i].getName().equals("id")) && (cookies[i].getValue() != null))
            {
                id = cookies[i].getValue();
            }

        }

        if (!usertype.equals("dottore"))
        {
            // Accesso negato.
            response.sendRedirect("/home");
            return;
        }



        if (v_id != null && !v_id.trim().isEmpty())
        {
            v = visiteQueries.getVisitaById(Integer.parseInt(v_id));
            if (v != null)
            {
                if(Integer.parseInt(id) == v.getDottoreId())
                {
                    // L'utente loggato è il dottore associato alla visita.
                    int newId = esamiQueries.newEsame(Integer.parseInt(v_id), Integer.parseInt(e_id), true, dottoriQueries.getDottoreById(Integer.parseInt(id)).getProvincia());
                    ticketEsame t_e = ticketEsamiQueries.newTicketEsame(newId, v.getPazienteId(), false);
                    response.sendRedirect("visita?id=" + v.getId());
                }
                else
                {
                    // L'utente loggato non è il dottore del paziente.
                    response.sendRedirect("/home");
                    request.getRequestDispatcher("/home").include(request,response);
                }
            }
            else
            {
                response.sendRedirect("/home");
                request.getRequestDispatcher("/home").include(request,response);
            }
        }

    }
}
