package servlets;

import classes.*;
import db.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class visitaServlet extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        String v_id = request.getParameter("id");
        visita v = null;
        String usertype = "";
        String id = "";

        Cookie[] cookies = request.getCookies();

        for(int i = 0; i < cookies.length; i++)
        {
            if ((cookies[i].getName().equals("user_type")) && (cookies[i].getValue() != null))
            {
                usertype = cookies[i].getValue();
            }

            if ((cookies[i].getName().equals("id")) && (cookies[i].getValue() != null))
            {
                id = cookies[i].getValue();
            }
        }
        if (v_id != null && !v_id.trim().isEmpty())
        {
            v = visiteQueries.getVisitaById(Integer.parseInt(v_id));
            paziente p = pazientiQueries.getPazienteById(v.getPazienteId());
            if (v != null)
            {
                if ((usertype.equals("dottore") && Integer.parseInt(id) == p.getIdDottore()) || (usertype.equals("paziente") && Integer.parseInt(id) == v.getPazienteId()))
                {
                    // L'utente loggato è il dottore o il paziente della visita.

                    dottore d = dottoriQueries.getDottoreById(v.getDottoreId());

                    List<ricetta> r = ricetteQueries.getRicetteByIdVisita(v.getId());

                    List<esame> e = esamiQueries.getEsamiByIdVisita(v.getId());

                    List<specialistica> s = specialisticheQueries.getSpecialisticheByIdVisita(v.getId());

                    request.setAttribute("paziente", p);
                    request.setAttribute("dottore", d);
                    request.setAttribute("visita", v);
                    request.setAttribute("ricette", r);
                    request.setAttribute("esami", e);
                    request.setAttribute("specialistiche", s);
                    request.getRequestDispatcher("visita.jsp").include(request,response);
                }
                else
                {
                    // L'utente loggato non è autorizzato ad accedere a queste informazioni.
                    response.sendRedirect("/home");
                    request.getRequestDispatcher("/home").include(request,response);
                }
            }
            else
            {
                response.sendRedirect("/home");
                request.getRequestDispatcher("/home").include(request,response);
            }
        }



    }


}


