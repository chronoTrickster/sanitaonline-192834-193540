package servlets;

import classes.*;
import db.*;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class specialisticaServlet extends HttpServlet  {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        String s_id = request.getParameter("id");
        specialistica s = null;
        String usertype = "";
        String id = "";

        Cookie[] cookies = request.getCookies();

        for(int i = 0; i < cookies.length; i++)
        {
            if ((cookies[i].getName().equals("user_type")) && (cookies[i].getValue() != null))
            {
                usertype = cookies[i].getValue();
            }

            if ((cookies[i].getName().equals("id")) && (cookies[i].getValue() != null))
            {
                id = cookies[i].getValue();
            }
        }
        if (s_id != null && !s_id.trim().isEmpty())
        {
            s = specialisticheQueries.getSpecialisticaById(Integer.parseInt(s_id));
            if (s != null)
            {
                if (((usertype.equals("dottore") && Integer.parseInt(id) == (pazientiQueries.getPazienteById(s.getPazienteId()).getIdDottore())) || (usertype.equals("paziente") && Integer.parseInt(id) == s.getPazienteId()) || (usertype.equals("ssp"))))
                {
                    // L'utente loggato è il dottore o il paziente dell'esame o il SSP .
                    paziente p = pazientiQueries.getPazienteById(s.getPazienteId());


                    request.setAttribute("paziente", p);
                    request.setAttribute("specialistica", s);
                    request.getRequestDispatcher("specialistica.jsp").include(request,response);
                }
                else
                {
                    // L'utente loggato non è autorizzato ad accedere a queste informazioni.
                    response.sendRedirect("/home");
                    request.getRequestDispatcher("/home").include(request,response);
                }
            }
            else
            {
                response.sendRedirect("/home");
                request.getRequestDispatcher("/home").include(request,response);
            }
        }


    }
}
