package servlets;

import classes.esame;
import classes.paziente;
import db.esamiQueries;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class esamiServlet extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        Cookie[] cookies = request.getCookies();

        String usertype = "";
        String id = "";
        for(int i = 0; i < cookies.length; i++)
        {
            if ((cookies[i].getName().equals("user_type")) && (cookies[i].getValue() != null))
            {
                usertype = cookies[i].getValue();
            }

            if ((cookies[i].getName().equals("id")) && (cookies[i].getValue() != null))
            {
                id = cookies[i].getValue();
            }
        }

        if(usertype.equals("paziente"))
        {
            List<esame> esami = esamiQueries.getEsamiByIdPaziente(Integer.parseInt(id));

            request.setAttribute("esami", esami);
            request.getRequestDispatcher("esami.jsp").include(request, response);
        }
        else
        {
            request.getRequestDispatcher("/home").include(request, response);
        }

    }


}
