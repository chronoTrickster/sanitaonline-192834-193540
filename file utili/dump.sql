PGDMP     +                     x            servizioSanitario    12.1    12.1 x    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    16393    servizioSanitario    DATABASE     �   CREATE DATABASE "servizioSanitario" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Italian_Italy.1252' LC_CTYPE = 'Italian_Italy.1252';
 #   DROP DATABASE "servizioSanitario";
                postgres    false            �            1259    30085    dottori    TABLE       CREATE TABLE public.dottori (
    id integer NOT NULL,
    nome character varying(50) NOT NULL,
    cognome character varying(50) NOT NULL,
    sesso character varying(1) NOT NULL,
    provincia character varying(2) NOT NULL,
    login_id integer NOT NULL
);
    DROP TABLE public.dottori;
       public         heap    postgres    false            �            1259    30083    dottori_id_seq    SEQUENCE     �   CREATE SEQUENCE public.dottori_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.dottori_id_seq;
       public          postgres    false    205            �           0    0    dottori_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.dottori_id_seq OWNED BY public.dottori.id;
          public          postgres    false    204            �            1259    30153    esami    TABLE     �   CREATE TABLE public.esami (
    id integer NOT NULL,
    visita_id integer NOT NULL,
    tipo_esame_id integer NOT NULL,
    attiva boolean NOT NULL,
    provincia character varying(2) NOT NULL
);
    DROP TABLE public.esami;
       public         heap    postgres    false            �            1259    30151    esami_id_seq    SEQUENCE     �   CREATE SEQUENCE public.esami_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.esami_id_seq;
       public          postgres    false    215            �           0    0    esami_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.esami_id_seq OWNED BY public.esami.id;
          public          postgres    false    214            �            1259    30077    login    TABLE     �   CREATE TABLE public.login (
    id integer NOT NULL,
    email character varying(50) NOT NULL,
    password character varying(128) NOT NULL,
    user_type integer NOT NULL
);
    DROP TABLE public.login;
       public         heap    postgres    false            �            1259    30075    login_id_seq    SEQUENCE     �   CREATE SEQUENCE public.login_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.login_id_seq;
       public          postgres    false    203            �           0    0    login_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.login_id_seq OWNED BY public.login.id;
          public          postgres    false    202            �            1259    30098    pazienti    TABLE     �  CREATE TABLE public.pazienti (
    id integer NOT NULL,
    nome character varying(50) NOT NULL,
    cognome character varying(50) NOT NULL,
    luogo_nascita character varying(50) NOT NULL,
    sesso character varying(1) NOT NULL,
    dottore_id integer NOT NULL,
    provincia character varying(2) NOT NULL,
    login_id integer NOT NULL,
    codice_fiscale character varying(16) NOT NULL,
    data_di_nascita timestamp without time zone NOT NULL,
    propic character varying(2000) NOT NULL
);
    DROP TABLE public.pazienti;
       public         heap    postgres    false            �            1259    30096    pazienti_id_seq    SEQUENCE     �   CREATE SEQUENCE public.pazienti_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.pazienti_id_seq;
       public          postgres    false    207            �           0    0    pazienti_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.pazienti_id_seq OWNED BY public.pazienti.id;
          public          postgres    false    206            �            1259    30194    ricette    TABLE     �   CREATE TABLE public.ricette (
    id integer NOT NULL,
    visita_id integer NOT NULL,
    attiva boolean NOT NULL,
    medicinali character varying(2000) NOT NULL,
    "timestamp" timestamp without time zone NOT NULL
);
    DROP TABLE public.ricette;
       public         heap    postgres    false            �            1259    30192    ricette_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ricette_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.ricette_id_seq;
       public          postgres    false    219            �           0    0    ricette_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.ricette_id_seq OWNED BY public.ricette.id;
          public          postgres    false    218            �            1259    30210    risultati_esami    TABLE     �   CREATE TABLE public.risultati_esami (
    id integer NOT NULL,
    esame_id integer NOT NULL,
    anamnesi character varying(2048) NOT NULL,
    "timestamp" timestamp without time zone NOT NULL
);
 #   DROP TABLE public.risultati_esami;
       public         heap    postgres    false            �            1259    30208    risultati_esami_id_seq    SEQUENCE     �   CREATE SEQUENCE public.risultati_esami_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.risultati_esami_id_seq;
       public          postgres    false    221            �           0    0    risultati_esami_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.risultati_esami_id_seq OWNED BY public.risultati_esami.id;
          public          postgres    false    220            �            1259    30226    risultati_specialistiche    TABLE     �   CREATE TABLE public.risultati_specialistiche (
    id integer NOT NULL,
    specialistica_id integer NOT NULL,
    anamnesi character varying(2048) NOT NULL,
    "timestamp" timestamp without time zone NOT NULL
);
 ,   DROP TABLE public.risultati_specialistiche;
       public         heap    postgres    false            �            1259    30224    risultati_specialistiche_id_seq    SEQUENCE     �   CREATE SEQUENCE public.risultati_specialistiche_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 6   DROP SEQUENCE public.risultati_specialistiche_id_seq;
       public          postgres    false    223            �           0    0    risultati_specialistiche_id_seq    SEQUENCE OWNED BY     c   ALTER SEQUENCE public.risultati_specialistiche_id_seq OWNED BY public.risultati_specialistiche.id;
          public          postgres    false    222            �            1259    30171    specialistiche    TABLE     �   CREATE TABLE public.specialistiche (
    id integer NOT NULL,
    visita_id integer NOT NULL,
    tipo_specialistica_id integer NOT NULL,
    dottore_id integer NOT NULL,
    attiva boolean NOT NULL,
    provincia character varying(2) NOT NULL
);
 "   DROP TABLE public.specialistiche;
       public         heap    postgres    false            �            1259    30169    specialistiche_id_seq    SEQUENCE     �   CREATE SEQUENCE public.specialistiche_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.specialistiche_id_seq;
       public          postgres    false    217            �           0    0    specialistiche_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.specialistiche_id_seq OWNED BY public.specialistiche.id;
          public          postgres    false    216            �            1259    30278    ssp    TABLE     �   CREATE TABLE public.ssp (
    id integer NOT NULL,
    provincia character varying(2) NOT NULL,
    login_id integer NOT NULL
);
    DROP TABLE public.ssp;
       public         heap    postgres    false            �            1259    30276 
   ssp_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ssp_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 !   DROP SEQUENCE public.ssp_id_seq;
       public          postgres    false    229            �           0    0 
   ssp_id_seq    SEQUENCE OWNED BY     9   ALTER SEQUENCE public.ssp_id_seq OWNED BY public.ssp.id;
          public          postgres    false    228            �            1259    30242    ticket_esami    TABLE     �   CREATE TABLE public.ticket_esami (
    id integer NOT NULL,
    esame_id integer NOT NULL,
    paziente_id integer NOT NULL,
    pagato boolean NOT NULL,
    data_pagamento timestamp without time zone
);
     DROP TABLE public.ticket_esami;
       public         heap    postgres    false            �            1259    30240    ticket_esami_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ticket_esami_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.ticket_esami_id_seq;
       public          postgres    false    225            �           0    0    ticket_esami_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.ticket_esami_id_seq OWNED BY public.ticket_esami.id;
          public          postgres    false    224            �            1259    30260    ticket_specialistiche    TABLE     �   CREATE TABLE public.ticket_specialistiche (
    id integer NOT NULL,
    specialistica_id integer NOT NULL,
    paziente_id integer NOT NULL,
    pagato boolean NOT NULL,
    data_pagamento timestamp without time zone
);
 )   DROP TABLE public.ticket_specialistiche;
       public         heap    postgres    false            �            1259    30258    ticket_specialistiche_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ticket_specialistiche_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.ticket_specialistiche_id_seq;
       public          postgres    false    227            �           0    0    ticket_specialistiche_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.ticket_specialistiche_id_seq OWNED BY public.ticket_specialistiche.id;
          public          postgres    false    226            �            1259    30137    tipologia_esami    TABLE     j   CREATE TABLE public.tipologia_esami (
    id integer NOT NULL,
    nome character varying(50) NOT NULL
);
 #   DROP TABLE public.tipologia_esami;
       public         heap    postgres    false            �            1259    30135    tipologia_esami_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tipologia_esami_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.tipologia_esami_id_seq;
       public          postgres    false    211            �           0    0    tipologia_esami_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.tipologia_esami_id_seq OWNED BY public.tipologia_esami.id;
          public          postgres    false    210            �            1259    30145    tipologia_specialistiche    TABLE     s   CREATE TABLE public.tipologia_specialistiche (
    id integer NOT NULL,
    nome character varying(50) NOT NULL
);
 ,   DROP TABLE public.tipologia_specialistiche;
       public         heap    postgres    false            �            1259    30143    tipologia_specialistiche_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tipologia_specialistiche_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 6   DROP SEQUENCE public.tipologia_specialistiche_id_seq;
       public          postgres    false    213            �           0    0    tipologia_specialistiche_id_seq    SEQUENCE OWNED BY     c   ALTER SEQUENCE public.tipologia_specialistiche_id_seq OWNED BY public.tipologia_specialistiche.id;
          public          postgres    false    212            �            1259    30119    visite    TABLE     �   CREATE TABLE public.visite (
    id integer NOT NULL,
    paziente_id integer NOT NULL,
    dottore_id integer NOT NULL,
    attiva boolean NOT NULL,
    "timestamp" timestamp without time zone NOT NULL
);
    DROP TABLE public.visite;
       public         heap    postgres    false            �            1259    30117    visite_id_seq    SEQUENCE     �   CREATE SEQUENCE public.visite_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.visite_id_seq;
       public          postgres    false    209            �           0    0    visite_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.visite_id_seq OWNED BY public.visite.id;
          public          postgres    false    208            �
           2604    30088 
   dottori id    DEFAULT     h   ALTER TABLE ONLY public.dottori ALTER COLUMN id SET DEFAULT nextval('public.dottori_id_seq'::regclass);
 9   ALTER TABLE public.dottori ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    204    205    205            �
           2604    30156    esami id    DEFAULT     d   ALTER TABLE ONLY public.esami ALTER COLUMN id SET DEFAULT nextval('public.esami_id_seq'::regclass);
 7   ALTER TABLE public.esami ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    214    215    215            �
           2604    30080    login id    DEFAULT     d   ALTER TABLE ONLY public.login ALTER COLUMN id SET DEFAULT nextval('public.login_id_seq'::regclass);
 7   ALTER TABLE public.login ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    203    202    203            �
           2604    30101    pazienti id    DEFAULT     j   ALTER TABLE ONLY public.pazienti ALTER COLUMN id SET DEFAULT nextval('public.pazienti_id_seq'::regclass);
 :   ALTER TABLE public.pazienti ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    206    207    207            �
           2604    30197 
   ricette id    DEFAULT     h   ALTER TABLE ONLY public.ricette ALTER COLUMN id SET DEFAULT nextval('public.ricette_id_seq'::regclass);
 9   ALTER TABLE public.ricette ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    218    219    219            �
           2604    30213    risultati_esami id    DEFAULT     x   ALTER TABLE ONLY public.risultati_esami ALTER COLUMN id SET DEFAULT nextval('public.risultati_esami_id_seq'::regclass);
 A   ALTER TABLE public.risultati_esami ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    221    220    221            �
           2604    30229    risultati_specialistiche id    DEFAULT     �   ALTER TABLE ONLY public.risultati_specialistiche ALTER COLUMN id SET DEFAULT nextval('public.risultati_specialistiche_id_seq'::regclass);
 J   ALTER TABLE public.risultati_specialistiche ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    222    223    223            �
           2604    30174    specialistiche id    DEFAULT     v   ALTER TABLE ONLY public.specialistiche ALTER COLUMN id SET DEFAULT nextval('public.specialistiche_id_seq'::regclass);
 @   ALTER TABLE public.specialistiche ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    217    216    217            �
           2604    30281    ssp id    DEFAULT     `   ALTER TABLE ONLY public.ssp ALTER COLUMN id SET DEFAULT nextval('public.ssp_id_seq'::regclass);
 5   ALTER TABLE public.ssp ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    228    229    229            �
           2604    30245    ticket_esami id    DEFAULT     r   ALTER TABLE ONLY public.ticket_esami ALTER COLUMN id SET DEFAULT nextval('public.ticket_esami_id_seq'::regclass);
 >   ALTER TABLE public.ticket_esami ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    225    224    225            �
           2604    30263    ticket_specialistiche id    DEFAULT     �   ALTER TABLE ONLY public.ticket_specialistiche ALTER COLUMN id SET DEFAULT nextval('public.ticket_specialistiche_id_seq'::regclass);
 G   ALTER TABLE public.ticket_specialistiche ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    227    226    227            �
           2604    30140    tipologia_esami id    DEFAULT     x   ALTER TABLE ONLY public.tipologia_esami ALTER COLUMN id SET DEFAULT nextval('public.tipologia_esami_id_seq'::regclass);
 A   ALTER TABLE public.tipologia_esami ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    211    210    211            �
           2604    30148    tipologia_specialistiche id    DEFAULT     �   ALTER TABLE ONLY public.tipologia_specialistiche ALTER COLUMN id SET DEFAULT nextval('public.tipologia_specialistiche_id_seq'::regclass);
 J   ALTER TABLE public.tipologia_specialistiche ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    213    212    213            �
           2604    30122 	   visite id    DEFAULT     f   ALTER TABLE ONLY public.visite ALTER COLUMN id SET DEFAULT nextval('public.visite_id_seq'::regclass);
 8   ALTER TABLE public.visite ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    209    208    209            �          0    30085    dottori 
   TABLE DATA           P   COPY public.dottori (id, nome, cognome, sesso, provincia, login_id) FROM stdin;
    public          postgres    false    205   P�       �          0    30153    esami 
   TABLE DATA           P   COPY public.esami (id, visita_id, tipo_esame_id, attiva, provincia) FROM stdin;
    public          postgres    false    215   ّ       �          0    30077    login 
   TABLE DATA           ?   COPY public.login (id, email, password, user_type) FROM stdin;
    public          postgres    false    203   ��       �          0    30098    pazienti 
   TABLE DATA           �   COPY public.pazienti (id, nome, cognome, luogo_nascita, sesso, dottore_id, provincia, login_id, codice_fiscale, data_di_nascita, propic) FROM stdin;
    public          postgres    false    207   ē       �          0    30194    ricette 
   TABLE DATA           Q   COPY public.ricette (id, visita_id, attiva, medicinali, "timestamp") FROM stdin;
    public          postgres    false    219   �       �          0    30210    risultati_esami 
   TABLE DATA           N   COPY public.risultati_esami (id, esame_id, anamnesi, "timestamp") FROM stdin;
    public          postgres    false    221   ;�       �          0    30226    risultati_specialistiche 
   TABLE DATA           _   COPY public.risultati_specialistiche (id, specialistica_id, anamnesi, "timestamp") FROM stdin;
    public          postgres    false    223   X�       �          0    30171    specialistiche 
   TABLE DATA           m   COPY public.specialistiche (id, visita_id, tipo_specialistica_id, dottore_id, attiva, provincia) FROM stdin;
    public          postgres    false    217   u�       �          0    30278    ssp 
   TABLE DATA           6   COPY public.ssp (id, provincia, login_id) FROM stdin;
    public          postgres    false    229   ��       �          0    30242    ticket_esami 
   TABLE DATA           Y   COPY public.ticket_esami (id, esame_id, paziente_id, pagato, data_pagamento) FROM stdin;
    public          postgres    false    225   ��       �          0    30260    ticket_specialistiche 
   TABLE DATA           j   COPY public.ticket_specialistiche (id, specialistica_id, paziente_id, pagato, data_pagamento) FROM stdin;
    public          postgres    false    227   ܜ       �          0    30137    tipologia_esami 
   TABLE DATA           3   COPY public.tipologia_esami (id, nome) FROM stdin;
    public          postgres    false    211   ��       �          0    30145    tipologia_specialistiche 
   TABLE DATA           <   COPY public.tipologia_specialistiche (id, nome) FROM stdin;
    public          postgres    false    213   �       �          0    30119    visite 
   TABLE DATA           R   COPY public.visite (id, paziente_id, dottore_id, attiva, "timestamp") FROM stdin;
    public          postgres    false    209   ̞       �           0    0    dottori_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.dottori_id_seq', 5, true);
          public          postgres    false    204            �           0    0    esami_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.esami_id_seq', 1, false);
          public          postgres    false    214            �           0    0    login_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.login_id_seq', 57, true);
          public          postgres    false    202            �           0    0    pazienti_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.pazienti_id_seq', 50, true);
          public          postgres    false    206            �           0    0    ricette_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.ricette_id_seq', 1, false);
          public          postgres    false    218            �           0    0    risultati_esami_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.risultati_esami_id_seq', 1, false);
          public          postgres    false    220            �           0    0    risultati_specialistiche_id_seq    SEQUENCE SET     N   SELECT pg_catalog.setval('public.risultati_specialistiche_id_seq', 1, false);
          public          postgres    false    222            �           0    0    specialistiche_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.specialistiche_id_seq', 1, false);
          public          postgres    false    216            �           0    0 
   ssp_id_seq    SEQUENCE SET     8   SELECT pg_catalog.setval('public.ssp_id_seq', 2, true);
          public          postgres    false    228            �           0    0    ticket_esami_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.ticket_esami_id_seq', 1, false);
          public          postgres    false    224            �           0    0    ticket_specialistiche_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.ticket_specialistiche_id_seq', 1, false);
          public          postgres    false    226            �           0    0    tipologia_esami_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.tipologia_esami_id_seq', 20, true);
          public          postgres    false    210            �           0    0    tipologia_specialistiche_id_seq    SEQUENCE SET     N   SELECT pg_catalog.setval('public.tipologia_specialistiche_id_seq', 20, true);
          public          postgres    false    212            �           0    0    visite_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.visite_id_seq', 1, false);
          public          postgres    false    208            �
           2606    30090    dottori dottori_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.dottori
    ADD CONSTRAINT dottori_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.dottori DROP CONSTRAINT dottori_pkey;
       public            postgres    false    205            �
           2606    30158    esami esami_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.esami
    ADD CONSTRAINT esami_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.esami DROP CONSTRAINT esami_pkey;
       public            postgres    false    215            �
           2606    30082    login login_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.login
    ADD CONSTRAINT login_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.login DROP CONSTRAINT login_pkey;
       public            postgres    false    203            �
           2606    30106    pazienti pazienti_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.pazienti
    ADD CONSTRAINT pazienti_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.pazienti DROP CONSTRAINT pazienti_pkey;
       public            postgres    false    207            �
           2606    30202    ricette ricette_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.ricette
    ADD CONSTRAINT ricette_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.ricette DROP CONSTRAINT ricette_pkey;
       public            postgres    false    219            �
           2606    30218 $   risultati_esami risultati_esami_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.risultati_esami
    ADD CONSTRAINT risultati_esami_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.risultati_esami DROP CONSTRAINT risultati_esami_pkey;
       public            postgres    false    221            �
           2606    30234 6   risultati_specialistiche risultati_specialistiche_pkey 
   CONSTRAINT     t   ALTER TABLE ONLY public.risultati_specialistiche
    ADD CONSTRAINT risultati_specialistiche_pkey PRIMARY KEY (id);
 `   ALTER TABLE ONLY public.risultati_specialistiche DROP CONSTRAINT risultati_specialistiche_pkey;
       public            postgres    false    223            �
           2606    30176 "   specialistiche specialistiche_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.specialistiche
    ADD CONSTRAINT specialistiche_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.specialistiche DROP CONSTRAINT specialistiche_pkey;
       public            postgres    false    217            �
           2606    30283    ssp ssp_pkey 
   CONSTRAINT     J   ALTER TABLE ONLY public.ssp
    ADD CONSTRAINT ssp_pkey PRIMARY KEY (id);
 6   ALTER TABLE ONLY public.ssp DROP CONSTRAINT ssp_pkey;
       public            postgres    false    229            �
           2606    30247    ticket_esami ticket_esami_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.ticket_esami
    ADD CONSTRAINT ticket_esami_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.ticket_esami DROP CONSTRAINT ticket_esami_pkey;
       public            postgres    false    225            �
           2606    30265 0   ticket_specialistiche ticket_specialistiche_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public.ticket_specialistiche
    ADD CONSTRAINT ticket_specialistiche_pkey PRIMARY KEY (id);
 Z   ALTER TABLE ONLY public.ticket_specialistiche DROP CONSTRAINT ticket_specialistiche_pkey;
       public            postgres    false    227            �
           2606    30142 $   tipologia_esami tipologia_esami_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.tipologia_esami
    ADD CONSTRAINT tipologia_esami_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.tipologia_esami DROP CONSTRAINT tipologia_esami_pkey;
       public            postgres    false    211            �
           2606    30150 6   tipologia_specialistiche tipologia_specialistiche_pkey 
   CONSTRAINT     t   ALTER TABLE ONLY public.tipologia_specialistiche
    ADD CONSTRAINT tipologia_specialistiche_pkey PRIMARY KEY (id);
 `   ALTER TABLE ONLY public.tipologia_specialistiche DROP CONSTRAINT tipologia_specialistiche_pkey;
       public            postgres    false    213            �
           2606    30124    visite visite_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.visite
    ADD CONSTRAINT visite_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.visite DROP CONSTRAINT visite_pkey;
       public            postgres    false    209            �
           2606    30091    dottori dottori_login_id_fkey    FK CONSTRAINT     }   ALTER TABLE ONLY public.dottori
    ADD CONSTRAINT dottori_login_id_fkey FOREIGN KEY (login_id) REFERENCES public.login(id);
 G   ALTER TABLE ONLY public.dottori DROP CONSTRAINT dottori_login_id_fkey;
       public          postgres    false    205    203    2784                       2606    30164    esami esami_tipo_esame_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.esami
    ADD CONSTRAINT esami_tipo_esame_id_fkey FOREIGN KEY (tipo_esame_id) REFERENCES public.tipologia_esami(id);
 H   ALTER TABLE ONLY public.esami DROP CONSTRAINT esami_tipo_esame_id_fkey;
       public          postgres    false    2792    211    215                        2606    30159    esami esami_visita_id_fkey    FK CONSTRAINT     |   ALTER TABLE ONLY public.esami
    ADD CONSTRAINT esami_visita_id_fkey FOREIGN KEY (visita_id) REFERENCES public.visite(id);
 D   ALTER TABLE ONLY public.esami DROP CONSTRAINT esami_visita_id_fkey;
       public          postgres    false    209    215    2790            �
           2606    30112 !   pazienti pazienti_dottore_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.pazienti
    ADD CONSTRAINT pazienti_dottore_id_fkey FOREIGN KEY (dottore_id) REFERENCES public.dottori(id);
 K   ALTER TABLE ONLY public.pazienti DROP CONSTRAINT pazienti_dottore_id_fkey;
       public          postgres    false    205    2786    207            �
           2606    30107    pazienti pazienti_login_id_fkey    FK CONSTRAINT        ALTER TABLE ONLY public.pazienti
    ADD CONSTRAINT pazienti_login_id_fkey FOREIGN KEY (login_id) REFERENCES public.login(id);
 I   ALTER TABLE ONLY public.pazienti DROP CONSTRAINT pazienti_login_id_fkey;
       public          postgres    false    203    207    2784                       2606    30203    ricette ricette_visita_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ricette
    ADD CONSTRAINT ricette_visita_id_fkey FOREIGN KEY (visita_id) REFERENCES public.visite(id);
 H   ALTER TABLE ONLY public.ricette DROP CONSTRAINT ricette_visita_id_fkey;
       public          postgres    false    209    219    2790                       2606    30219 -   risultati_esami risultati_esami_esame_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.risultati_esami
    ADD CONSTRAINT risultati_esami_esame_id_fkey FOREIGN KEY (esame_id) REFERENCES public.esami(id);
 W   ALTER TABLE ONLY public.risultati_esami DROP CONSTRAINT risultati_esami_esame_id_fkey;
       public          postgres    false    215    2796    221                       2606    30235 G   risultati_specialistiche risultati_specialistiche_specialistica_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.risultati_specialistiche
    ADD CONSTRAINT risultati_specialistiche_specialistica_id_fkey FOREIGN KEY (specialistica_id) REFERENCES public.specialistiche(id);
 q   ALTER TABLE ONLY public.risultati_specialistiche DROP CONSTRAINT risultati_specialistiche_specialistica_id_fkey;
       public          postgres    false    223    217    2798                       2606    30187 -   specialistiche specialistiche_dottore_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.specialistiche
    ADD CONSTRAINT specialistiche_dottore_id_fkey FOREIGN KEY (dottore_id) REFERENCES public.dottori(id);
 W   ALTER TABLE ONLY public.specialistiche DROP CONSTRAINT specialistiche_dottore_id_fkey;
       public          postgres    false    217    2786    205                       2606    30182 8   specialistiche specialistiche_tipo_specialistica_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.specialistiche
    ADD CONSTRAINT specialistiche_tipo_specialistica_id_fkey FOREIGN KEY (tipo_specialistica_id) REFERENCES public.tipologia_specialistiche(id);
 b   ALTER TABLE ONLY public.specialistiche DROP CONSTRAINT specialistiche_tipo_specialistica_id_fkey;
       public          postgres    false    2794    217    213                       2606    30177 ,   specialistiche specialistiche_visita_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.specialistiche
    ADD CONSTRAINT specialistiche_visita_id_fkey FOREIGN KEY (visita_id) REFERENCES public.visite(id);
 V   ALTER TABLE ONLY public.specialistiche DROP CONSTRAINT specialistiche_visita_id_fkey;
       public          postgres    false    217    209    2790                       2606    30284    ssp ssp_login_id_fkey    FK CONSTRAINT     u   ALTER TABLE ONLY public.ssp
    ADD CONSTRAINT ssp_login_id_fkey FOREIGN KEY (login_id) REFERENCES public.login(id);
 ?   ALTER TABLE ONLY public.ssp DROP CONSTRAINT ssp_login_id_fkey;
       public          postgres    false    203    229    2784                       2606    30248 '   ticket_esami ticket_esami_esame_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ticket_esami
    ADD CONSTRAINT ticket_esami_esame_id_fkey FOREIGN KEY (esame_id) REFERENCES public.esami(id);
 Q   ALTER TABLE ONLY public.ticket_esami DROP CONSTRAINT ticket_esami_esame_id_fkey;
       public          postgres    false    215    2796    225            	           2606    30253 *   ticket_esami ticket_esami_paziente_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ticket_esami
    ADD CONSTRAINT ticket_esami_paziente_id_fkey FOREIGN KEY (paziente_id) REFERENCES public.pazienti(id);
 T   ALTER TABLE ONLY public.ticket_esami DROP CONSTRAINT ticket_esami_paziente_id_fkey;
       public          postgres    false    207    225    2788                       2606    30271 <   ticket_specialistiche ticket_specialistiche_paziente_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ticket_specialistiche
    ADD CONSTRAINT ticket_specialistiche_paziente_id_fkey FOREIGN KEY (paziente_id) REFERENCES public.pazienti(id);
 f   ALTER TABLE ONLY public.ticket_specialistiche DROP CONSTRAINT ticket_specialistiche_paziente_id_fkey;
       public          postgres    false    227    207    2788            
           2606    30266 A   ticket_specialistiche ticket_specialistiche_specialistica_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.ticket_specialistiche
    ADD CONSTRAINT ticket_specialistiche_specialistica_id_fkey FOREIGN KEY (specialistica_id) REFERENCES public.specialistiche(id);
 k   ALTER TABLE ONLY public.ticket_specialistiche DROP CONSTRAINT ticket_specialistiche_specialistica_id_fkey;
       public          postgres    false    217    227    2798            �
           2606    30130    visite visite_dottore_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.visite
    ADD CONSTRAINT visite_dottore_id_fkey FOREIGN KEY (dottore_id) REFERENCES public.dottori(id);
 G   ALTER TABLE ONLY public.visite DROP CONSTRAINT visite_dottore_id_fkey;
       public          postgres    false    205    2786    209            �
           2606    30125    visite visite_paziente_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.visite
    ADD CONSTRAINT visite_paziente_id_fkey FOREIGN KEY (paziente_id) REFERENCES public.pazienti(id);
 H   ALTER TABLE ONLY public.visite DROP CONSTRAINT visite_paziente_id_fkey;
       public          postgres    false    209    207    2788            �   y   x�5�M
�0��7���h%�E�B�n�6��8i������kp�,��+��*�1ajw}W����/-u�.�I�!&�?���̲aÕ�pN�\�mF�) f�%mc�ms�Z��~ �w�'�      �      x������ � �      �   �  x�}��n�0���a*��߮2� �FJroG��Ǵ�H��d������Q�V�~���X���\ΟgZ�m4��9���1���\�.�0B'@��!���J��;�%�a�Q���O4{��V����ť3�
�i	����g�q�,�.z�h=c�	�詚�@�cG��8-�4'�ܜ�[B�������V��wDk��M��,qF,I?�����TS�I���o(<*����C��D��[b<B���� F	!����lOc�UK�_>�q瀃t$�9.\O��干n�����f��3/��f�tΜ�:���6x�UB�9�Z��Bm���b6��7����Qv�ǫ����!>�Cm�=�#��d��2�ӧ�W�x�
R�"�!�d���X�Rd�~Ȫ���<�� ��--I~#���і�G���H/N����]7�����_oM������      �   J  x��X]o�|����|�_"��Y���F��J�t�EI�^������ȯ�T��$0��!=���aY�4]C����u�2�];6�[Cv��l�?E�oҵf��J�{�B�VT����ҿ��\������u^������?q����_Ɓ�~�\Z�ׯCג��sr�Ӵ�Ux�jgD�'�]�pE�,<w��÷5I�0�z�I�vu�_d��f�/C�J}�E�>��g^8xI֣��jO�m߂�����$Uv(ҍ���(�5ǵW\�BK ����@�ooC�X^���_ڳeFX��<>�E�S1��J}#�R\�����"Q���z/� ����ȡ*��M��J���bV����W\�W?,�c�ק��	�������"��܄W	td����o�v'�p��s�u|����bH���V�f[���V����57$Q[��5yhF42m�Cv���Ȣ<��Tr�x����>�I���kR6csz���#��-���iČ�</�,qL�S���l�1bEA��`ԟ�@��f-�����\1�7�2�i��o%O�`�=�`z��e��]VFP5������GŮ��tkT�0�0�V.��y�2|����:5�5q�=��c�u�e��En�(`����=&p
X`ʫ�I�tӑ��y���u�����[�B��aA:��y�2�^��УI}Bk��=��0��}�P�d�&������!��.���e
����CH>����>B��i�)��`Lc0�Ջ���z�2M�G8><�>8d}���gG�1I�U�ґ�:�7��-��.3d;>��,��W��_e�̵�*Ӏo�~����ʘ0�Ϊ�Sr�=��!�y�elBr(�]����$��z��)�5�};�I3��[rh���{oQ��.�C�)��L|��s�l��̽�9#������:���
�wqY��بL��1��{���ǞO�m^��H��j��y��Ҡ!Sn����]�@yʬ3�dr�b���˅�e���$C_mp�.{��u�WI��2�6�py.���p�Y.!֡o_�^�?����0��%Yr��e2l�����i;��^�<�7>U���-�uo㉂**�������2��\J]��I�=$F}sn?��O�"q���ch
.v�L�K���{�r��j�gl���k������^��W����9�=��Ԃcr/^n,x�P��w�����IX�����f�2E��
�z3���C;/���ŭ��+H����fʨ8�U�+��Y�W����"V,������_����8��%����	/�&`�����?@x�
\������}}C�te�/ˬ�C��,�B9_P6��<���V +l�C}t���d��Gl!����<H�a�d�bi�Ŵ*��/�-���n'�x�.����ZɊ��5O%mp��S�~��>�@�c��V�?�)���<�K���_��`�/�]�!�6z1�����$�Kb�.�f�3��^��q���{X.}�;슷���U~(��<�SYpi&��Ax�
mS�Zgt=�n[�ݞH����:�х`15n�5�',,���Xt��y�OD�9$[y�m5Q�1��B�-�|s[�b^x-���ԌX�#`�h���^����*�*�*�au[[�B����-)y���	�����,�o�p�Cq7���3��r��'$Z<����Ӿ�*�G�J�y������=}�RzAK��pF��M"���5�˳K�E�c�@]�P`!R,U4�\�S����NH�k)�)XBI����	��Q�ܕEi�@�VK�O�jɖ������P_;����nzC�!�����M|z����F'��e ڟܖR��m6�ғ]� +�2?*�Z9�P.���;��YbUnO�)�_j��н��@M�W�E�*��'8-,͑״Ԥ�Ba�f<�)�����'�ƣ2���ә�Km���i��J�Ɔ極o����������b%Q^>`����	�r2$���H/d�7o�7��O^K�V�'�v��	��I�̖��`�i�Z\��p@������02mO��t�����!��&
�u�9[2�����������3      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x�3����45�2���45����� 'dT      �      x������ � �      �      x������ � �      �   �   x�M�Kn�0D��)|�"r���Z����]�hHJ9M�ҋ�6� ;�0��C�D�����Mi&l^�\�4�V��q�0}�4	Gϭ�
�;`G�!V�����m�4����雋f�G�NR�YH�߯�8#�$��=��e������T$kP�p��f�Wp	\JR��-���{3���>��7�¦t��0[m�5�X;����$��Jb�G���ޭJ�xê�.���!�������'��?^��      �   �   x�M��� ���a����h4�mL���&Ț-���X��7����8g�c�iUT�x#�fI���Ub���$�Ϳ���;R7F/��B*�+H���5��������c�FGG}H�
�a��T�����:�F#�_����^�!��47d���g�X�e���)]�կq�ߩ��D�G^'EQ���j      �      x������ � �     