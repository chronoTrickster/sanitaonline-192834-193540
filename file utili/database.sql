-- DROP TABLES -----------------------------------------------------------------

DROP TABLE "risultati_esami";
DROP TABLE "risultati_specialistiche";
DROP TABLE "ticket_esami";
DROP TABLE "ticket_specialistiche";
DROP TABLE "esami";
DROP TABLE "tipologia_esami";
DROP TABLE "specialistiche";
DROP TABLE "tipologia_specialistiche";
DROP TABLE "ricette";
DROP TABLE "visite";
DROP TABLE "pazienti";
DROP TABLE "dottori";
DROP TABLE "ssp";
DROP TABLE "login";

-- CREATE TABLES ---------------------------------------------------------------

CREATE TABLE login (
    id SERIAL PRIMARY KEY,
    email VARCHAR (50) NOT NULL,
    password VARCHAR (128) NOT NULL,
    user_type INT NOT NULL
);

CREATE TABLE dottori (
    id SERIAL PRIMARY KEY,
    nome VARCHAR (50) NOT NULL,
    cognome VARCHAR (50) NOT NULL,
    sesso VARCHAR (1) NOT NULL,
    provincia VARCHAR (2) NOT NULL,
    login_id INT NOT NULL,
    FOREIGN KEY (login_id) REFERENCES login (id)
);

CREATE TABLE pazienti (
    id SERIAL PRIMARY KEY,
    nome VARCHAR (50) NOT NULL,
    cognome VARCHAR (50) NOT NULL,
    luogo_nascita VARCHAR (50) NOT NULL,
    sesso VARCHAR (1) NOT NULL,
    dottore_id INT NOT NULL,
    provincia VARCHAR (2) NOT NULL,
    login_id INT NOT NULL,
    codice_fiscale VARCHAR (16) NOT NULL,
    data_di_nascita TIMESTAMP NOT NULL,
    propic VARCHAR (2000) NOT NULL,
    FOREIGN KEY (login_id) REFERENCES login (id),
    FOREIGN KEY (dottore_id) REFERENCES dottori (id)
);

CREATE TABLE visite (
    id SERIAL PRIMARY KEY,
    paziente_id INT NOT NULL,
    dottore_id INT NOT NULL,
    attiva BOOL NOT NULL,
    timestamp TIMESTAMP NOT NULL,
    FOREIGN KEY (paziente_id) REFERENCES pazienti (id),
    FOREIGN KEY (dottore_id) REFERENCES dottori (id)
);

CREATE TABLE tipologia_esami (
    id SERIAL PRIMARY KEY,
    nome VARCHAR (50) NOT NULL
);

CREATE TABLE tipologia_specialistiche (
    id SERIAL PRIMARY KEY,
    nome VARCHAR (50) NOT NULL
);

CREATE TABLE esami (
    id SERIAL PRIMARY KEY,
    visita_id INT NOT NULL,
    tipo_esame_id INT NOT NULL,
    attiva BOOL NOT NULL,
    provincia VARCHAR (2) NOT NULL,
    FOREIGN KEY (visita_id) REFERENCES visite (id),
    FOREIGN KEY (tipo_esame_id) REFERENCES tipologia_esami(id)
);

CREATE TABLE specialistiche (
    id SERIAL PRIMARY KEY,
    visita_id INT NOT NULL,
    tipo_specialistica_id INT NOT NULL,
    dottore_id INT NOT NULL,
    attiva BOOL NOT NULL,
    provincia VARCHAR (2) NOT NULL,
    FOREIGN KEY (visita_id) REFERENCES visite (id),
    FOREIGN KEY (tipo_specialistica_id) REFERENCES tipologia_specialistiche (id),
    FOREIGN KEY (dottore_id) REFERENCES dottori (id)
);

CREATE TABLE ricette (
    id SERIAL PRIMARY KEY,
    visita_id INT NOT NULL,
    attiva BOOL NOT NULL,
    medicinali VARCHAR(2000) NOT NULL,
    timestamp TIMESTAMP NOT NULL,
    FOREIGN KEY (visita_id) REFERENCES visite (id)
);

CREATE TABLE risultati_esami (
    id SERIAL PRIMARY KEY,
    esame_id INT NOT NULL,
    anamnesi VARCHAR (2048) NOT NULL,
    timestamp TIMESTAMP NOT NULL,
    FOREIGN KEY (esame_id) REFERENCES esami (id)
);

CREATE TABLE risultati_specialistiche (
    id SERIAL PRIMARY KEY,
    specialistica_id INT NOT NULL,
    anamnesi VARCHAR (2048) NOT NULL,
    timestamp TIMESTAMP NOT NULL,
    FOREIGN KEY (specialistica_id) REFERENCES specialistiche (id)
);

CREATE TABLE ticket_esami (
    id SERIAL PRIMARY KEY,
    esame_id INT NOT NULL,
    paziente_id INT NOT NULL,
    pagato BOOL NOT NULL,
    data_pagamento TIMESTAMP,
    FOREIGN KEY (esame_id) REFERENCES esami (id),
    FOREIGN KEY (paziente_id) REFERENCES pazienti (id)
);

CREATE TABLE ticket_specialistiche (
    id SERIAL PRIMARY KEY,
    specialistica_id INT NOT NULL,
    paziente_id INT NOT NULL,
    pagato BOOL NOT NULL,
    data_pagamento TIMESTAMP,
    FOREIGN KEY (specialistica_id) REFERENCES specialistiche (id),
    FOREIGN KEY (paziente_id) REFERENCES pazienti (id)
);

CREATE TABLE ssp (
    id SERIAL PRIMARY KEY,
    provincia VARCHAR(2) NOT NULL,
    login_id INT NOT NULL,
    FOREIGN KEY (login_id) REFERENCES login (id)
);

-- INSERTS ---------------------------------------------------------------------

-- Login -----------------------------------------------------------------------
INSERT INTO login(email, password, user_type) VALUES
  ('adele@paziente.it','password',0),
  ('alessandro@paziente.it','password',0),
  ('alessia@paziente.it','password',0),
  ('arnaldo@paziente.it','password',0),
  ('arsenio@paziente.it','password',0),
  ('battista@paziente.it','password',0),
  ('benigno@paziente.it','password',0),
  ('betta@paziente.it','password',0),
  ('bianca@paziente.it','password',0),
  ('bibiana@paziente.it','password',0),
  ('carolina@paziente.it','password',0),
  ('cleo@paziente.it','password',0),
  ('clorinda@paziente.it','password',0),
  ('daniel@paziente.it','password',0),
  ('donna@paziente.it','password',0),
  ('doriana@paziente.it','password',0),
  ('egle@paziente.it','password',0),
  ('ercolina@paziente.it','password',0),
  ('fabrizia@paziente.it','password',0),
  ('fernanda@paziente.it','password',0),
  ('fosca@paziente.it','password',0),
  ('fulvio@paziente.it','password',0),
  ('gerolamo@paziente.it','password',0),
  ('leonida@paziente.it','password',0),
  ('letizia@paziente.it','password',0),
  ('lorenzo@paziente.it','password',0),
  ('luca@paziente.it','password',0),
  ('lucilla@paziente.it','password',0),
  ('manfredo@paziente.it','password',0),
  ('manuela@paziente.it','password',0),
  ('marco@paziente.it','password',0),
  ('matteo@paziente.it','password',0),
  ('max@paziente.it','password',0),
  ('michelangelo@paziente.it','password',0),
  ('olimpia@paziente.it','password',0),
  ('piero@paziente.it','password',0),
  ('priamo@paziente.it','password',0),
  ('rebecca@paziente.it','password',0),
  ('roberta@paziente.it','password',0),
  ('rodolfo@paziente.it','password',0),
  ('romana@paziente.it','password',0),
  ('rosalinda@paziente.it','password',0),
  ('rosario@paziente.it','password',0),
  ('rossella@paziente.it','password',0),
  ('sabino@paziente.it','password',0),
  ('simeone@paziente.it','password',0),
  ('tizio@paziente.it','password',0),
  ('venanzio@paziente.it','password',0),
  ('virginio@paziente.it','password',0),
  ('vitalik@paziente.it','password',0),
  ('dario@dottore.it','password',1),
  ('fulvio@dottore.it','password',1),
  ('ilaria@dottore.it','password',1),
  ('christian@dottore.it','password',1),
  ('francesca@dottore.it','password',1),
  ('mi@ssp.it','password',2),
  ('to@ssp.it','password',2);

-- Dottore ---------------------------------------------------------------------
INSERT INTO dottori (nome, cognome, sesso, provincia, login_id) VALUES
  ('Dario','Amendolito','M','MI',51),
  ('Fulvio','Giordano','M','MI',52),
  ('Ilaria','Biagini','F','MI',53),
  ('Christian','Santarella','M','TO',54),
  ('Francesca','Cunsolo','F','TO',55);

-- Paziente --------------------------------------------------------------------
INSERT INTO pazienti (nome, cognome, luogo_nascita, sesso, dottore_id, provincia, login_id, codice_fiscale, data_di_nascita, propic) VALUES
  ('Adele','Riginaldo','Firenze','F',1,'MI',1,'RGNDLA71D67A633K','27/4/1971','/assets/propic/1.jpg'),
  ('Alessandro','Pinotti','Napoli','M',1,'MI',2,'PNLLSN69P06F839Q','06/09/1980','/assets/propic/2.jpg'),
  ('Alessia','Lamolinara','Milano','F',2,'MI',3,'LMLLSS90L47H239N','7/7/1990','/assets/propic/3.jpg'),
  ('Arnaldo','Timpini','Fiume','M',2,'MI',4,'TMPRLD72H23D669N','23/6/1972','/assets/propic/4.jpg'),
  ('Arsenio','Azzoletti','Brindisi','M',3,'MI',5,'ZZLRSN00B10I066X','10/2/2000','/assets/propic/5.jpg'),
  ('Battista','Pitarra','Torino','M',4,'TO',6,'PTRBTS98S14F041U','14/11/1998','/assets/propic/6.jpg'),
  ('Benigno','Borgiotti','Campobasso','M',5,'TO',7,'BRGBGN89M26G257B','26/8/1989','/assets/propic/7.jpg'),
  ('Betta','Namor','Torino','F',3,'MI',8,'NMRBTT76T71E067Q','31/12/1976','/assets/propic/8.jpg'),
  ('Bianca','Verme','Livorno','F',4,'TO',9,'VRMBNC00S64E625A','24/11/2000','/assets/propic/9.jpg'),
  ('Bibiana','Serembe','Reggio Calabria','F',5,'TO',10,'SRMBBN03A51I198D','11/1/2003','/assets/propic/10.jpg'),
  ('Carolina','Ballariano','Aquila','F',1,'MI',11,'BLLCLN02E42G231R','2/5/2002','/assets/propic/11.jpg'),
  ('Cleo','Francomanno','Torino','F',2,'MI',12,'FRNCLE89D46B386P','6/4/1989','/assets/propic/12.jpg'),
  ('Clorinda','Sigiani','Reggio Emilia','F',3,'MI',13,'SGNCRN85B51B825V','11/2/1985','/assets/propic/13.jpg'),
  ('Daniel','Leva','Milano','M',1,'MI',14,'QTVDXC51R63I815L','13/10/2000','/assets/propic/14.jpg'),
  ('Donna','Gambrioli','Vicenza','F',4,'TO',15,'GMBDNN62T63B143P','23/12/1962','/assets/propic/15.jpg'),
  ('Doriana','Saloma','Novara','F',5,'TO',16,'SLMDRN98E62C989K','22/5/1998','/assets/propic/16.jpg'),
  ('Egle','Lampasona','Asti','F',1,'MI',17,'LMPGLE69H67B707C','27/6/1969','/assets/propic/17.jpg'),
  ('Ercolina','Scopari','Milano','F',2,'MI',18,'SCPRLN73E64C052E','24/5/1973','/assets/propic/18.jpg'),
  ('Fabrizia','Princess','Venezia','F',3,'MI',19,'PRNFRZ73E53D748G','13/5/1973','/assets/propic/19.jpg'),
  ('Fernanda','Geraldi','Pesaro','F',4,'TO',20,'GRLFNN92M60F413O','20/8/1992','/assets/propic/20.jpg'),
  ('Fosca','Castellotti','Cuneo','F',5,'TO',21,'CSTFSC86M61E887X','21/8/1986','/assets/propic/21.jpg'),
  ('Fulvio','Romancino','Modena','M',2,'MI',22,'RMNFLV02L16G250G','16/7/2002','/assets/propic/22.jpg'),
  ('Gerolamo','Manoti','Bari','M',3,'MI',23,'MNTGLM83L17A893Q','17/7/1983','/assets/propic/23.jpg'),
  ('Leonida','Mighali','Cagliari','F',1,'MI',24,'MGHLND68M65D342Y','25/8/1968','/assets/propic/24.jpg'),
  ('Letizia','Taborgna','Nuoro','F',2,'MI',25,'TBRLTZ00D50G058O','10/4/2000','/assets/propic/25.jpg'),
  ('Lorenzo','Cremonesi','Milano','M',4,'TO',26,'CRMLNZ98R23F205J','23/10/1998','/assets/propic/26.jpg'),
  ('Luca','Crisafulli','Bari','M',5,'TO',27,'CRSLCU01P11A662Y','11/09/2001','/assets/propic/27.jpg'),
  ('Lucilla','Badini','Savona','F',3,'MI',28,'BDNLLL71M68D600Z','28/8/1971','/assets/propic/28.jpg'),
  ('Manfredo','Pinturo','Trento','M',1,'MI',29,'PNTMFR84C19I672B','19/3/1984','/assets/propic/29.jpg'),
  ('Manuela','Farisano','Venezia','F',4,'TO',30,'FRSMNL00S48D515V','8/11/2000','/assets/propic/30.jpg'),
  ('Marco','Messina','Venezia','M',2,'MI',31,'MSSMRC96A01L736O','01/01/1996','/assets/propic/31.jpg'),
  ('Matteo','Castagna','Verona','M',3,'MI',32,'CSTMTT98C25L781L','25/03/1998','/assets/propic/32.jpg'),
  ('Max','Catagnoli','Reggio Calabria','M',4,'TO',33,'CTGMXA64T15G735G','15/12/1964','/assets/propic/33.jpg'),
  ('Michelangelo','Kanaan','Verona','M',5,'TO',34,'KNNMHL98R23E512O','23/10/1998','/assets/propic/34.jpg'),
  ('Olimpia','Chiolini','Torino','F',5,'TO',35,'CHLLMP97R61H551X','21/10/1997','/assets/propic/35.jpg'),
  ('Piero','Kotina','Palermo','M',1,'MI',36,'KTNPRI94P30A958T','30/9/1994','/assets/propic/36.jpg'),
  ('Priamo','Bugattella','Alessandria','M',2,'MI',37,'BGTPRM87R31C087Q','31/10/1987','/assets/propic/37.jpg'),
  ('Rebecca','Brunori','Grossetto','F',1,'MI',38,'BRNRCC00R67E202D','27/10/2000','/assets/propic/38.jpg'),
  ('Roberta','Bottiglioni','Cagliari','F',2,'MI',39,'BTTRRT99E65F985A','25/5/1999','/assets/propic/39.jpg'),
  ('Rodolfo','Tassara','Milano','M',3,'MI',40,'TSSRLF89P09I793H','9/9/1989','/assets/propic/40.jpg'),
  ('Romana','Pontiero','Sassari','F',3,'MI',41,'PNTRMN85D66I452B','26/4/1985','/assets/propic/41.jpg'),
  ('Rosalinda','Cavarelli','Catanzaro','F',4,'TO',42,'CVRRLN61L60C726B','20/7/1961','/assets/propic/42.jpg'),
  ('Rosario','Ghisio','Cagliari','M',4,'TO',43,'GHSRSR61H01E742N','1/6/1961','/assets/propic/43.jpg'),
  ('Rossella','Paulich','Viterbo','F',5,'TO',44,'PLCRSL79D64E269N','24/4/1979','/assets/propic/44.jpg'),
  ('Sabino','Maranca','Vicenza','M',5,'TO',45,'MRNSBN61P03E762O','3/9/1961','/assets/propic/45.jpg'),
  ('Simeone','Cantalini','Bolzano','M',1,'MI',46,'CNTSMN60R09C652W','9/10/1960','/assets/propic/46.jpg'),
  ('Tizio','Vergero','Udine','M',2,'MI',47,'VRGTZI62C13I478J','13/3/1962','/assets/propic/47.jpg'),
  ('Venanzio','Bonasio','Brescia','M',3,'MI',48,'BNSVNZ94M10B855S','10/8/1994','/assets/propic/48.jpg'),
  ('Virginio','Dansi','Imola','M',4,'TO',49,'DNSVGN68E13D319X','13/5/1968','/assets/propic/49.jpg'),
  ('Vitalik','Buterin','Como','M',5,'TO',50,'BTRVLK94A31C933U','31/01/1994','/assets/propic/50.jpg');

-- SSP -------------------------------------------------------------------------
INSERT INTO ssp (provincia, login_id) VALUES
  ('MI', 56),
  ('TO', 57);

-- tipologia esami -------------------------------------------------------------
INSERT INTO tipologia_esami (nome) VALUES
  ('Angiografia'),
  ('Anoscopia'),
  ('Anticorpi'),
  ('Antigene carboidratico'),
  ('Arteriografia'),
  ('Biopsia del cuore'),
  ('Broncoscopia'),
  ('Chetosteroidi'),
  ('Citotossicità'),
  ('Dacriocistografia'),
  ('Defecografia'),
  ('Deossicortisolo'),
  ('Ecogradia'),
  ('Elettroencefalogramma'),
  ('Fluorangiografia'),
  ('Gengivoplastica'),
  ('Ileoncoloscopia'),
  ('Linfangiografia intratoracica'),
  ('Risonanza magnetica'),
  ('Tomografia computerizzata');

-- tipologia specialistiche -------------------------------------------------------------
INSERT INTO tipologia_specialistiche (nome) VALUES
  ('Allergologica'),
  ('Andrologica'),
  ('Angiologica'),
  ('Cardiochirurgica'),
  ('Cardiologica'),
  ('Diabetologica'),
  ('Geriatrica'),
  ('Microangiologica'),
  ('Nefrologica'),
  ('Neurologica'),
  ('Oculista'),
  ('Odontostomatologica'),
  ('Oncologica'),
  ('Ortopedica'),
  ('Piscodiagnostica'),
  ('Pneumologica'),
  ('Podologica'),
  ('Reumatologica'),
  ('Urologica'),
  ('Vulnologica');
